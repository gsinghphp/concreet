import React from 'react';
import PlacesAutocomplete, {
    geocodeByAddress,
    //getLatLng,
} from 'react-places-autocomplete';

class InputPlace extends React.Component {

    static defaultProps = {
        disabled: false
    }

    constructor(props) {
        super(props);
        this.state = { address: "", change: false };
    }

    static getDerivedStateFromProps(props, state) {

        if (state.address === '' && state.change !== true) {
            return {
                address: props.value,
                change: true
            }
        }
        return null
    }

    handleChange = address => {
        this.setState({ address });
        this.props.onChange(address)
    };

    handleSelect = address => {
        geocodeByAddress(address)
            .then(results => {
                this.props.onChange(results[0])
                this.setState({ address: results[0].formatted_address });
                return results[0]
            })
            .catch(error => console.error('Error', error));
    };

    render() {
        const { placeholder, disabled } = this.props;
        return (
            <PlacesAutocomplete
                value={this.state.address}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="input-place">
                        <input
                            {...getInputProps({
                                placeholder,
                                className: 'location-search-input',
                            })}
                            disabled={disabled}
                        />
                        <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? 'suggestion-item--active'
                                    : 'suggestion-item';
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                return (
                                    <div
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style,
                                        })}
                                    >
                                        <span>{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}

export default InputPlace;
