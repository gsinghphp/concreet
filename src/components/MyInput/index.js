import React from "react";
import styled from "styled-components";
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import InputPlace from './InputPlace';

const ConInput = styled.input`
  border-bottom: 1px solid #2f7ac9 !important;
  font-size: 16px !important;
  box-shadow: 0 1px 0 0 #2f7ac9 !important;
  max-width: 300px;
  height: 2.2rem !important;
  &:focus {
    box-shadow: 0 1px 0 0 #2f7ac9 !important;
    border-bottom: 1px solid #2f7ac9 !important;
  }
  ::placeholder {
    color: #2f7ac9;
  }
  ::-webkit-input-placeholder {
    /* Chrome/Opera/Safari */
    color: #2f7ac9;
  }
  ::-moz-placeholder {
    /* Firefox 19+ */
    color: #2f7ac9;
  }
  :-ms-input-placeholder {
    /* IE 10+ */
    color: #2f7ac9;
  }
`;

const MyInput = ({
  name,
  value,
  placeholder,
  onChange,
  validation,
  disabled,
  suggestions,
  onSuggestionClick,
  type,
}) => {
  let error = validation;
  if (typeof validation === "object" && validation !== null) {
    if (typeof validation[[name]] !== "undefined") {
      error = validation[[name]].msg;
    }
  }

  return (
    <div className="input-field">
      <ConInput
        placeholder={placeholder}
        name={name}
        value={value}
        className="validate"
        onChange={onChange}
        type={type}
        autoComplete="off"
        disabled={disabled}
      />
      <div className="error">{error}</div>
      {suggestions}
      {/* {suggestions.length > 0 && (
        <div className="option-list">
          {
            suggestions.map(listing => (
              <div style={{ textAlign: 'center' }} className="option-item" onClick={() => onSuggestionClick(listing)} key={listing.id}>
                {fullListingName(listing)}
              </div>
            ))
          }
        </div>
      )} */}
    </div>
  );
};

MyInput.defaultProps = {
  validation: null,
  disabled: false,
  suggestions: <></>,
  onSuggestionClick: () => { },
  onChange: () => { },
  type: "text",
};

export default MyInput;


const CurrencyInput = ({
  name,
  value,
  placeholder,
  onChange,
  disabled
}) => {
  const customProps =  {};

  if(value){
    customProps['startAdornment'] = <InputAdornment position="start">$</InputAdornment>
  }

  return (
    <div className="input-field currency-field">
      <Input
        disabled={disabled}
        id="adornment-amount"
        placeholder={placeholder}
        onChange={onChange}
        className="currency-input"
        name={name}
        value={value}
        {...customProps}
      />
    </div>
  );
};

CurrencyInput.defaultProps = {
  disabled: false
}

export {
  CurrencyInput,
  InputPlace
}
