import React from 'react';
import MyInput, { InputPlace } from '../../MyInput';
import { makeAddress } from '../../../utils/methods';

import { DatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

const AddressForm = ({ address, onChange, onSelectAddress, disabled }) => {
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <InputPlace
                name="address_street_name"
                onChange={onSelectAddress}
                value={makeAddress(address)}
                placeholder="Street Address"
                disabled={disabled} />

            <MyInput
                name="address_apartment_number"
                value={address.address_apartment_number}
                placeholder="Apartment Number"
                onChange={onChange}
                disabled={disabled}
            />

            <DatePicker
                views={['year', 'month']}
                label="Start Date"
                onChange={(e) => {
                    onChange({
                        target: {
                            name: 'address_valid_from',
                            value: e.format('YYYY-MM-DD')
                        }
                    })
                }}
                format="MM/YYYY"
                value={address.address_valid_from !== "" ? address.address_valid_from : null}
                disabled={disabled}
                className="date-pic"
            />

            <DatePicker
                views={['year', 'month']}
                label="End Date"
                minDate={address.address_valid_from !== "" ? address.address_valid_from : ""}
                onChange={(e) => {
                    onChange({
                        target: {
                            name: 'address_valid_to',
                            value: e.format('YYYY-MM-DD')
                        }
                    })
                }}
                format="MM/YYYY"
                value={address.address_valid_to !== ""? address.address_valid_to : null}
                disabled={disabled}
                className="date-pic"
            />
        </MuiPickersUtilsProvider>
    )
}

AddressForm.defaultProps = {
    onChange: () => { },
    disabled: false
}

export default AddressForm;