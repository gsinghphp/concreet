import React from 'react';
import MyInput from '../../MyInput';
import { CurrencyInput } from '../../MyInput';
import MySelect from '../../MySelect';
import Upload from '../../Upload';
import { accountTypes } from '../../../utils/constants';

const FinancialForm = ({ account, onChange, onDrop, onDelete, disabled }) => (
    <>
        <MyInput
            name="bank_account_name"
            placeholder="Bank Name"
            onChange={onChange}
            disabled={disabled}
            value={account.bank_account_name}
        />

        <MySelect
            name="bank_account_type"
            placeholder="Account Type"
            values={accountTypes}
            value={account.bank_account_type}
            onChange={onChange}
            disabled={disabled}
        />

        <CurrencyInput
          name="bank_account_current_month_starting_balance"
          value={account.bank_account_current_month_starting_balance}
          placeholder="Current Balance"
          onChange={onChange}
          disabled={disabled}
        />

        <CurrencyInput
          name="bank_account_last_month_starting_balance"
          onChange={onChange}
          placeholder="Last Month's Ending Balance"
          value={account.bank_account_last_month_starting_balance}
          disabled={disabled}
        />
        <div className="upload-list full-small dropzone-left">
            <Upload
                accept="application/pdf,image/x-png,image/png,image/jpeg"
                onDrop={(accept) => onDrop(accept, 'bank_account_statement_1')}
                file={account.bank_account_statement_1}
                text="Statement 1"
                onDelete={() => onDelete('bank_account_statement_1')}
                disabled={disabled} />

            <Upload
                accept="application/pdf,image/x-png,image/png,image/jpeg"
                file={account.bank_account_statement_2}
                onDrop={(accept) => onDrop(accept, 'bank_account_statement_2')}
                text="Statement 2"
                onDelete={() => onDelete('bank_account_statement_2')}
                disabled={disabled} />
        </div>
    </>
)

FinancialForm.defaultProps = {
    onChange: () => { },
    onDelete: () => { },
    onDrop: () => { },
    disabled: false,
}

export default FinancialForm;