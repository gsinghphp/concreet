import React from 'react';
import { DatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import MyInput from '../../MyInput';
import { CurrencyInput } from '../../MyInput';
import MySelect from '../../MySelect';
import Upload from '../../Upload';
import { incomeTypes } from '../../../utils/constants';

const IncomeForm = ({ income, onChange, onDrop, onDelete, disabled }) => {

    
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>

            <MySelect
                className="demo"
                placeholder="Source Type"
                name="income_type_id"
                values={incomeTypes}
                onChange={onChange}
                value={income.income_type_id}
                disabled={disabled}
            />
            <MyInput
                name="income_title"
                placeholder="Employer Name"
                onChange={onChange}
                value={income.income_title}
                disabled={disabled}
            />

            <DatePicker
                views={['year', 'month']}
                label="Start Date"  
                maxDate={new Date()}
                onChange={(e) => {
                    onChange({
                        target: {
                            name: 'income_start_date',
                            value: e.format('YYYY-MM-DD')
                        }
                    })
                }}
                format="MM/YYYY"
                className="date-pic"
                value={income.income_start_date !== "" ? income.income_start_date : null}
                disabled={disabled}
            />

            <CurrencyInput
              name="income_monthly"
              placeholder="Monthly Salary"
              onChange={onChange}
              value={income.income_monthly}
              disabled={disabled}
            />
            <div className="upload-list full-small dropzone-left">
                <Upload
                    accept="application/pdf,image/x-png,image/png,image/jpeg"
                    onDrop={onDrop}
                    file={income.documents[0]}
                    text="Document 1"
                    disabled={disabled}
                    onDelete={() => onDelete(income.documents[0])} />
                <Upload
                    accept="application/pdf,image/x-png,image/png,image/jpeg"
                    onDrop={onDrop}
                    file={income.documents[1]}
                    text="Document 2"
                    disabled={disabled}
                    onDelete={() => onDelete(income.documents[1])}
                />
            </div>
        </MuiPickersUtilsProvider>
    )
}

IncomeForm.defaultProps = {
    onChange: () => { },
    onDrop: () => { },
    onDelete: () => { },
    disabled: false
}

export default IncomeForm;