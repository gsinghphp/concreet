import BankAddressForm from './BankAddressForm';
import BankAccountForm from './BankAccountForm';
export {
    BankAddressForm,
    BankAccountForm
}
