import React from 'react';
import InputMask from 'react-input-mask';
import MyInput from '../../MyInput';

const formatChars = {
    n: '[0-1]',
    m: '[0-9]',
    e: '[0-3]',
    d: '[0-9]',
    z: '[1-2]',
    y: '[0-9]'
}



const BankAddressForm = ({ values, onChange, validations, type }) => (
    <>
        <MyInput
            name="address"
            placeholder="Address"
            onChange={onChange}
            value={values.address}
            validation={validations.addressV}

        />

        <MyInput
            name="address2"
            placeholder="Address 2"
            onChange={onChange}
            value={values.address2}
        />

        <MyInput
            name="city"
            placeholder="City"
            onChange={onChange}
            value={values.city}
            validation={validations.cityV}
        />

        <MyInput
            name="state"
            placeholder="State"
            onChange={onChange}
            value={values.state}
            validation={validations.stateV}
        />

        <MyInput
            name="zipcode"
            placeholder="Zip Code"
            onChange={onChange}
            type="number"
            value={values.zipcode}
            validation={validations.zipcodeV}
        />
        {type === 1 ? (
            <div class="input-field date-input">
                <InputMask
                    value={values.dob}
                    mask="nm/ed/zyyy"
                    alwaysShowMask={false}
                    formatChars={formatChars}
                    placeholder="Date of Birth"
                    name="dob"
                    onChange={onChange} />
                <div className="error">{validations.dobV}</div>
            </div>
        ) : (
                <MyInput
                    name="company_ein"
                    placeholder="Company EIN"
                    onChange={onChange}
                    type="number"
                    value={values.company_ein}
                    validation={validations.company_einV}
                />
            )}
        <MyInput
            name="last_4_of_ssn"
            placeholder="Last 4 of SSN"
            onChange={onChange}
            type="password"
            value={values.last_4_of_ssn}
            validation={validations.last_4_of_ssnV}
        />
    </>
)

BankAddressForm.defaultProps = {
    onChange: () => { },
    type: 0,
}

export default BankAddressForm;