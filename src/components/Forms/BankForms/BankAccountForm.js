import React from 'react';
import MyInput from '../../MyInput';

const BankAccountForm = ({ values, onChange, validations }) => (
    <>
        <MyInput
            name="bank_name"
            placeholder="Bank Name"
            onChange={onChange}
            value={values.bank_name}
            validation={validations.bank_nameV}
        />

        <MyInput
            name="account_number"
            placeholder="Account Number"
            onChange={onChange}
            value={values.account_number}
            validation={validations.account_numberV}
            type="number"
        />

        <MyInput
            name="routing_number"
            placeholder="Routing Number"
            onChange={onChange}
            value={values.routing_number}
            validation={validations.routing_numberV}
            type="number"
        />
    </>
)

BankAccountForm.defaultProps = {
    onChange: () => { }
}

export default BankAccountForm;