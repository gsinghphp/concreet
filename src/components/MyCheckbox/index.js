import React from "react";
import styled from 'styled-components';

const Checkbox = styled.input`
    border-color: #ffffff !important;
    background: #ffffff !important;

`;

const MyCheckbox = ({ value, onChange, label, disabled, shape, outsideLabel}) => {
  if (outsideLabel) {
    return (
      <div className="custom-checkbox">
        <label>
          <Checkbox
            type="checkbox"
            className={`filled-in ${shape}`}
            value={value}
            onChange={e => onChange(e.target.value)}
            checked={value}
            disabled={disabled}
          />
          <span></span>
        </label>
        <span className="label-text primary-check">{label}</span>
      </div>
    )
  }
  return (
    <label>
      <Checkbox
        type="checkbox"
        className={`filled-in ${shape}`}
        value={value}
        onChange={e => onChange(e.target.value)}
        checked={value}
        disabled={disabled}
      />
      <span className={value ? "label-text primary-check" : "label-text"}>{label}</span>
    </label>
  )
};

MyCheckbox.defaultProps = {
    onChange: () => { },
    disabled: false,
    shape: 'square',
    outsideLabel: false
}

export default MyCheckbox;
