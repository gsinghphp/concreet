import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const MyButton = ({ name, onClick, disabled, className, loading, type }) => (
  <div className="waves-effect-outer">
    <button
      className={`waves-effect btn ${className}`}
      onClick={onClick}
      disabled={disabled || loading}
      type={type}
    >
      {name}
    </button>
    {loading && (
      <CircularProgress
        size={30}
        style={{
          position: "absolute",
          right: "36%",
          marginTop: 10,
          zIndex: 9999,
          color: "#2f7ac9"
        }}
      />
    )}
  </div>
);

MyButton.defaultProps = {
  disabled: false,
  className: "",
  loading: false,
  type:"button"
};

export default MyButton;
