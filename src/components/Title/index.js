import React from 'react';
import { connect } from 'react-redux';
import { fullListingName } from '../../utils/methods';

const Title = ({ user, type,listing }) => {
    if (user) {
        return (
            <h5>
              {`${user.firstname} ${user.lastname}'s ${type} for `} <br />
              {listing.details && fullListingName(listing.details)}
            </h5>
        )
    }
    return (
        <></>
    )
}

const mapStateToProps = ({ authStore, applicationStore }) => ({
    user: authStore.user ? authStore.user.message.user : authStore.user,
    listing: applicationStore.renterApplication.length ? applicationStore.renterApplication[0].listing : {}
});

Title.defaultProps = {
    type: 'Resume'
}

export default connect(mapStateToProps)(Title);
