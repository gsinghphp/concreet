import FullLogo from './full';
import ShortLogo from './short';

export {
    FullLogo,
    ShortLogo
}
