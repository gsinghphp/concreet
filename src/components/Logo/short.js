import React from "react";
import logo from '../../img/Concrete.svg';

const ShortLogo = ({ onClick }) => (
  <div className="logo" onClick={onClick}>
    <div>
      <img src={logo} alt="Concrete Logo" style={{ height: 45 }} />
    </div>
  </div>
);

ShortLogo.defaultProps = {
  onClick: () => { }
}

export default ShortLogo;
