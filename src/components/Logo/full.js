import React from "react";
import logo from '../../img/logo.svg';

const FullLogo = () => (
  <div className="logo">
    <div>
      <img src={logo} alt="Concrete Logo" />
    </div>
  </div>
);

export default FullLogo;
