import React from 'react';
import PlaidLink from 'react-plaid-link'

class Plaid extends React.PureComponent {

    static defaultProps = {
        onSuccess: () => { }
    }
    
    render() {
        const { children, onSuccess } = this.props;
        return (
            <PlaidLink
                className="plaid-btn"
                clientName="Concreet"
                env="sandbox"
                selectAccount
                product={["auth", "transactions"]}
                publicKey="b37f57f5267b03ae3f110e4926ebb3"
                onExit={() => { }}
                onSuccess={(public_token, data) => { onSuccess(data); }}>
                {children}
            </PlaidLink>
        )
    }
}

export default Plaid;
