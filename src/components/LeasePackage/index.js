import React from 'react';
import Grid from "@material-ui/core/Grid";
import { formatAmount, getTotalAmount, checkSignature, getSignatureId, getUserRole } from '../../utils/methods'
import { MySelect, Upload, MyCheckbox } from "../index"
import moment from 'moment';

const LeasePackage = ({
    showSignsInfo,
    landlordInfo,
    renter,
    paymentValue,
    handleChange,
    disabled,
    listing,
    type,
    onSelectSignature,
    landlord,
    applicationValues,
    backAccounts,
    dialog,
    onCloseDialog,
    activeLandlordSignature,
    activeRenterSignature,
    signatures }) => (
        <Grid container spacing={24} justify="center" className="center-align ">
            <Grid item xs={12} sm={4} className="lease-payment-box">
                <h5>Payments</h5>
                <ul className="lease-payment-list">
                    {!landlord && <li>
                        <span> Broker Fee</span>
                        <span>${formatAmount(applicationValues.broker_fee) || 0}</span>
                    </li>}

                    <li>
                        <span> Security Deposit</span>
                        <span>${formatAmount(applicationValues.security_deposit) || 0}</span>
                    </li>
                    <li>
                        <span> First Month's Rent</span>
                        <span>${formatAmount(applicationValues.first_months_rent) || 0}</span>
                    </li>
                    <li>
                        <span> Last Month's Rent</span>
                        <span>${formatAmount(applicationValues.last_months_rent) || 0}</span>
                    </li>
                    {!landlord &&
                        <>
                            <li>
                                <span> Application and Verification Fee</span>
                                <span>$100</span>
                            </li>
                            <li>
                                <span> Total Due</span>
                                <span>${getTotalAmount(applicationValues)}</span>
                            </li>
                        </>}

                </ul>
                <MySelect
                    className="demo"
                    placeholder="Link a Bank Account to pay amount due"
                    values={backAccounts}
                    add
                    value={paymentValue}
                    addText="Add a New Payment Method"
                    onChange={handleChange}
                    name="payment"
                    disabled={type !== 'renter'}
                    dialog={dialog}
                    onCloseDialog={onCloseDialog}
                />
                {!landlord && <Grid item xs={12} sm={12} className="#">
                    <h5>Landlord</h5>
                    <div className="profile-detail">
                        {/* Landlordfirstname Landlordlastname <br /> */}
                        {listing.landlord_email}
                    </div>
                </Grid>}
                <Grid item xs={12} sm={12} className="#">
                    <h5>Lease Documents and Retainers</h5>
                    <div className="upload-list full-small dropzone-center lease-dropzone">
                        {listing.documents.map((doc, index) => (
                            <div className="upload-box lease-document" key={doc.id}>
                                <Upload
                                    accept="application/pdf"
                                    file={doc.details}
                                    text="Last Year"
                                    disabled={disabled} />
                                <MyCheckbox
                                    name={`verify[${doc.id}]`}
                                    label={fetchRenterSignatureText(showSignsInfo, signatures, doc.id, "Renter", renter)}
                                    disabled={!activeRenterSignature || applicationValues.status === 6 || getUserRole() === 2 }
                                    value={checkSignature(signatures, doc.id, "Renter")}
                                    shape="round"
                                    onChange={(value) => onSelectSignature(doc.id, value, getSignatureId(signatures, doc.id, "Renter"))}
                                />
                                <MyCheckbox
                                    label={fetchLandlordSignatureText(showSignsInfo, signatures, doc.id, 'Landlord', landlordInfo)}
                                    disabled={!activeLandlordSignature || applicationValues.status === 6 || getUserRole() === 2 }
                                    value={checkSignature(signatures, doc.id, 'Landlord')}
                                    shape="round"
                                    onChange={(value) => onSelectSignature(doc.id, value, getSignatureId(signatures, doc.id, "Landlord"))} />
                            </div>
                        ))}

                    </div>
                </Grid>
            </Grid>
        </Grid>
    );

LeasePackage.defaultProps = {
    renter: {},
    landlordInfo: {},
    showSignsInfo: false,
    handleChange: () => { },
    disabled: false,
    type: '',
    onSelectSignature: () => { },
    landlord: false,
    applicationValues: {},
    dialog: false,
    onCloseDialog: () => { },
    activeLandlordSignature: false,
    activeRenterSignature: false,
    signatures: []
}

const fetchRenterSignatureText = (showSignInfo, signatures, id, type, renter) => {
    if (!showSignInfo) {
        return 'I agree to the above document and endorse it with my digital signature';
    }

    if (checkSignature(signatures, id, type)) {
        let sign = getSignatureId(signatures, id, type);
        let text = `Signed at ${moment(sign.created_at).format('h:mm a')} on ${moment(sign.created_at).format('MM/DD/YYYY')} `;
        text += renter ? `by ${renter.firstname} ${renter.lastname}` : '';
        return text;
    }
    return "Awaiting renter's signature";
}

const fetchLandlordSignatureText = (showSignInfo, signatures, id, type, landlordInfo) => {
    if (!showSignInfo) {
        return 'Awaiting landlord signature...';
    }

    if (checkSignature(signatures, id, type)) {
        let sign = getSignatureId(signatures, id, type);
        let text = `Signed at ${moment(sign.created_at).format('h:mm a')} on ${moment(sign.created_at).format('MM/DD/YYYY')} `;
        text += landlordInfo ? `by ${landlordInfo.firstname} ${landlordInfo.lastname}` : '';
        return text;
    }
    return "I agree to the above document and endorse it with my digital signature";
}

export default LeasePackage;
