import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class ConfirmDialog extends React.Component {
    dialogPromise;
    dialogPromiseResolve;
    dialogPromiseReject;
    state = {
        open: false,
    }

    open = async () => {
        this.setState({ open: true })
        return this.dialogPromise = new Promise((resolve, reject) => {
            this.dialogPromiseResolve = resolve;
            this.dialogPromiseReject = reject;
        });
    }

    accept = async () => {
        this.setState({ open: false })
        return this.dialogPromiseResolve(true)
    }

    reject = async () => {
        this.setState({ open: false })
        return this.dialogPromiseReject(false)
    }

    render() {
        const { open } = this.state;
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={this.reject}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Document Delete?"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Are you sure to delete this?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.reject} color="primary">
                            No
                        </Button>
                        <Button onClick={this.accept} color="primary" autoFocus>
                            Yes
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

};

ConfirmDialog.defaultProps = {
    onYes: () => { },
    onNo: () => { }
}

export default ConfirmDialog;