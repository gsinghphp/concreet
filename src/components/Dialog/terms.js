import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class TermsDialog extends React.Component {

    static defaultProps = {
        open: false,
        toggle: () => { }
    }

    render() {
        const { open, toggle } = this.props;
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={toggle}
                    aria-labelledby="scroll-dialog-title"
                >
                    <DialogTitle id="scroll-dialog-title"><h4 className="top-title">Concreet Terms of Service</h4></DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <p>Last Updated: April 25, 2018</p>
                            <h4 className="top-sub-title">IMPORTANT -PLEASE READ CAREFULLY</h4>
                            <p>
                                This Concreet Terms Of Use Agreement (the Terms),along with any other terms that may be posted on the  website  located  at www.Concreet.com(the  Site),  which  is  owned  by  and  operated  on  behalf  of ConcreetTechnologies LLC(Concreet,  we,  us  or  our)  and/or  any  written  agreement  providing other terms and conditions that incorporate these Terms by reference, states the terms and conditions under which a user of the site (User, you and your)  may access, visit, browse, interact withand/or usein any other way(each of the foregoing, both individually and collectively, Use) any component or portionof our Site and Services (defined below). For the avoidance of doubt, these Termsincorporate by reference Concreets Privacy Policy (the   Privacy  Policy)   located   at   https://www.Concreet.com /privacy_policy or at such other URLas we may indicate.
                        </p>
                            <br></br>
                            <p>
                                By (i) clicking the "Iaccept" button when presented to you or (ii) by Usingthe Site or any of the Services, you are indicating your agreement to be bound by all the terms and conditions contained in these Terms. If you do not agree to each and every provision of theseTerms(including but not limited to the Privacy Policy as incorporated by reference), you do not have our consent to Usethe Site or any of the Servicesin any way or for any reason whatsoever, and must cease all Use of the Site and Services immediately.
                        </p>

                            <ol className="counting pl-10">
                                <li>
                                    <b>CONCREET, ITS SERVICES AND ITS USERS</b>
                                    <ol className="counting">
                                        <li>Concreet offers  the  Site  as anonline technology  platform  that  facilitates  the  delivery  of residential rentaland lease-related services including, without limitation, residence listing, rental applications,applicant  screening    (including  credit  checks),scheduling  of  open-house  visits,digitalexecution of residential rental agreements, leases, and related guaranty agreements (any suchrental agreement, leaseor guaranty agreement, a Rental Agreement), online payment of rent, brokers fees,utilitiesand other related items,and delivery and execution of other rental, lease, and residence-related agreements and notifications (collectively, the "Services").
                    							</li>
                                        <li>Given the kinds of Services available through the Site, Concreet has a number of different kinds of Users, including:
                    									<ol className="counting">
                                                <li><b>Renters:</b>  individuals  that  are  Using  the  Site  and  Services  (i)  to  apply  to  rent  or  lease  a residence  by  Usingthe  Site  or  Services,  (ii)  to  apply  to  act  as  a  guarantor  of  another individuals application to rent or lease a residence by Using the Site or Services, and (iii) to make   certain   payments,   receive   certain   notifications,   and   sign   certain   additional agreements  in  connection  with  their  current Rental  Agreement(for  example,  a  tenant making a monthly rent payment or accepting a notification that her or his Landlord might be required to provide);</li>
                                                <li><b>Landlords:</b> individuals or legal entities that are Using the Site and Services in connection with a residential property that they own, operate and/or manage; and</li>
                                                <li><b>Brokers:</b> licensed real estate brokers, agents, and salespersons that are Using the Site and
                                                             Services on behalf of a Renter and/or a Landlord.Please read these Terms carefully as some of the terms and conditions herein may apply to some
                                                             but not all Users.
                    											</li>
                                            </ol>
                                        </li>
                                        <li>For the avoidance of doubt, if you are Using the Site or Services but do not qualify as a Renter,
                                                Landlord, or Broker (for example, if you are visiting the Site but have not yet created an Account
                                                (defined below)), you are still a User for the purposes of these Terms and are still bound by all
                                                the terms and conditions herein generally applicable to all Users.
                    						 	</li>
                                    </ol>
                                </li>
                                <li>
                                    <b> ACCESS AND USE OF SITE </b>
                                    <ol className="counting">
                                        <li>Subject to your compliance with the Terms, Concreet hereby grants you permission to Use the
                                                Site and Services, provided that you shall not (and shall not allow any third party to):
                    									<ol className="counting">
                                                <li>engage in commercial use of the Site or Services or any information or content on the Site
                                                        not provided by you except as expressly permitted herein;
                    										</li>
                                                <li>
                                                    without Concreets prior written permission in each instance, copy, download, transmit,
                                                    modify, reproduce, sell, resell, sub-license, distribute, publish, publicly display, create
                                                    derivative works or compilations of, reverse engineer, assign, sub-license, transfer or
                                                    otherwise exploit any portion of the Site (including but not limited to any HTML code used
                                                    to generate any page of the Site), the Services, any Concreet Content (defined below), or
                                                    any User Information (defined below) contained therein except for your own Use as
                                                    permitted by these Terms;
                    										</li>
                                                <li>remove any copyright, trademark or other proprietary rights notices (whether Concreets or
                                                     another partys) contained in or on the Site or in or on any content or other material
                                                     obtained in your Use of the Site and Services;
                    										</li>
                                                <li>
                                                    use any robot, spider, site search/retrieval application, or other automated device, process
                                                    or means to access, retrieve or index any portion of the Site, including, but not limited to,
                                                    for purposes of constructing or populating a searchable database of business or property
                                                    reviews;
                    										</li>
                                                <li>
                                                    except as expressly permitted in these Terms, collect or harvest any information about other
                                                    Users (including usernames and/or email addresses) for any purpose;
                    										</li>
                                                <li>
                                                    reformat or frame any portion of the web pages that are part of the Site;
                    										</li>
                                                <li>
                                                    create or transmit unwanted electronic communications such as "spam" to other Users of
                                                    the Site and Services or otherwise interfere with other Users Use of the Site or Services;
                    										</li>
                                                <li>
                                                    transmit any viruses, worms, defects, Trojan horses or other items of a destructive nature
                                                       to or through the Site or Use the Site or any Service to violate the security of any computer
                                                       network, crack passwords or security encryption codes, transfer or store illegal material,
                                                       including any material that may be deemed threatening or obscene;
                    										</li>
                                                <li>
                                                    use any device, software or procedure that interferes with the proper working of the Site or
                                                    Services, or otherwise attempt to interfere with the proper working of the Site or Services;
                    										</li>
                                                <li>
                                                    take any action that imposes, or may impose in Concreet's sole discretion, an
                                                    unreasonable or disproportionately large load on the Site or on Concreets IT infrastructure;
                    										</li>
                                                <li>
                                                    to the extent you are conducting business Using the Site and Services, doing so in a way
                                                           that is unfair, unlawful, or constitutes a deceptive business practice;
                    										</li>
                                                <li>
                                                    engage in any fraudulent activity or activity that facilitates fraud on, through, or in
                                                    connection with the Site and Services;
                    										</li>
                                                <li>
                                                    impersonate any person or entity, including, but not limited to, other Users, falsely state
                                                    or otherwise misrepresent your affiliation with any person or entity, or express or imply that
                                                    we endorse any statement you make; or
                    										</li>
                                                <li>
                                                    use the Site or Services, intentionally or unintentionally, to violate any applicable local,
                                                state, national or international law or regulation, including, but not limited to, the Fair
                                                Housing Act (enacted as Title VIII of the Civil Rights Act of 1968, as amended, modified or
                                                restated) and all other fair housing laws, regulations and ordinances (collectively, Fair
                                                Housing Laws) and privacy laws and regulations.
                    										</li>
                                            </ol>
                                        </li>
                                        <li>You are solely responsible for (i) providing all equipment and software necessary to establish a
                                                connection to the Internet and to Use the Site and the Services, (ii) your access to the Internet,
                                                and (iii) any telephone, wireless or other connection, service or other fees and charges associated
                                                with such equipment, software and access, and Concreet shall have no responsibility or liability
                                                for the same.
                    							</li>
                                    </ol>
                                </li>
                                <li>
                                    <b>CHILDREN UNDER THE AGE OF 18.</b> The Site and Services are not intended for children under 18 years
                                    of age. No one under age 18 (and no one else on behalf of someone under 18) may Use the Site or
                                    any of the Services, enter into any transaction of any kind through the site, register for an account on
                                    the Site or provide any information to or on the Site. We do not knowingly collect any information
                                    from children under 18. If you are under 18, do not Use the Site or any of the Services, enter into any
                                    transaction of any kind through the site, register for an account on the Site or provide any information
                                    to or on the Site, and do not let anyone else do so on your behalf. If you believe we might have any
                                    information from or about a child under 18, please contact us at Support@Concreet.com.
                    				</li>
                                <li><b>ACCOUNT REGISTRATION AND USE.</b>
                                    <ol className="counting">
                                        <li>
                                            In order to Use any of the Services, you first need to register and create a password-protected
                    						account (an <b>"Account"</b>) and, in connection therewith, submit such User Information (defined
                    						below) as the Site indicates is required. Some kinds of Users may be required to submit different
                    						User Information than other kinds of Users in order to create and activate an Account.
                    						</li>
                                        <li>
                                            You agree: (i) that any information or materials required by the Account registration form or
                    					creation process will be true, accurate, current, and complete; and (ii) to maintain and update all
                    					such information and materials to keep your Account true, accurate, current, and complete at all
                    					times.
                    						</li>
                                        <li>
                                            You are responsible for maintaining the confidentiality of your password, your email address and
                                            your Account, and are fully responsible for all activities that occur under your password and your
                                            Account. You agree to: (i) immediately notify Concreet of any unauthorized use of your password
                                            or account, or any other breach of security, (ii) immediately change your password if you become
                                            aware that your Account has been compromised, (iii) ensure that you fully exit from your Account
                                            at the end of each session. You agree and acknowledge that you will not allow others to utilize
                                            your Account and that you will not disclose your Site password to anyone. You will be solely
                                            responsible for safeguarding your password and also for any actions under your password and
                                            Account, whether authorized by you or not. If you lose control of your password, you may lose
                                            substantial control of any information and materials in your Account and you could potentially
                                            be subject to legally binding actions taken on your behalf through use of your Account. You
                                            further agree not to use anyone else's password on the Site or attempt to gain access to the
                                            Account of any other User. Concreet cannot and will not be liable for any loss or damage arising
                                            from your failure to comply with this section.
                    						</li>
                                    </ol>
                                </li>

                                <li><b>USER INFORMATION.</b>
                                    <ol className="counting">
                                        <li>User Information. For the purposes of these Terms, User Information shall mean any and all
                                                data, information, and other materials that a User (whether you or another User) submits,
                                                uploads, posts, e-mails or otherwise sends or transmits (Transmit) to or in connection with the
                                                Site and Services (including, without limitation, in connection with creating an Account, posting
                                                a residential listing, or otherwise in connection with a property to be rented or leased) or as may
                                                later be required or requested in connection with any Service and/or any transaction related
                                                thereto, entered into through, or arising in connection with, the Site or Services. The term User
                                                Information also includes any data, information, and other materials provided or made available
                                                by a third party at the direction and/or authorization of User (for instance, the information
                                                provided by a Background Checking Service about a Renter); provided, however, that the term
                                                User Information shall not include any information that Concreet may otherwise obtain or
                                                develop about a User (for instance, any notes or records that Concreet may keep about your
                                                usage of the Site or any of the Services).
                    								</li>

                                        <li>
                                            You are solely responsible and liable for any and all User Information that you Transmit to, or in
                                               connection with, the Site and Services. In the case of any User Information provided by another
                                               User, you acknowledge and agree that such User, not Concreet, is solely responsible and liable
                                               in connection with such User Information. You also acknowledge and agree that Concreet has no
                                               responsibility or duty to review, approve, or pre-screen any User Information provided by, on
                                               behalf of, or to a User (including you), whether such User Information is made available only to
                                               certain specific other parties or is generally available on the Site, and Concreet is not responsible
                                               for the accuracy, quality, relevance, or completeness of such User Information. You expressly
                                               agree that you, not Concreet, bears the responsibility to evaluate, and bears all risks associated
                                               with, the use of any User Information, and that under no circumstances will Concreet be liable
                                               for any User Information or for any damage or loss of any kind incurred as a result of the use of
                                               any User Information.
                    								</li>

                                        <li>
                                            You hereby represent and warrant that any User Information you Transmit to the Site or
                                            otherwise communicate, share, or make available (whether through the Site or not through the
                                            Site) in connection with the Site, the Services or any potential or executed agreement or
                                            transaction related thereto, including but not limited to any Rental Agreement, shall be true,
                                            accurate, complete, up-to-date, and with no material omissions.
                    								</li>

                                        <li>Concreet reserves the right (but does not have the obligation) in its sole discretion to delete or
                                                refuse to post, provide, or facilitate the provision of any User Information (whether provided by
                                                you or on your behalf or otherwise) that violates the letter or spirit of any applicable agreements
                                                between Concreet and the entity or individual posting or seeking to post the content, or for any
                                                other reason.
                    								</li>

                                        <li><b>Concreets Use of Your User Information.</b>
                                            <ol className="counting">
                                                <li>With respect to any information or materials that you Transmit to the Site, except as
                                                otherwise expressly limited by these Terms (including the Privacy Policy), you grant
                                                Concreet a perpetual, irrevocable, worldwide, royalty-free, non-exclusive license to use,
                                                copy, excerpt, reproduce, display, publish, modify, distribute and create derivative works
                                                and compilations of such information or materials in any form or media, for any purpose,
                                                and to allow others to do so. In addition to using your User Information for the purpose of
                                                providing the Services, you expressly agree that Concreet may disclose or use any of your
                                                User Information for purposes that include, but are not limited to (i) enforcing these Terms;
                                                (ii) complying with any laws, regulations or rules of any federal, state or local government
                                                or agency; (iii) responding to a claim that comes to our attention that any User Information
                                                violates the rights of third parties; or (iv) protecting the rights or property of Concreet, its
                                                partners, vendors, and contractors, its Users or the public.
                    								        </li>

                                                <li>To the extent your Use of any Service requires information or other materials about you to
                                                    be provided to a third party or to another User (for instance, the provision of your credit
                                                    information to a Landlord) by way of a third-party service that provides information about
                    										you (any such service, a <b>Background Checking Service</b>), your creation of an account
                                                    hereby constitutes authorization to Concreet to request and/or direct the distribution of
                                                    such information or other materials by each applicable Background Checking Service in each
                                                    instance when such information or other materials is required so you can use a Service.
                    								    </li>

                                            </ol>
                                        </li>


                                        <li><b>User Representations and Warranties.</b>
                                            You hereby represent and warrant that your User
                                            Information, Concreet's use thereof pursuant to these Terms, and any Communications (defined
                                            below) that you send to any User or other third party through or in connection with the Site, the
                                            Services, or any matter related thereto (i) do not and will not, directly or indirectly, violate,
                                            infringe or breach any duty toward or rights of any person or entity, including without limitation
                                            any Fair Housing, copyright, trademark, service mark, trade secret, other intellectual property,
                                            publicity or privacy right; (ii) are not fraudulent, misleading, hateful, tortious, defamatory,
                                            slanderous, libelous, abusive, violent, threatening, profane, vulgar or obscene; (iii) do not harass
                                            others, promote bigotry, racism, hatred or harm against any individual or group, promote
                                            discrimination based on race, sex, religion, nationality, sexual orientation or age, or otherwise
                                            interfere with another party's use of the Site or Services; (iv) do not promote illegal or harmful
                                            activities; and (v) are not illegal, unlawful or contrary to the laws or regulations in any state or
                                            country where, as applicable, your User Information has been or is created, displayed or
                                            accessed. In the case of User Information about you that is provided or otherwise made available
                                            by a Background Checking Service, the above representations and warranties shall be applicable
                                            to the best of your knowledge.
                    								</li>
                                    </ol>
                                </li>

                                <li><b>INTERACTIONS WITH THIRD PARTIES.</b>
                                    <ol className="counting">
                                        <li>In your Use of the Site and Services, you may communicate with, receive information and/or
                                            materials from, provide information and/or materials to, or enter into agreements (including but
                                            not limited to Rental Agreements) with one or more third parties (which may or may not include
                                            other Users). Any such activity, and any terms, conditions, warranties or representations
                                            associated with such activity (for example, the terms of a Rental Agreement), are solely between
                                            you and the applicable third party. Concreet shall have no liability, obligation, or responsibility
                                            for any goods, services, information or other materials provided to you by a third party or by you,
                                            or on your behalf, to a third party, Concreet does not endorse any agreement, arrangement,
                                            service or good that is made available to you as a result of your Use of the Site and the Services.
                                            In addition, to the extent it may apply, you acknowledge that certain third-parties providing
                                            ancillary software, hardware, or services in connection with the Site and the Services may require
                                            your agreement to additional or different licenses or other terms prior to your use of or access
                                            to such software, hardware or services.
                    						</li>

                                        <li><b>Communications.</b>Regardless of the kinds of communications methods currently enabled by the
                                            Site (whether email, instant-messaging, audio messaging, video messaging, or any other kind of
                                            communications method or technology), you acknowledge and agree that, unless Concreet
                                            expressly participates in such Communication (defined below), Concreet shall be deemed to be
                                            not involved in any communication, conversation, exchange, or messaging (collectively, a
                    							"<b>Communication</b>") between you and any third party (including any other User), whether that
                                            Communication takes places on or through the Site or otherwise pertains to the Site, the Services,
                                            or any matter related thereto. Concreet is only responsible for Communications issued directly
                                            by Concreet and has no responsibility or liability for the content or consequences of any
                                            Communications which it is not a part of. You will address any issues or concerns you have
                                            concerning any Communications not expressly involving Concreet by contacting the applicable
                                            third party or parties directly.
                    						</li>

                                        <li><b>Disputes.</b>In the event that you have a dispute with another User (for instance, a Landlord has a
                                            dispute with a Renter in connection with a Rental Agreement) or other third party you have dealt
                                            with in connection with the Site, the Services, or any related transaction, you release Concreet
                                            permanently and irrevocably from any claims, demands and damages (actual and consequential)
                                            of every kind and nature, known and unknown, suspected and unsuspected, disclosed and
                                            undisclosed, arising out of or in any way connected with such dispute.
                    						</li>
                                    </ol>
                                </li>

                                <li><b>ADDITIONAL TERMS REGARDING RENTAL AGREEMENTS</b>
                                    <ol className="counting">
                                        <li>Although Concreet may provide the Services and the Site to ultimately enable Users to enter into
                                            Rental Agreements for specific properties, unless Concreet expressly states otherwise in a
                                            separate communication or agreement with one or more Users, Concreet is not party to or
                                            involved in any way in any Rental Agreement or any related transactions other than by providing
                                            the Site and the Services. As a result, you acknowledge and agree that Concreet will not be liable
                                            under any circumstances for any aspect of any Rental Agreement, any aspect of the residence
                                            rented or leased pursuant to such Rental Agreement (including but not limited to such
                                            residences safety, quality, condition, operation or management, or compliance with any
                                            applicable laws or regulations), any transaction or other matter arising from or related to such
                                            Rental Agreement, or any other interaction or transaction between a Renter and a Landlord or a
                                            Broker acting on a Landlords behalf.
                    						</li>

                                        <li>You may not Use the Site or the Services to propose, facilitate, or enter into a Rental Agreement
                                            that will be shorter than thirty (30) days in length.
                    						</li>

                                        <li>A User that is a tenant of a residence (as opposed to the owner, operator, or manager of that
                                            property) may not Use the Site or Services to sublease or assign the Rental Agreement to another
                                            User.
                    						</li>
                                    </ol>
                                </li>

                                <li><b>PAYMENTS.</b>
                                    <ol className="counting">
                                        <li>Except as expressly provided in these Terms or in any Additional Requirements (defined below),
                                            any payments submitted through the Site by a User, whether for a rental deposit, rental
                                            payment, or any other service or transaction, are for goods or services provided by persons other
                                            than Concreet, such as another User (for example, a Landlord) or other third party. Concreet is
                                            not involved in, is not liable for, and is not a party to those transactions even though (i) Concreet
                                            provides a platform through the Site and Services through which you may submit payments to
                                            other Users or certain third parties and/or other Users and certain third parties may submit
                                            payments to you or to others on your behalf, and (ii) Concreet may receive a portion of payments
                                            made by Users through the Site. The total payment amounts required for those transactions are
                                            determined solely by you and the other User or third party with which you are entering into an
                                            agreement, and not by Concreet. You acknowledge that you will address any discrepancies,
                                            issues or concerns with such payments by contacting the other User or third party directly, rather
                                            than Concreet. Concreet shall not be involved in such issues except where the issue is solely
                                            attributable to a malfunction or error occurring on the Site or in connection with the Services.
                    						</li>

                                        <li>Notwithstanding the above, certain Services relating to Users, such as screening services and
                                            Rental Agreement execution fees, may have a charge associated with them that may be
                                            determined and imposed by Concreet or another company on a User, which other company may
                                            or may not be a subsidiary or affiliate of Concreet. Concreet or such other company may collect
                                            these fees directly from Users. You will be notified by Concreet before you incur any such fee or
                                            charge.
                    						</li>
                                    </ol>
                                </li>

                                <li><b>SERVICES NOT PROVIDED.</b>

                                    <p>You acknowledge and agree that, unless Concreet expressly states otherwise in a separate communication
                                        or agreement, Concreet does not provide the following services through the Site and Services, and bears
                                        no responsibility or liability for the following activities, among others:
                    					</p>
                                    <ol className="counting">
                                        <li>Rental Agreements, including without limitation, negotiations, offers, agreements, establishing
                                            rents or fees, or any related communications (although the Services do facilitate these
                                            transactions between Renters and Landlords or Brokers representing Landlords);
                    							</li>

                                        <li>
                                            Legal, brokerage or other related professional services or advice;
                    								</li>

                                        <li>
                                            Inspection, screening or pre-approval of rental properties;
                    								</li>

                                        <li>Verification, screening or pre-approval of property listings, <b>including, for the avoidance of doubt,
                                            any property listings that Concreet itself has arranged to display or make available on or
                    									through the Site;</b> or
                    								</li>

                                        <li>Evaluation, screening or pre-approval of any third party that has provided listings or other
                                            content for the Site.
                    								</li>
                                    </ol>
                                    <p>In the event that you need or desire such services or need assistance with such services, you are
                    									responsible for obtaining them from a third party.</p>
                                </li>

                                <li><b>ADDITIONAL REQUIREMENTS FOR SPECIFIC SERVICES.</b>It may be the case that specific Services may
                                    be subject to additional fees, additional guidelines, technical and non-technical specifications, and
                                    other requirements, terms, rules, or policies of Concreet that are not set forth in these Terms
                    					("<b>Additional Requirements</b>"). Any Additional Requirements will be posted on the Site or the
                                    applicable screens or page(s) on the Site. All such Additional Requirements are hereby incorporated
                                    by reference into these Terms. In the event of a conflict between the Additional Requirements and
                                    these Terms, the terms of the Additional Requirements shall govern.
                    				</li>

                                <li><b>ADDITIONAL TERMS FOR LANDLORD AND BROKERS.</b>
                                    <ol className="counting">
                                        <li>You acknowledge and agree that any User Information you receive through or as a result
                                            of Using the Site and Services that constitutes Personal Information (as that term is defined in
                                            the Privacy Policy) of a Renter shall only be used in connection with the specific matter that it
                                            was provided to you in connection with (for example, the specific rental application that Renter
                                            is making). You further acknowledge and agree that such User Information shall not be used in
                                            any other way, for any other purpose, or in connection with any other matter, whether in
                                            connection with the Site and Services or not, or shared with or made available to any other
                                            person, without such Renters prior authorization.
                    						</li>

                                        <li>If you are a Broker, you represent and warrant that you have any and all licenses,
                                            registrations, and permits required to act as such in the jurisdiction in which you are operating
                                            in, all such licenses, registrations, and permits are valid and up-to-date, and that you are acting
                                            in full compliance with the laws and regulations governing such licenses, registrations and
                                            permits in connection with any Use by you of the Site and the Services or in connection with any
                                            transaction related thereto.
                    						</li>

                                        <li>If you are a Broker, and in connection with your Use of the Site, the Services, or any
                                            transaction related thereto, you would be entering into a â€˜dual representation of both a Renter
                                            and an owner, operator, or manager of a property (whether such is a User or not), you are solely
                                            responsible for ensuring that all persons you are representing in connection with the same
                                            matter are notified in writing of your dual representation, and you shall be solely liable for your
                                            failure to do so. Furthermore, you will provide written evidence of such notification to Concreet
                                            at any time Concreet so requests.
                    						</li>

                                        <li>If you are a Broker, Concreet has the right to require you to as soon as possible provide a
                                            copy of a legally binding agreement between you and any Landlord on whose behalf you are
                                            Using the Site and Services.
                    						</li>

                                        <li>If you are a Landlord or a Broker acting on behalf of a Landlord in connection with the Site
                                            and Services, Concreet has the right to require you to as soon as possible provide legally valid
                                            documentary evidence that you (if you are a Landlord), or the Landlord on whose behalf you are
                                            acting (if you are a Broker), owns any residence that you are making available to rent or lease or
                                            that you are otherwise dealing with through and/or in connection with the Site and Services.
                    						</li>

                                    </ol>
                                </li>


                                <li><b>INDEMNITY.</b>
                                    <ol className="counting">
                                        <li>
                                            You agree to indemnify, defend and hold Concreet, its affiliates, and its and their
                    						members, managers, officers, directors, employees, shareholders, assigns and successors,
                    						agents, representatives, and relevant third parties including its and their service providers,
                    						distribution partners, referral partners, advertisers, licensors, licensees, consultants and
                    						contractors (collectively the "<b>Concreet Indemnified Parties</b>") harmless from and against any
                    						claims, liabilities, losses, costs, damages or expenses including reasonable attorneys' fees and
                    						costs, directly or indirectly, arising out of or in any way relating to (i) your Use of the Site and
                    						Services; (ii) any User Information you Transmitted to the Site or in connection with any of the
                    						Services; (iii) your violation (alleged or actual) of any laws or regulations of any state or country,
                    						including without limitation, privacy or Fair Housing laws; (iv) any violation or breach (alleged or
                    						actual) of these Terms or the terms of any other agreement or understanding between you and
                    						Concreet; or (v) any violation or infringement (alleged or actual) of any intellectual property,
                    						privacy, publicity, confidentiality or proprietary rights of any third-party as a result of your Use
                    						of the Site or Services or any matter arising or relating thereto (collectively, "<b>Claims</b>.").
                    					 </li>

                                        <li>
                                            You are solely responsible for defending any such Claims, and for payment of losses, costs,
                                            damages or expenses resulting from the foregoing to both any third party and to any Concreet
                                            Indemnified Party. Concreet shall have the right, in its sole discretion, to select its own legal
                                            counsel to defend Concreet from any Claims (but by doing so shall not excuse your indemnity
                                            obligations) and you shall be solely responsible for the payment of all of Concreet's reasonable
                                            attorneys' fees incurred through appeal in connection therewith. You shall notify Concreet
                                            immediately if you become aware of any actual or potential claims, suits, actions, allegations or
                                            charges that could affect your or Concreet's ability to fully perform their respective duties or to
                                            exercise its rights under these Terms. You shall not, without the prior written approval of
                                            Concreet, settle, dispose or enter into any proposed settlement or resolution of any Claim
                                            (whether having been finally adjudicated or otherwise) brought against you, if such settlement
                                            or resolution results in any obligation or liability for Concreet.
                    						</li>
                                    </ol>
                                </li>

                                <li><b>DISCLAIMER OF WARRANTIES.</b>
                                    YOU EXPRESSLY UNDERSTAND AND AGREE THAT:
                    				<ol className="counting">
                                        <li>CONCREET IS PROVIDING THE SITE, THE SERVICES, ALL INFORMATION AND MATERIALS
                    						(INCLUDING ALL USER INFORMATION AND CONCREET CONTENT) ON AN AS-IS AND ASAVAILABLE
                    						BASIS WITHOUT WARRANTIES OF ANY KIND. CONCREET AND ITS LICENSORS
                    						DISCLAIM ALL REPRESENTATIONS AND WARRANTIES OF ANY KIND, EXPRESS, IMPLIED OR
                    						STATUTORY, TO THE EXTENT THAT THEY MAY BE EXCLUDED BY LAW, INCLUDING, BUT NOT
                    						LIMITED TO, WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
                    						NON-INFRINGEMENT, COURSE OF DEALING, COURSE OF PERFORMANCE, AND NONINFRINGEMENT.
                    						FURTHERMORE, YOU ACKNOWLEDGE AND AGREE THAT CONCREET AND ITS
                    						LICENSORS MAKE NO REPRESENTATIONS AND WARRANTIES CONCERNING (I) REGARDING THE
                    						SECURITY, ACCURACY, RELIABILITY, COMPLETENESS, TIMELINESS, USEFULNESS, AND
                    						PERFORMANCE OF THE SITE AND SERVICES; (II) THAT THE ENTIRETY OR ANY PORTION OF THE
                    						SITE OR THE SERVICES WILL BE ERROR-FREE, SECURE, TIMELY, OR UNINTERRUPTED OR OPERATE
                    						IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA; (III) THAT THE
                    						RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SITE OR SERVICES, OR THE
                    						INFORMATION, DATA AND MATERIALS THEREIN WILL BE ACCURATE, RELIABLE, OR AVAILABLE,
                    						(VI) THAT ANY ERRORS IN THE SITE OR SERVICES OR IN ANY INFORMATION OR MATERIALS THEREIN WILL BE CORRECTED, (V) REGARDING THE SAFETY OR LEGALTY OF THE SITE, SERVICES,
                    						OR ANY TRANSACTION ENTERED INTO IN CONNECTION THERETO; OR (VI) THE SITE AND ITS
                    						CONTENTS AND SERVERS AND SYSTEMS THAT MAKES THE SITE AVAILABLE ARE FREE OF ANY
                    						VIRUSES OR OTHER HARMFUL COMPONENTS.
                    					</li>

                                        <li>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM
                    						CONCREET OR FROM YOUR USE OF THE SITE OR SERVICES, SHALL CREATE ANY WARRANTY NOT
                    						EXPRESSLY STATED IN THESE TERMS.
                    					</li>

                                        <li>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN
                    						WARRANTIES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.
                    					</li>
                                    </ol>
                                </li>

                                <li><b>LIMITATION OF LIABILITY</b>
                                    <ol className="counting">
                                        <li>YOU ASSUME TOTAL RESPONSIBILITY AND RISK FOR YOUR USE OF THE SITE AND SERVICES
                    						AND ANY MATERIAL, INFORMATION, GOODS OR SERVICES YOU OBTAIN THROUGH THE USE OF
                    						THE SITE AND SERVICES. YOU EXPRESSLY AGREE THAT YOUR USE OF THE SITE AND SERVICES,
                    						AND YOUR ENTRY INTO ANY TRANSACTION IN CONNECTION THERETO, IS AT YOUR SOLE RISK,
                    						AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE OR HARM TO YOU OR ANY OF
                    						YOUR PROPERTY AS A RESULT.
                    					</li>

                                        <li>IN NO EVENT SHALL CONCREET, ITS AFFILIATES, AND ITS AND THEIR MEMBERS,
                    						MANAGERS, OFFICERS, DIRECTORS, EMPLOYEES, SHAREHOLDERS, ASSIGNS AND SUCCESSORS,
                    						AGENTS, REPRESENTATIVES, AND RELEVANT THIRD PARTIES INCLUDING ITS AND THEIR SERVICE
                    						PROVIDERS, DISTRIBUTION PARTNERS, REFERRAL PARTNERS, ADVERTISERS, LICENSORS,
                    						LICENSEES, CONSULTANTS AND CONTRACTORS (<b>AFFILIATED ENTITIES</b>) BE LIABLE, WHETHER IN
                    						CONTRACT, TORT (INCLUDING WITHOUT LIMITATION NEGLIGENCE) OR OTHERWISE FOR ANY
                    						INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, EXEMPLARY OR CONSEQUENTIAL DAMAGES,
                    						INCLUDING, BUT NOT LIMITED TO, LOSS OF DATA OR OTHER INTANGIBLES, INCOME OR PROFIT,
                    						LOSS OF OR DAMAGE TO PROPERTY OR CLAIMS OF THIRD PARTIES, EVEN IF CONCREET HAS BEEN
                    						ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, IN CONNECTION WITH ANY CLAIM, SUIT, OR
                    						OTHER MATTER, INCLUDING BUT NOT LIMITED TO ANY CLAIM, SUIT, OR OTHER MATTER,
                    						RELATING TO, ARISING FROM OR IN CONNECTION WITH THESE TERMS, THE SITE, THE SERVICES,
                    						OR ANY TRANSACTION RELATED THERETO, ANY COMMUNICATION BY OR BETWEEN YOU AND
                    						ANY THIRD PARTY (INCLUDING ANY OTHER USER), OR ANY INFORMATION OR MATERIALS MADE
                    						AVAILABLE BY CONCREET IN CONNECTION WITH YOUR USE OF THE SITE AND SERVICES.
                    					</li>

                                        <li>YOUR SOLE AND EXCLUSIVE REMEDY FOR DISSATISFACTION WITH THE SITE OR SERVICES
                    						IS TO STOP USING THE SITE AND SERVICES. THE MAXIMUM LIABILITY OF CONCREET AND THE
                    						AFFILIATED ENTITIES FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION, WHETHER IN
                    						CONTRACT, TORT (INCLUDING WITHOUT LIMITATION NEGLIGENCE) OR OTHERWISE WILL BE THE
                    						TOTAL AMOUNT, IF ANY, PAID BY YOU TO CONCREET IN THE PRECEDING TWELVE (12) MONTHS
                    						IN CONNECTION WITH THE SITE OR ANY SERVICE.</li>

                                        <li>You acknowledge that the limitations set forth above are an essential basis of the bargain
                    						and of the allocation of risks between the parties. Some states or jurisdictions do not allow the
                    						limitation or exclusion of liability in the manner described above. Accordingly, some of the above
                    						limitations may not apply to you. If you live in a jurisdiction whose laws prevent you from taking
                    						full responsibility and risk for your Use of the Site and Services in accordance with these Terms,
                    						the liability of Concreet and the Affiliated parties is limited to the greatest extent allowed by the
                    						laws of that jurisdiction.
                    					</li>
                                    </ol>
                                </li>

                                <li>
                                    <b>INTELLECTUAL PROPERTY</b>
                                    <ol className="counting">
                                        <li><b>Concreet Content.</b> As between you and Concreet, all right, title, and interest, including
                    						without limitation all patent, trademark, copyright, trade secret, and other intellectual property
                    						rights, in and to the Site, the Services, all User Information not provided directly by you, all
                    						material on the Site or contained therein, all text, graphics, and other works on the Site, the Sites
                    						design and coding, all computer programs used and licensed in connection with the Site, the look
                    						and feel of the Site, and all data and reports generated by the Site (collectively, Concreet
                    						Content) are owned by Concreet or its licensors. These materials are protected under
                    						copyright, trademark and other intellectual property and other laws.
                    				   </li>

                                        <li>
                                            <b>Concreet Marks.</b> All product and service names and other terms, logos, or symbols used
                    						as source indicators for goods and/or services of Concreet or its subsidiaries or affiliates,
                    						including without limitation the term Concreet, or a third party, as the case may be, are
                    						registered or unregistered trademarks or service marks owned by the applicable entity
                    						(collectively, Marks). The absence of a name, trademark or logo in this list does not constitute
                    						a waiver of any and all intellectual property rights that Concreet or a third party has established
                    						in any of its goods, services, or Marks. All Marks used in connection with the Site and Services
                    						are the property of Concreet or the applicable third parties, as the case may be, and shall remain
                    						the property of Concreet and such third parties. Nothing contained in the Site shall be construed
                    						as granting, by implication or otherwise, any license or right to use any such Mark without the
                    						prior written permission of Concreet or such third party that may own such Mark. Your misuse
                    						of any such Mark, or any other Concreet Content, is strictly prohibited.
                    					</li>

                                        <li>
                                            <b>Feedback and Developments.</b> You may make suggestions for changes, modifications or
                    						improvements (<b>Feedback</b>) to the Site, the Services, any Concreet Content or to any other
                    						products, services, technologies, platforms, and documentation that Concreet may provide. All
                    						Feedback shall be solely owned by Concreet (including all intellectual property rights therein and
                    						thereto). You shall and hereby do make all assignments necessary to achieve such ownership.
                    						Also, Concreet shall own any developments and/or enhancements to the Site and Services or to
                    						any other Concreet products, services, technologies, platforms, documentation, et al resulting
                    						from access to and use of your User Information.
                    					</li>

                                        <li><b>Aggregated Data.</b> You acknowledge and agree that Concreet is free to disclose aggregate,
                    						anonymized measures of usage of the Site and Services by Users (including yourself), and to reuse
                    						all general knowledge, experience, know-how, works and technologies (including ideas,
                    						concepts, processes and techniques) acquired during its operation of the Site and Services. You
                    						further agree that (a) Concreet shall have the right to create compilations and analyses of any
                    						information produced by, obtained through or otherwise related to the Site and Services or any
                    						other Concreet products, services, technologies, platforms, or documentation (<b>Aggregate
                    						Data</b>), and to create reports, studies, analyses and other work product from Aggregate Data
                    						(<b>Analyses</b>), and (b) Concreet shall have exclusive ownership rights to, and the exclusive right
                    						to use, such Aggregate Data and Analyses for any purpose; provided, however, that Concreet
                    						shall not distribute any Aggregate Data or Analyses in a manner that identifies any portion of
                    						such as sourced from or based on you or your User Information
                    					</li>
                                    </ol>
                                </li>

                                <li><b>MODIFICATION AND TERMINATION.</b>
                                    <ol className="counting">
                                        <li>You acknowledge and agree that Concreet, in our sole discretion, with or without notice,
                    					and for any or no reason, may refuse, move or remove any or all User Information you Transmit
                    					to the Site, suspend your account for any length of time, delete or alter your Account, refuse you
                    					any and all current or future Use of the Site and Services (or any portion thereof). Concreet may
                    					also in its sole discretion and at any time modify or discontinue providing the Site or any part
                    					thereof, or any or all of the Services with or without notice. You agree that any modification or
                    					termination of your access to the Site and Services under any provision of these Terms may be
                    					effected without prior notice, and acknowledge and agree that Concreet may immediately bar
                    					any further access to the Site. Further, you agree that Concreet shall have no responsibility or
                    					liability to you or any third party for any modification or termination to any portion of the Site or
                    					Services (including the fact that any portion of the Site or Services is operable and accessible),
                    					your Account or your access to the Site or the Services, or for the failure of the Site and the
                    					deletion of any User Information, Concreet Content or other content maintained or transmitted
                    					by Concreet.</li>
                                        <li>In the event these Terms are revised at any time for any reason, Concreet may provide
                    					you notice of these changes by any reasonable means, including by posting the revised version
                    					of the Terms on the Site. By Using the Site following the posting of changes to these Terms, you
                    					accept such changes.</li>
                                    </ol>
                                </li>

                                <li><b>GENERAL.</b>
                                    <ol className="counting">
                                        <li>Governing Law and Venue. These Terms and the relationship between you and Concreet
                    						shall be governed by and construed in accordance with the laws of the State of New York without
                    						regard to its conflicts of laws analysis. For all legal proceedings arising out of any Use of the Site
                    						or any Service and/or relating to these Terms, you and Concreet hereby irrevocably and
                    						unconditionally submit to the exclusive jurisdiction of courts located in the City of New York, NY,
                    						and the parties consent to the personal jurisdiction of such courts and expressly waive any right
                    						they may otherwise have to cause any such action or proceeding to be brought or tried
                    						elsewhere. You and Concreet irrevocably waive, to the fullest extent permitted by law, any
                    						objection that you/it may now or hereafter have to the laying of the venue of any proceeding
                    						brought in any such court or any claim that a legal proceeding commenced in such court has been
                    						brought in an inconvenient forum. In addition, you hereby agree to service of process on you by
                    						e-mail to the address you have provided to Concreet for your Account, if any, and by any other
                    						means permitted by law.
                    					</li>
                                        <li><b>Notices.</b> All notices, demands, or consents given by you under these Terms will be in
                    						writing and will be deemed given when delivered to Concreet at the following email address:
                    						Support@Concreet.com. Any notices to you may be made by either (i) email, registered mail,
                    						courier, or by postal mail to the email or postal address in your Account or otherwise in
                    						Concreets records or (ii) if a general notice to all or a large group of Users, by posting such
                    						notification on the Site. It is solely your responsibility to keep your email and postal address
                    						information accurate, complete, and current, and Concreet shall have no responsibility for or
                    						liability for anything resulting from such information being inaccurate, incomplete, or out of date.
                    					</li>
                                        <li><b>Relationship of the Parties.</b> The relationship between you and Concreet is that of
                    							independent contractors. Nothing contained in these Terms shall be construed as creating any
                    							agency, partnership, joint venture or other form of joint enterprise, employment or fiduciary
                    							relationship between you and Concreet, and, unless Concreet expressly agrees otherwise in a
                    							separate agreement or communication with or to you, neither you nor Concreet shall have any
                    							authority to contract for or bind the other party in any manner whatsoever.
                    					</li>
                                        <li><b>Authority.</b> You hereby represent and warrant to Concreet that: (i) you have all the
                    						requisite power and authority, corporate or otherwise, to enter into the binding contract created
                    						by these Terms, conduct yourself and your business and to execute, deliver, and perform all of
                    						your obligations under these Terms; (ii) you have the right to submit and use your User
                    						Information in the manner you have done so to or through your Account and/or otherwise to,
                    						through, or in connection with the Site and Services; (iii) you have the right to grant the
                    						permissions you agree to grant under these Terms; (iv) your performance under these Terms
                    						and/or the rights granted herein do not and will not conflict with or result in a breach or violation
                    						of any of the terms or provisions, or constitute a default under any contract or agreement, to
                    						which you are currently bound or will become bound in the future; and (v) your performance
                    						under these Terms will comply with all applicable laws, rules and regulations (including, without
                    						limitation, Fair Housing, export control, privacy and obscenity laws), domestic or foreign.
                    					</li>

                                        <li><b>Assignment.</b> You may not assign or transfer these Terms, in whole or in part, without the
                    						prior written consent of Concreet, which may be granted or withheld by Concreet in its sole
                    						discretion. Any attempted assignment in violation of this Section will be null and void and of no
                    						force or effect. Concreet may assign these Terms freely at any time without notice. Subject to
                    						the foregoing, these Terms will bind and inure to the benefit of each party's permitted successors
                    						and assigns.
                    					</li>

                                        <li><b>Waiver.</b> The failure to exercise or enforce any right or provision shall not affect Concreet's
                    						right to exercise or enforce such right or provision at any time thereafter, nor shall a waiver of
                    						any breach or default of these Terms constitute a waiver of any subsequent breach or default or
                    						a waiver of the provision itself. Concreet shall have no responsibility or liability to you for its
                    						failure to, or decision not to, enforce any right or provision in these Terms or in any other
                    						agreement between itself and any third party or its decision to grant a waiver of any provision of
                    						these Terms or any other agreement it is party to.
                    					</li>

                                        <li><b>Severability.</b> If any provision of these Terms is held by a court or arbitrator of competent
                    						jurisdiction not enforceable to its full extent, then such provision shall be enforced to the
                    						maximum extent permitted by law. If any provision of these Terms shall be held to be invalid,
                    						illegal, unenforceable or in conflict with the law of any jurisdiction, the validity, legality and
                    						enforceability of the remaining provisions shall not in any way be affected or impaired thereby.
                    				    </li>

                                        <li><b>Entire Agreement.</b> These Terms, the Privacy Policy, and any Additional Requirements
                    						applicable to you, constitute the entire and exclusive understanding and agreement between
                    						you and Concreet regarding this subject matter, and supersede any and all prior or
                    						contemporaneous agreements or understandings, written and oral, between you and Concreet
                    						relating to this subject matter.
                    					</li>

                                        <li><b>Headings.</b> The section headings and sub-headings contained in these Terms are for
                    					convenience only and have no legal or contractual effect.
                    					</li>

                                        <li><b>Electronic Signatures.</b> If you choose to use the electronic signature agreement execution
                    						functionality made availability on or through this Site, you understand, acknowledge and agree
                    						that you have carefully reviewed the disclosure relating to use of your electronic signature to
                    						execute an agreement and will not execute an agreement with your electronic signature without
                    						providing your consent to use your electronic signature. You agree that you will rely on your own
                    						legal counsel to determine the sufficiency of the electronic signature execution of the lease and
                    						its enforceability and that Concreet makes no warranty or other representation with respect to
                    						the sufficiency of the electronic signature execution of such agreement under applicable, federal,
                    						state, or local laws or regulations.
                    				    </li>

                                        <li><b>Special Admonitions for International Use.</b> The Site is hosted in the United States and is
                    						intended for Use by residents of the United States of America only. All matters relating to the
                    						Site are governed exclusively by the laws of the State of New York in the United States of America
                    						and not the jurisdiction in which you are located. If you are located outside of the United States
                    						of America and you contact us, please be advised that any information you provide to us will be
                    						transferred to the United States of America and that by submitting information, you explicitly
                    						authorize such transfer.
                    					</li>

                                        <li><b>Support.</b> If you have any questions or concerns about the Site or Services or any part of
                    						these Terms, including the Privacy Policy, please contact Concreet at Support@Concreet.com
                    					</li>

                                    </ol>
                                </li>


                            </ol>

                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={toggle} color="primary">Close</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default TermsDialog;
