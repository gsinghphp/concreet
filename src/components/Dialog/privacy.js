import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class PrivacyDialog extends React.Component {

    static defaultProps = {
        open: false,
        toggle: () => { }
    }

    render() {
        const { open, toggle } = this.props;
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={toggle}
                    aria-labelledby="scroll-dialog-title"
                >
                    <DialogTitle id="scroll-dialog-title"><h4 className="top-title">Concreet Privacy Policy</h4></DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <ol className="counting pl-10">
                                <li> <b>Introduction</b>
                                    <p>Concreet Technologies LLC, a Delaware limited liability company ("<b>Concreet</b>" or "<b>We</b>"), respects your privacy and is committed to protecting it through our compliance with this policy (the <b>Privacy Policy</b>). This Privacy Policy describes the types of information we may collect from you or that you may provide when you visit the website <b>www.Concreet.com</b> (our "<b>Website</b>") and our practices for collecting, using, maintaining, protecting, and disclosing that information. Please note that this Privacy Policy, as updated, is incorporated in its entirety into, and forms part of, the Websiteâ€™s Terms of Service [LINK: TERMS OF SERVICE].</p>
                                    <p>For the avoidance of doubt, this Privacy Policy applies whether you are a visitor to the Website, you are someone using the Website and the services made available through the Website to apply to rent or lease a personal residence (such as an apartment or other domicile), whether you are supporting the rental or lease application of another person (for instance, as a guarantor of an applicant), whether you are someone using the Website to accomplish certain transactions (e.g. paying rent) or handle certain other matters in connection with a rental, whether you are using the Website in your capacity as a real-estate broker or agent (whether on behalf of a would-be or current tenant or on behalf of a Landlord, a <b>Broker</b>), or whether you are using the Website in your capacity as, or at the direction of, the owner, manager or operator of a personal residence which is being made available for rent or lease through the Website or in connection with which the Website is being used as a means by which a current or future tenant and a landlord can handle certain transactions or other matters (e.g. paying rent or providing certain notifications required by law to a tenant)(any such user, a <b>Landlord</b>).</p>
                                    <ol className="counting">
                                        <li>This Privacy Policy applies to information we collect or make available:
                               <ol className="counting">
                                                <li>on this Website;</li>
                                                <li>in email, text, and other electronic messages between you and this Website;</li>
                                                <li>in and from documents, reproductions of documents, digital images, and other materials that you may upload to the Website or otherwise provide to Concreet;</li>
                                                <li>through an opt-in newsletter or other opt-in online publication Concreet may offer in connection with this Website or its services but only if you choose to sign-up for that newsletter or other publication;</li>
                                                <li>from third-party services that provide may provide background information or credit information about you (such services, <b>Background Checking Services</b>), but only after you expressly authorize the sharing of this information with Concreet;</li>
                                                <li>when you interact with our advertising and applications on third-party websites and services, if those applications or advertising include links to this Privacy Policy.</li>
                                                <p>Except to the extent it states otherwise, this Privacy Policy does not apply to information collected by:</p>
                                                <li>Concreet offline or through any other means, including on any other website operated by Concreet or any third party; or</li>
                                                <li>any third party, including through any application or content that may link to or be accessible from or on the Website.</li>
                                            </ol>
                                        </li>
                                        <p>Please read this Privacy Policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use the Website. By accessing or using this Website, you agree to this Privacy Policy. This Privacy Policy may change from time to time. Your continued use of this Website after we make changes is deemed to be acceptance of those changes, so please check the Privacy Policy periodically for updates.</p>
                                    </ol>
                                </li>
                                <li><b>Children Under the Age of 18</b>
                                    <p>The Website is not intended for children under 18 years of age. No one under age 18 may provide any information to or on the Website. We do not knowingly collect Personal Information from children under 18. If you are under 18, do not: use or provide any information on this Website or on or through any of its features, register on the Website, make any purchases through the Website, use any of the interactive or public comment features of this Website or provide any information about yourself to us, including but not limited to your name, address, telephone number, email address, or any screen name or user name you may use. If we learn we have collected or received Personal Information from a child under 18 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 18, please contact us at <span>Support@Concreet.com</span>.</p>
                                </li>
                                <li><b>Information Provided by or About You That We Collect</b>
                                    <p>We collect several types of information and, in some instances, documents, from and about users of the Website, including:</p>
                                    <ol className="counting">
                                        <li>if and when you establish an account on the Website, whether or not as a prerequisite to your further use of the Website and our services, information by which you may be personally identified ("<b>Personal Information</b>") including, but not necessarily limited to: your first and last name, your email address, your phone number, your social security number, your date of birth, and, if you are a Broker, your REBNY ID Number;</li>
                                        <li>in the event that you use the Website to apply to rent or lease a residence or to support the application of someone else applying to rent or lease a residence (for instance, as a guarantor), we may collect additional Personal Information about you including but not necessarily limited to: the address of your current residence (and potentially prior residences as well), a clear and accurate digital scan or photograph of your driverâ€™s license (should you have one), a clear and accurate digital scan or photograph of your passport (if required); your criminal history, information verifying your prior addresses, information about any instances in which you were evicted from a domicile you were renting or leasing, information verifying your driverâ€™s license (if you have one), your employment history, your income information, bank account statements, brokerage account statements, tax information, and copies of your pay stubs and/or similar documentation; provided, however, that Concreet will not collect or retain any credit profile information provided by a Background Checking Service (for example, Experian). Instead, Concreet will make a â€˜childâ€™ web browser window accessible through the Website to those persons (such as a particular Broker or Landlord) whom you have authorized to view such information;</li>
                                        <li>in the event, you complete any payment transaction on the Website (for example, payment of a fee related to a rental application or the payment of an amount due under your rental agreement) through the Website, transaction information such as, but not necessarily limited to, billing address, fulfillment instructions (if any), and credit card or payment card information (such information may in part or in whole constitute Personal Information); provided, however, that Concreet will not retain any credit card or payment card information that would require the Website to be PCI-compliant;</li>
                                        <li>records and copies of your correspondence (including email addresses), if you contact us;</li>
                                        <li>your responses to surveys that we might ask you to complete for research purposes;</li>
                                        <li>your search queries on the Website;</li>
                                        <li>any information you send to third parties through the Website;</li>
                                        <li>information about the computer you use to access the Website, any related equipment and your internet connection, including your IP address, operating system, and browser type;</li>
                                        <li>information about how you use and access the Website, which may include Website traffic data, location data, logs, and other communication data and the resources that you access and use on the Website.</li>
                                    </ol>
                                </li>
                                <li><b>How We Collect Information About You</b>
                                    <p>We collect information about you:</p>
                                    <ol className="counting">
                                        <li>directly from you when you provide it to us including, but not limited to, when you register for the Website, apply to rent or lease a residence, complete a transaction, run search queries on the Website, correspond with Concreet, or respond to one of our surveys;</li>
                                        <li>automatically as you use and navigate through the Website;</li>
                                        <li>automatically if you choose to use an online payment service to complete any payment transaction on or through the Website. Please note that this Privacy Policy does not apply to any Personal Information or other information that you provide to an online payment service except to the extent that information is transferred to the Website;</li>
                                        <li>through the use of automatic data collection technologies, such as browser cookies;</li>
                                        <li>from third parties, for example, our business partners. As indicated above, in no event will Concreet obtain Personal Information about you from a Background Checking Service without first obtaining your authorization, whether pursuant to Concreetâ€™s Terms of Service, your logging on to the website or mobile application operated by a Background Checking Service in order to authorize such Personal Information sharing, or through a portal or child window accessible on the Website through which you are able to authorize a Background Checking Service to share Personal Information with Concreet; and/or,</li>
                                        <li>directly from you if and when you upload documents (including but not limited to documents containing Personal Information about yourself) to the Website.</li>
                                        <p>In the event that Concreet obtains any Personal Information about you from a Background Checking Service, you will be able to view that information when you access your account profile on the Website.</p>
                                    </ol>
                                </li>
                                <li> <b>Information We Collect Through Automatic Data Collection Technologies; Do Not Track Signals.</b>
                                    <p>As stated above, we may use automatic data collection technologies to collect certain information including, but not necessarily limited to, information about or in connection with you, your activities on the Website or otherwise, the computer you use to access the Website, your browser and your internet connection.</p>
                                    <p>The technologies we use for this automatic data collection include browser cookies (commonly known as <b>cookies</b>). A cookie is a small file placed on the hard drive of your computer. The information we collect automatically may include Personal Information or we may maintain it or associate it with Personal Information we collect in other ways or receive from third parties. This helps us to improve the Website and to deliver a better and more personalized service, including but not limited to enabling us to: estimate our audience size and usage patterns; store information about your preferences, allowing us to customize the Website according to your individual interests and needs; speed up your searches; and, recognize you when you return to the Website. <b>Please note that if you refuse to accept the cookies on the Website by
                                                   selecting the applicable settings on your browser, you will not be able to log on to the Website and the
                                          Website will not function as intended for you.</b>
                                    </p>
                                    <p> <b>In addition, please note that the Website will not respond or operate differently in any way in response
                                                       to any Do-Not-Track signals it receives from your web browser. If that is not acceptable to you, please
                                                       immediately terminate your Concreet account and discontinue any further use of the Website.
                                      </b>
                                    </p>
                                </li>
                                <li> <b>Third-Party Use of Cookies and Other Tracking Technologies.</b>
                                    <p>Some content or applications on the Website are served by third-parties, including content providers, and application providers. These third parties may use cookies alone or in possibly conjunction other tracking technologies to collect information about you when you use the Website. The information these third parties collect may be associated with your Personal Information or they may collect information, including Personal Information, about your online activities over time and across different websites and other online services. They may use this information to provide you with interest-based (behavioral) advertising or other targeted content.</p>
                                    <p>We do not control these third parties' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly. For information about how you can opt out of receiving targeted advertising from many providers, see [LINK: CHOICES ABOUT HOW WE USE AND DISCLOSE YOUR INFORMATION].</p>
                                </li>
                                <li> <b>How We Use Your Information</b>
                                    <p>We use information that we collect about you or that you provide to us, including any Personal Information:</p>
                                    <ol className="counting">
                                        <li>to present the Website and its contents to you;</li>
                                        <li>to provide you with information or services that you request from us;</li>
                                        <li>in the case of your Personal Information, to share with those Brokers and Landlords that you have specifically authorized in connection with a rental application (whether your own or in connection with another userâ€™s rental application) and for no other purpose;</li>
                                        <li>in the case of your Personal Information, to share with your current Landlord solely to the extent required in order to complete a transaction through the Website that you have authorized (for instance, making a rent payment);</li>
                                        <li>to fulfill any other purpose for which you provide it;</li>
                                        <li>to provide you with notices about your account, including any expiration and renewal notices;</li>
                                        <li>to carry out our obligations and enforce our rights arising from any agreements entered into between you and us, including for billing and collection;</li>
                                        <li>to notify you about changes to the Website or any products or services we may offer or provide though it;</li>
                                        <li>to allow you to participate in interactive features on the Website;</li>
                                        <li>to provide to our third-party vendors who assist us with operating, maintaining, and improving the Website by providing us with services, such as email delivery, operation of discussion forums, other information technology services, payment processing services and other services, and who receive your data for the limited purposes of providing us with such services;</li>
                                        <li>to promote on the Website or to contact you about our own services and products that we believe may be of interest to you. If you do not want us to use your information in this way, please check the relevant box located on the form on which we collect your data when you register for the Website or adjust your user preferences in your account profile. For more information, see [LINK: CHOICES ABOUT HOW WE USE AND DISCLOSE YOUR INFORMATION];</li>
                                        <li>in any other way we may describe when you provide the information; and/or</li>
                                        <li>for any other purpose with your consent.</li>
                                    </ol>
                                </li>
                                <li><b>Disclosure of Your Information</b>
                                    <ol className="counting">
                                        <li>We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.</li>
                                        <li>We may disclose Personal Information that we collect or you provide as described in this Privacy Policy:
                               <ol className="counting">
                                                <li>to our subsidiaries and affiliates;</li>
                                                <li>to contractors, service providers, and other third parties we use to support our business and who are bound by contractual obligations to keep Personal Information confidential and use it only for the purposes for which we disclose it to them;</li>
                                                <li>to a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of Concreetâ€™s assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which Personal Information held by Concreet about the Websiteâ€™s users is among the assets transferred;</li>
                                                <li>to a prospective Broker or Landlord or your current Landlord as described in Sections 7.3 and 7.4 above.</li>
                                                <li>to fulfill the purpose for which you provide it. For example, if you give us an email address to use the "email a friend" feature of the Website, we will transmit the contents of that email and your email address to the recipients;</li>
                                                <li>for any other purpose disclosed by us when you provide the information; and/or</li>
                                                <li>otherwise with your consent.</li>
                                            </ol>
                                            <li>We may also disclose your Personal Information:
                                 <ol className="counting">
                                                    <li>to comply with any court order, law, or legal process, including to respond to any government or regulatory request;</li>
                                                    <li>to enforce or apply our Terms of Service, this Privacy Policy, or to carry out our obligations and enforce our rights arising from any agreements entered into between you and us, including any obligations and rights related to billing and collection; and/or</li>
                                                    <li>If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of Concreet, our customers, or others. This may include exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.</li>
                                                </ol>
                                            </li>
                                        </li>
                                    </ol>
                                </li>
                                <li><b>Choices About How We Use and Disclose Your Information</b>
                                    <p>We strive to provide you with choices regarding the Personal Information you provide to us. We have created mechanisms to provide you with the following control over your information:</p>
                                    <ol className="counting">
                                        <li><b>Promotional Offers from Concreet.</b>If you do not wish to have your contact information used by Concreet to promote our products or services, you can opt-out by checking the relevant box located on the form on which we collect your data when you sign up you register to use the Website or when you choose to opt-in to any online newsletter or other online publication we may offer, or by logging into the Website at any time and adjusting your user preferences in your account profile by checking or unchecking the relevant boxes or by sending us an email stating your request to Support@Concreet.com. Also, if we have sent you a promotional email, you may select the â€˜unsubscribeâ€™ link in that or send us a return email asking to be omitted from future email distributions. This opt-out does not apply to information provided to or by Concreet in connection with any rental or lease application you make through the Website, any application to be a guarantor of another person that you make through the Website, any transaction you enter into through the Website, any product or service purchases, any support for any product or service you have purchased or applied for through the Website, any product or service experiences or any other transactions.</li>
                                        <p>Also, as stated above, please note that we do not control third parties' collection or use of your information to serve interest-based advertising. However, those third parties may provide you with ways to choose not to have your information collected or used in this way. You can opt out of receiving targeted ads from members of the Network Advertising Initiative ("<b>NAI</b>") on the NAI's website.</p>
                                    </ol>
                                </li>
                                <li> <b>Accessing and Correcting Your Information</b>
                                    <ol className="counting">
                                        <li>You can review your Personal Information, including but not limited to any Personal Information that Concreet has obtained about you from a third party, by logging into the Website and visiting your account profile page.</li>
                                        <li>If you have reason to believe that any of the Personal Information entered directly by you into the Website is (as shown in your account profile) inaccurate, erroneous, or incomplete, you will have the ability to correct that Personal Information through your account profile page.</li>
                                        <li>If you have reason to believe that any of your Personal Information not entered directly by you into the Website (e.g. Personal Information obtained from a Background Checking Service or from a document or reproduction of a document you have uploaded to the Website) is inaccurate, erroneous, and/or incomplete, you have the right to dispute such Personal Information by emailing Concreet at <span>Support@Concreet.com</span> with a description of which of your Personal Information is inaccurate, erroneous, and/or incomplete, how such Personal Information is inaccurate, erroneous, and/or incomplete, and how such Personal Information should be corrected.</li>
                                        <li>In the event that you dispute the accuracy or completeness of any of your Personal Information, Concreet reserves the right:
                               <ol className="counting">
                                                <li>To request you provide, or to authorize Concreet to obtain from one or more third parties, documents, information, and other material to support your assertion, and</li>
                                                <li>in the case of any Personal Information obtained from a Background Checking Service or other third party, submit to and be governed by any such information dispute procedures such third party may have in place.
                                   <p>Furthermore, you acknowledge and agree that Concreet has the right, in its sole discretion and notwithstanding any determinations made by any other party, to make any final determination about whether any of your Personal Information is or is not inaccurate, erroneous, and/or incomplete as for purposes of the services offered through this Website, should or should not altered in any way.</p>
                                                </li>
                                            </ol>
                                            <li>While you can request that certain of your Personal Information be deleted, you acknowledge that, in various instances, this may not be possible without also deleting your user account. Furthermore, Concreet may not accommodate a request to change or delete any or all of your Personal Information if we believe the change or deletion could be considered a violation of any applicable law, statute, regulation, ordinance, judgment, decision or other legal requirement, or cause your Personal Information to be incorrect or erroneous or to include or increase any material omission.</li>
                                        </li>
                                    </ol>
                                </li>
                                <li> <b>Your California Privacy Rights</b>
                                    <p>California Civil Code Section Â§ 1798.83 permits users of the Website that are California residents to request certain information regarding our disclosure of Personal Information to third parties for their direct marketing purposes. To make such a request, please send an email to
                             <span>Support@Concreet.com.</span></p>
                                </li>

                                <li>
                                    <b>Data Security</b>
                                    <p>We have implemented measures designed to secure your Personal Information from accidental loss and
                                            from unauthorized access, use, alteration, and disclosure. Concreet is in full compliance with all FCRA
                                            requirements. All Personal Information and any other information related to payment transactions will be
                                            protected by industry-standard encryption and data protection measures at all times. Any payment card
                                            information you provide to the Website will be collected by a third-party payment processor and will not
                                          be stored on Concreetâ€™s computer systems.</p>
                                    <p>The safety and security of your information also depends on you. Where we have given you (or where you
                                            have chosen) a password for access to certain parts of the Website, you are responsible for keeping this
                                            password confidential. We ask you not to share your password with anyone. We urge you to be careful
                                            about giving out information in public areas of the Website like message boards. The information you
                                          share in public areas may be viewed by any user of the Website.</p>

                                    <p>Unfortunately, the transmission of information via the Internet is not completely secure. Although we do
                                            our best to protect your Personal Information, we cannot guarantee the security of your Personal
                                            Information transmitted to the Website. Any transmission of Personal Information is at your own risk. We
                                            are not responsible for circumvention of any privacy settings or security measures contained on the
                                            Website.
                                  </p>
                                </li>

                                <li>
                                    <b>Retention of Personal Information</b>
                                    <p>Certain of your Personal Information required for the maintenance of your account (including but not
                                            necessarily limited to information such as your name, date of birth, email address, login credentials and
                                            passwords for the Website) shall be retained until you or Concreet chooses to delete your account on the
                                            Website. Furthermore, Concreet reserves the right to retain certain Personal Information indefinitely in
                                            connection with Concreetâ€™s books and records, such as a customer list or a record of transactions
                                            conducted through the Website. Other than the Personal Information described in the preceding
                                            sentences, all your Personal Information shall be automatically deleted from the Website following thirty
                                            (30) continuous days of your non-use of the Website unless (i) Concreet is required to do otherwise by
                                            any applicable law, statute, regulation, ordinance, judgment, decision or other legal requirement, or (ii)
                                            Concreet has reason to believe retaining some or all of such Personal Information is necessary or
                                            appropriate to protect the rights, property, or safety of Concreet, our customers, or others, including but
                                            not limited for the purposes of fraud protection and credit risk reduction. In addition, certain of your
                                            payment-related Personal Information will not be deleted for as long as you have selected to make any
                                          recurring rent or lease payments or any other recurring payments through the Website.</p>
                                </li>

                                <li>
                                    <b>Changes to Our Privacy Policy</b>
                                    <p>Any changes made to this Privacy Policy will be posted on this page along with a notice that the Privacy
                                            Policy has been updated on the Website home page. If we make material changes to how we treat our
                                            users' Personal Information, we will notify you by email to the email address specified in your account
                                            and/or through a notice on the Website home page. The date the Privacy Policy was last revised is
                                            identified at the top of the page. You are responsible for ensuring we have an up-to-date active and
                                            deliverable email address for you, and for periodically visiting the Website and this Privacy Policy to check
                                          for any changes.</p>

                                </li>

                                <li><b>Contact Information</b>
                                    <p>To ask questions or comment about this Privacy Policy and our privacy practices, contact us at any time
                                      at Support@Concreet.com.</p>
                                </li>


                            </ol>

                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={toggle} color="primary">
                            Accept
            </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default PrivacyDialog;
