import React from "react";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Dialog from '@material-ui/core/Dialog';
import MuiDialogContent from '@material-ui/core/DialogContent';
import AddBankAccount from '../../container/BankAccount/Add';
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  formControl: {
    minWidth: "100%",
    textAlign: "left"
  },
  icon: {
    fill: "#2F7AC9"
  },
  selectEmpty: {
    fontSize: 18,
    color: "#2F7AC9",
    "&:before": {
      borderBottom: "2px solid #2F7AC9"
    },
    "&:hover": {
      borderColor: "#2F7AC9"
    },
    "&:hover:before": {
      borderBottom: "2px solid #2F7AC9 !important"
    }
  }
});

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);

const MySelect = ({ onCloseDialog, placeholder, classes, values, onChange, add, addText, value, name, disabled, dialog, validation }) => (
  <>
    <FormControl className={classes.formControl}>
      <Select
        value={value}
        name="age"
        displayEmpty
        classes={{
          root: 'select-input',
          select: 'select-unfocused'
        }}
        className={classes.selectEmpty}
        inputProps={{
          name,
          classes: {
            icon: classes.icon
          }
        }}
        MenuProps={{
          MenuListProps: {
            className: 'select-popup'
          }
        }}
        onChange={onChange}
        disabled={disabled}
      >

        <MenuItem value="" disabled >
          {placeholder}
        </MenuItem>
        {values.map(value => (
          <MenuItem value={value.id} key={value.id}>
            {value.name}
          </MenuItem>
        ))}
        {add && <MenuItem value="add">
          <>
            <i className="material-icons">add_circle</i> {addText}
          </>
        </MenuItem>}
      </Select>
    </FormControl>
    <Dialog
      onClose={onCloseDialog}
      aria-labelledby="customized-dialog-title"
      open={dialog}
      className="dialog-bg"
    >
      <DialogContent>
        <div className="dilog-input">
          <AddBankAccount onClose={onCloseDialog} />
        </div>
      </DialogContent>
    </Dialog>

  </>
);

MySelect.defaultProps = {
  values: [],
  onChange: () => { },
  add: false,
  value: "",
  disabled: false,
  dialog: false,
  onCloseDialog: () => { }
}

export default withStyles(styles)(MySelect);
