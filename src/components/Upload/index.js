import React from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';
import { getExtension } from '../../utils/methods';
import pdfImg from '../../img/pdf-placeholder.png';

const Upload = (props) => {
    let image = props.placeholder;
    let thumbnail = pdfImg;

    if (props.file) {
        if (props.file.details) {
            image = `${process.env.REACT_APP_S3_BUCKET_URL}${props.file.details.document_url}`;
            thumbnail = (getExtension(image) === 'pdf') ? `${process.env.REACT_APP_S3_BUCKET_URL}${props.file.details.thumbnail}` : pdfImg
        }

        if (typeof props.file.document_url !== 'undefined') {
            image = `${process.env.REACT_APP_S3_BUCKET_URL}${props.file.document_url}`;
            thumbnail = (getExtension(image) === 'pdf') ? `${process.env.REACT_APP_S3_BUCKET_URL}${props.file.thumbnail}` : pdfImg
        }

        if (typeof props.file.name !== 'undefined') {
            console.log(props.file.name)
            if (getExtension(props.file.name) !== 'pdf') {
                image = URL.createObjectURL(props.file)
            }
        }
    }

    // console.log(props.file, 'file')
    return (
        <>
            {!props.file ? (
                <Dropzone {...props} disabled={props.disabled}>
                    {({ getRootProps, getInputProps, isDragActive }) => {
                        return (
                            <div
                                {...getRootProps()}
                                className={classNames('dropzone', { 'dropzone--isActive': isDragActive })}
                            >
                                <input {...getInputProps()} />
                                <div className="dropzone-center">
                                    {<>{!props.disabled && <i className="material-icons">add_circle</i>}
                                        {props.text && <span className="i-text">{props.text}</span>}
                                    </>}
                                </div>
                                {/* {props.file.loading && <CircularProgress className="loader-1" style={{
                                    color: "#007BC4"
                                }} />} */}
                            </div>
                        )
                    }}
                </Dropzone>
            ) : (
                    <div className={`${props.placeholder_class}`}>
                        <a style={{ height: '100%', width: '100%' }} rel="noopener noreferrer" href={image} target="_blank" download>
                            <img
                                src={getExtension(image) === 'pdf' ? thumbnail : image}
                                alt="pdf_placeholder"
                                style={props.style}
                            />
                        </a>
                        {!props.disabled && <i className="material-icons" onClick={props.onDelete}>delete_outline</i>}
                    </div>
                )}


        </>
    )
}

Upload.defaultProps = {
    onDelete: () => { },
    placeholder: pdfImg,
    text: null,
    file: undefined,
    disabled: false,
    style: {
        height: 200,
        width: 150,
    },
    placeholder_class: "upload-demo"
}

export default Upload;
