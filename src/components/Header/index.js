import React from "react";
import Grid from "@material-ui/core/Grid";
import { withRouter } from 'react-router-dom';
import Avatar from "@material-ui/core/Avatar";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { setInitialAuth } from "../../store/auth/action";
import { ShortLogo } from "../Logo";
import { connect } from "react-redux";
import usrpng from '../../img/user.png';

class Header extends React.Component {

  state = {
    anchorEl: null
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  logout = () => {
    const { clear } = this.props;
    this.handleClose()
    clear()
  }

  editProfile = () => {
    const { history } = this.props;
    this.handleClose();
    history.push('/edit-profile');
  }

  navigateToHome = () => {
    const { user, history } = this.props;
    if (user) {
      switch (user.message.user.role.role_id) {
        case 1:
          history.push("/renter");
          break
        case 2:
          history.push("/create-application");
          break
        case 3:
          history.push("/my-listings");
          break
        default:
          break
      }
    }
  }

  render() {
    const { user } = this.props;
    const { anchorEl } = this.state;
    if (!user) {
      return <></>;
    } else {
      return (
        <div className="top-head">
          <ShortLogo onClick={this.navigateToHome} />
          <div className="profile">
            <Grid container justify="center" alignItems="center">
              <Avatar
                alt="Remy Sharp"
                src={usrpng}
                aria-owns={anchorEl ? "simple-menu" : undefined}
                onClick={this.handleClick}
              />
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.editProfile}>Change Password</MenuItem>
                <MenuItem onClick={this.logout}>Logout</MenuItem>
              </Menu>
            </Grid>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = ({ authStore }) => authStore;
const mapDispatchToProps = {
  clear: () => setInitialAuth()
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
