import React from "react";
import styled from 'styled-components';

const Radio = styled.input`
    border-color: #ffffff !important;
    background: #ffffff !important;

`;

const MyRadio = ({ value, onChange, label, name, content, disabled }) => (
    <label>
        <Radio disabled={disabled} name={name} type="radio" className="filled-in" onClick={onChange} checked={value} />
        <span className="label-text">{label}</span>
        <p className="content-verify">{content}</p>
    </label>
);

MyRadio.defaultProps = {
    onChange: () => { }
}

export default MyRadio;
