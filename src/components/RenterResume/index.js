import React from 'react';
import Grid from "@material-ui/core/Grid";
import { Upload, AddressForm, FinancialForm, IncomeForm } from '../index';
import { questions } from '../../utils/constants';
import { checkAnswer } from '../../utils/methods';

class RenterResume extends React.Component {
    static defaultProps = {
        uploadDoc: () => { },
        deleteUserDoc: () => { },
        changeAddress: () => { },
        selectAddress: () => { },
        changeAccount: () => { },
        uploadBankDoc: () => { },
        deleteBankDoc: () => { },
        changeIncome: () => { },
        setIncomeDocument: () => { },
        deleteIncomeDoc: () => { },
        onChangeAnswer: () => { },
        addresses: [],
        incomes: [],
        accounts: [],
        userDocuments: [],
        background_questions: [],
        disabled: false,
        answers: []
    }

    render() {
        const {
            uploadDoc,
            userDocuments,
            deleteUserDoc,
            addresses,
            changeAddress,
            selectAddress,
            accounts,
            changeAccount,
            uploadBankDoc,
            deleteBankDoc,
            incomes,
            changeIncome,
            setIncomeDocument,
            deleteIncomeDoc,
            disabled,
            background_questions,
            answers,
            onChangeAnswer } = this.props;
        return (
            <div className="renter-dropzone">
                <Grid container spacing={24} className="# ">
                    <Grid item xs={12} sm={12} md={4} className="#">
                        <p className="top-identificatquestionion">Identification</p>
                        <div className="upload-list fquestionull-dropzone">
                            <Upload
                                accept="application/pdf,image/x-png,image/png,image/jpeg"
                                onDrop={(accept) => uploadDoc(accept, 1)}
                                file={userDocuments[1]}
                                text="Upload a Picture of your ID"
                                disabled={disabled}
                                onDelete={() => deleteUserDoc(userDocuments[1].document_id || null)}
                                placeholder_class="full-image"
                                style={{}} />
                        </div>
                        <p className="btm-discription">Please provide a valid Government ID. The picture should clearly include your full name and birthday.</p>
                    </Grid>
                    <Grid item xs={12} sm={7} md={4} className="#">
                        <p className="top-identification">Tax Documents</p>
                        <div className="upload-list identification-list">
                            <Upload
                                accept="application/pdf,image/x-png,image/png,image/jpeg"
                                onDrop={(accept) => uploadDoc(accept, 8)}
                                file={userDocuments[8]}
                                text="This Year"
                                disabled={disabled}
                                onDelete={() => deleteUserDoc(userDocuments[8].document_id || null)} />
                            <Upload
                                accept="application/pdf,image/x-png,image/png,image/jpeg"
                                onDrop={(accept) => uploadDoc(accept, 17)}
                                file={userDocuments[17]}
                                text="Last Year"
                                disabled={disabled}
                                onDelete={() => deleteUserDoc(userDocuments[17].document_id || null)} />
                        </div>
                        <p className="btm-discription">Please provide your two most recent Tax Returns</p>
                    </Grid>
                    <Grid item xs={12} sm={5} md={4} className="#">
                        <p className="top-identification">Concreet Verification</p>
                        <div className="upload-list">
                            <div className="dropzone dropzone-contact">
                                <p className="pdf-contant">
                                    Background Check This report is provided by Concreet Brokerage LLC, a licensed reseller of Accurate Background. If you have purchased and completed Verification and the report still cannot be viewed, Concreet may be waiting on the 3rd party provider to complete their check. If this persists for more than 7 days, please contact us at support@concreet.com
                  </p>
                            </div>
                        </div>
                        <p className="btm-discription">If Concreet Verification is required for this application the Background Check will be included as a part of the application fee. This report will be available once the Application has been completed and submitted.</p>
                    </Grid>
                </Grid>

                <div className="renter-address ">
                    <Grid container spacing={24} className="# ">
                        <Grid item xs={12} sm={12} className="pb-0">
                            <p className="top-identification">Address History<span style={{color: 'red'}}> * </span></p>
                            <p className="btm-discription">Please provide your current address and history of past addresses. We recommend that you include at least 7 years or your past 3 addresses. If your address is current, you need not provide an End Date.</p>
                        </Grid>

                        {addresses.map((address, index) => (
                            <Grid item xs={12} sm={6} md={4} className="pt-0" key={index}>
                                <div className="renter-input-box">
                                    <AddressForm disabled={disabled} address={address} onChange={(e) => changeAddress(e, index)} onSelectAddress={(addres) => selectAddress(addres, index)} />
                                </div>
                            </Grid>
                        ))}

                    </Grid>
                </div>

                <div className="renter-finacial">
                    <Grid container spacing={24} >
                        <Grid item xs={12} sm={12} className="pb-0">
                            <p className="top-identification">Financials</p>
                            <p className="btm-discription">Please provide a snapshot of your current financials. We recommend that you upload the two most recent statements for each account that you add.</p>
                        </Grid>
                        {accounts.map((account, index) => (
                            <Grid item xs={12} sm={6} md={4} className="pt-0" key={index}>
                                <div className="renter-input-box">
                                    <FinancialForm
                                        account={account}
                                        onChange={(e) => changeAccount(e, index)}
                                        onDrop={(accept, name) => uploadBankDoc(accept, index, name)}
                                        onDelete={name => deleteBankDoc(name, index)}
                                        disabled={disabled} />
                                </div>
                            </Grid>
                        ))}
                    </Grid>
                </div>
                <div className="renter-finacial">
                    <Grid container spacing={24} >
                        <Grid item xs={12} sm={12} className="pb-0">
                            <p className="top-identification">Income Source </p>
                            <p className="btm-discription">Please provide your current income sources. We recommend that you include 2 supporting documents per source. Supporting documents include but are not limited to: paystubs, work for hire documents, or dividend statements.</p>
                        </Grid>
                        {incomes.map((income, index) => (
                            <Grid item xs={12} sm={6} md={4} className="pt-0" key={index}>
                                <div className="renter-input-box">
                                    <IncomeForm
                                        income={income}
                                        onChange={(e) => changeIncome(e, index)}
                                        onDrop={accept => setIncomeDocument(accept, index)}
                                        onDelete={document => deleteIncomeDoc(document, index)}
                                        disabled={disabled} />
                                </div>
                            </Grid>
                        ))}
                    </Grid>
                </div>
                <div>
                    <Grid container spacing={24} >
                        <Grid item xs={12} sm={12} className="pb-0">
                            <p className="top-identification">Background Questions </p>
                            {questions.map((question, index) => (
                            <div className="qus-list" key={index}>
                                <button
                                    className={`${checkAnswer(background_questions[index], 'yes', answers[index])} mini-btn`}
                                    onClick={() => onChangeAnswer(true, index)}
                                    disabled={disabled} >
                                    yes
                      </button>
                                <button
                                    disabled={disabled}
                                    onClick={() => onChangeAnswer(false, index)}
                                    className={`${checkAnswer(background_questions[index], 'no', answers[index])} mini-btn`} >
                                    no
                      </button>
                                {question.question}
                            </div>
                        ))}
                        </Grid>
                        
                        
                    </Grid>
                </div>

            </div>
        )
    }
}

export default RenterResume
