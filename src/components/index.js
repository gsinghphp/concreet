import AddressForm from './Forms/AddressForm'
import FinancialForm from './Forms/FinancialForm'
import IncomeForm from './Forms/IncomeForm'
import Upload from './Upload';
import MyButton from './MyButton';
import MyRadio from './MyRadio';
import MyCheckbox from './MyCheckbox'
import MyInput from './MyInput';
import MySelect from './MySelect';
import Title from './Title';
import ConfirmDialog from './Dialog/confirm';

export {
    AddressForm,
    FinancialForm,
    IncomeForm,
    Upload,
    MyButton,
    MyInput,
    MyRadio,
    Title,
    MySelect,
    ConfirmDialog,
    MyCheckbox
}
