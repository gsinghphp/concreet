import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import interceptor from "./utils/interceptor";
import configureStore from "./store/configureStore";
import { ToastContainer } from "react-toastify";
import { PersistGate } from "redux-persist/es/integration/react";
import Routes from "./routes";
import "react-toastify/dist/ReactToastify.css";
import "./styles/main.css";

const { store, persistor } = configureStore();
interceptor.setupInterceptors(store);
const App = () => (
  <Provider store={store}>
    <PersistGate loading={<div />} persistor={persistor}>
      <Router>
        <div className="wrapper">
          <Routes />
        </div>
      </Router>
      <ToastContainer />
    </PersistGate>
  </Provider>
);

export default App;
