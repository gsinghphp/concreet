import React from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import MyButton from "../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { FullLogo } from "../../components/Logo";
import MyInput from "../../components/MyInput";
import { validate, checkValid } from "../../utils/validations";

class Login extends React.Component {
  state = {
    formValues: {
      email: "",
      password: ""
    },
    validations: {
      emailV: null,
      passwordV: null
    },
    loading: false
  };

  handleChange = e => {
    let { formValues, validations } = this.state;
    const { name, value } = e.target;
    formValues[[name]] = value;
    validations = validate(validations, name, value, formValues.password);
    this.setState(() => ({
      formValues,
      validations
    }));
  };

  userLogin = () => {
    const { formValues } = this.state;
    this.setState(() => ({ loading: true }));
    this.props.login(formValues);
  };

  static getDerivedStateFromProps(props, state) {
    let updates = {};
    if (props.user) {
      updates["loading"] = false;

      switch (props.user.message.user.role.role_id) {
        case 1:
          props.history.push("/renter");
          break
        case 2:
          props.history.push("/create-application");
          break
        case 3:
          props.history.push("/my-listings");
          break
        default:
          break
      }
    }
    if (props.phase === 'ERROR') {
      if (props.error === 'User Not Found.') {
        toast.error('Invalid User');
      }
      props.clear();
      updates["loading"] = false;
    }

    return updates;
  }

  render() {
    const {
      formValues: { email, password },
      loading,
      validations,
    } = this.state;
    return (
      <div className="wrapper-login">
        <div className="container center-align">
          <FullLogo />
          <h1>Login</h1>
          <form onSubmit={(e) => {
            e.preventDefault();
          }}>
            <Grid container spacing={16} className="center-align">
              <Grid item xs={12} sm={6} md={4}>
                <MyInput
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  placeholder="Email"
                  validation={validations.emailV}
                />
                <MyInput
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                  placeholder="Password"
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <MyButton
                  className="sg-btn"
                  name="Log In"
                  disabled={checkValid(validations)}
                  onClick={this.userLogin}
                  loading={loading}
                  type="submit"
                />
                <Link to="/forgot-password">Forgot Password?</Link>
                <br />
                <Link to="/register">Agent Registration</Link>
              </Grid>

            </Grid>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
