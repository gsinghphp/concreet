import Login from "./Login";
import { connect } from "react-redux";
import { loginUser, setInitialAuth } from "../../store/auth/action";

const mapStateToProps = ({ authStore }) => authStore;
const mapDispatchToProps = {
  login: user => loginUser(user),
  clear : () => setInitialAuth(),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
