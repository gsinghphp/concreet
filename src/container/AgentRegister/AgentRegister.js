import React from "react";
import MyButton from "../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { toast } from "react-toastify";
import { validate, checkValid } from "../../utils/validations";
import { licenceTypes } from "../../utils/constants";
import { FullLogo } from "../../components/Logo";
import MyInput from "../../components/MyInput";
import MySelect from "../../components/MySelect";
import MyCheckbox from "../../components/MyCheckbox";
import TermsDialog from '../../components/Dialog/terms';

class AgentRegister extends React.Component {
  state = {
    user: {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      verifyPassword: "",
      role_id: 2,
      rebny_no: "",
      company: "",
      office_address: "",
      license: {
        number: "optional",
        type: "nonoptional",
        company: "optional",
        expiry_date: "20/12/2017"
      }
    },
    licence: "",
    validations: {
      emailV: null,
      passwordV: null,
      verifyPasswordV: null,
      firstnameV: null,
      lastnameV: null,
      companyV: null,
      licenceV: null,
    },
    checked: false,
    loading: false,
    terms: false,
  };

  handleChange = e => {
    let { user, validations } = this.state;
    const { name, value } = e.target;

    user[[name]] = value;
    validations = validate(validations, name, value, user.password);

    this.setState(() => ({
      user,
      validations
    }));
  };

  registerAgent = async () => {
    const { user } = this.state;
    this.setState(() => ({ loading: true }));
    this.props.register(user);
  };

  checkValidation = () => {
    const { validations, checked } = this.state;
    const check = checkValid(validations)

    if (check || !checked) {
      return true;
    }
    return false;
  };

  static getDerivedStateFromProps(props, state) {
    let updates = {};
    if (props.registerdata) {
      props.clear();
      updates["loading"] = false;
      toast.success("Please Check your email");
      props.history.push("/login");
    }

    if (props.user) {
      props.history.push("/create-application");
    }

    if (props.error) {
      updates["loading"] = false;
    }
    return updates;
  }

  toggleTerms = () => {
    this.setState((state)=>({
      terms: !state.terms
    }))
  }

  render() {
    const {
      checked,
      loading,
      user: { email, password, verifyPassword, firstname, lastname, company },
      validations: { emailV, passwordV, verifyPasswordV, firstnameV, lastnameV, companyV },
      licence,
      terms,
    } = this.state;

    return (
      <div className="wrapper-login">
        <div className="container center-align">
          <FullLogo />
          <form onSubmit={(e) => {
            e.preventDefault();
          }}>
            <Grid container spacing={16} className="center-align">
              <Grid item xs={12} sm={4}>
                <div className="pass-form">
                  <MyInput
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    placeholder="Email"
                    validation={emailV}
                  />
                  <MyInput
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                    placeholder="Password"
                    validation={passwordV}
                    type="password"
                  />
                  <MyInput
                    name="verifyPassword"
                    value={verifyPassword}
                    onChange={this.handleChange}
                    placeholder="Re-enter Password"
                    validation={verifyPasswordV}
                    type="password"
                  />
                </div>
                <MyInput
                  name="firstname"
                  value={firstname}
                  onChange={this.handleChange}
                  placeholder="First Name"
                  validation={firstnameV}
                />
                <MyInput
                  name="lastname"
                  value={lastname}
                  onChange={this.handleChange}
                  placeholder="Last Name"
                  validation={lastnameV}
                />
                <MyInput
                  name="company"
                  value={company}
                  onChange={this.handleChange}
                  placeholder="Brokerage Name"
                  validation={companyV}
                />
                <div className="select-outer">
                  <MySelect
                    name="licence"
                    placeholder="Licence Type"
                    values={licenceTypes}
                    value={licence}
                    onChange={e => {
                      let { validations } = this.state;
                      const { value } = e.target;
                      validations = validate(validations, "licence", String(value));
                      this.setState(() => ({ licence: value, validations }))
                    }}
                  />
                </div>
              </Grid>
              <Grid item xs={12}>
                <MyCheckbox
                  outsideLabel
                  value={checked}
                  onChange={e =>
                    this.setState(state => ({
                      checked: !state.checked
                    }))
                  }
                  label={<span>I agree to the <span className="text-underline" onClick={this.toggleTerms}>Concreet Terms of Service</span></span>}
                />
              </Grid>
              <Grid item xs={12}>
                <MyButton
                  name="Email Verify Link"
                  onClick={this.registerAgent}
                  disabled={this.checkValidation()}
                  loading={loading}
                  type="submit"
                />
              </Grid>
            </Grid>
          </form>
        </div>
        <TermsDialog open={terms} toggle={this.toggleTerms}/>
      </div>
    );
  }
}

export default AgentRegister;
