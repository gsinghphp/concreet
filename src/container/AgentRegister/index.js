import AgentRegister from "./AgentRegister";
import { connect } from "react-redux";
import { registerAgent, setInitialAuth } from "../../store/register/action";

const mapStateToProps = ({ agentRegisterStore }) => agentRegisterStore;
const mapDispatchToProps = {
  register: user => registerAgent(user),
  clear: () => setInitialAuth(),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AgentRegister);
