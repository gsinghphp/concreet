import React from "react";
import MyButton from "../../components/MyButton";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { FullLogo } from "../../components/Logo";

class Landing extends React.Component {
  componentWillMount() {
    if (!!this.props.user) {
      switch (this.props.user.message.user.role.role_id) {
        case 1:
          window.location.href = "/renter";
          break
        case 2:
          window.location.href = "/create-application";
          break
        case 3:
          window.location.href = "/my-listings";
          break
        default:
          break
      }
    }
  }

  render() {
    const { history } = this.props;
    return (
      <div className="wrapper-login">
        <div className="container center-align">
          <FullLogo />
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <MyButton name="Log In" onClick={() => history.push("/login")} />
            </Grid>
            <Grid item xs={12}>
              <MyButton
                name="Agent Registration"
                onClick={() => history.push("/register")}
              />
            </Grid>
            {/* <Grid item xs={12}>
              <MyButton
                name="Renter Registration"
                onClick={() => history.push("/renter-register")}
              />
            </Grid>
            <Grid item xs={12}>
              <MyButton
                name="Landlord Registration"
                onClick={() => history.push("/landlord-register")}
              />
            </Grid> */}
          </Grid>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { user: state.authStore.user };
};

export default connect(mapStateToProps)(Landing);
