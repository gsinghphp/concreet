import { connect } from 'react-redux';
import EditProfile from './EditProfile';

const mapStateToProps = ({ addressStore, authStore }) => ({
    user: authStore.user ? authStore.user.message.user : authStore.user
});

export default connect(mapStateToProps)(EditProfile);
