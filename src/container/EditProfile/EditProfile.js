import React from 'react';
import Grid from "@material-ui/core/Grid";
import { toast } from "react-toastify";
import { MyInput, MyButton } from '../../components'
import { validate, checkValid } from "../../utils/validations";
import { userRoles } from '../../utils/constants';
import { changePassword } from '../../store/auth/action'
import Avatar from '@material-ui/core/Avatar';
import usrImg from '../../img/user.png';
import TermsDialog from '../../components/Dialog/terms';
import PrivacyDialog from '../../components/Dialog/privacy';

class EditProfile extends React.Component {

    state = this.initialState

    get initialState() {
        return {
            formValues: {
                currentPassword: "",
                password: "",
                verifyPassword: ""
            },
            validations: {
                currentPasswordV: null,
                passwordV: null,
                verifyPasswordV: null
            },
            errors: null,
            loading: false,
            terms: false,
            privacy: false,
        }
    }

    handleChange = e => {
        let { formValues, validations } = this.state;
        const { name, value } = e.target;
        formValues[[name]] = value;
        validations = validate(validations, name, value, formValues.password);
        this.setState(() => ({
            formValues,
            validations
        }));
    };

    changePasswordUser = () => {
        const { formValues: { currentPassword, password } } = this.state;
        this.setState(() => ({ loading: true }));
        changePassword({ currentPassword, newPassword: password }).then(() => {
            toast.success("Password Changed Successfully");
            this.setState(this.initialState);
        }).catch(() => {
            this.setState(() => ({ loading: false }));
        })
    }

    toggleTerms = () => {
        this.setState((state) => ({
            terms: !state.terms
        }))
    }

    togglePrivacy = () => {
        this.setState((state) => ({
            privacy: !state.privacy
        }))
    }

    render() {
        const {
            formValues: { currentPassword, password, verifyPassword },
            validations,
            loading,
            terms,
            privacy
        } = this.state;
        const { user } = this.props;
        return (
            <div className=" wrapper-forget-pswd">
                <div className="container center-align forget-pswd">
                    <h5 className="top-heading">Account Settings</h5>
                    <h5>User Information</h5>
                    {user && (
                        <div className="profile-detail">
                            <div className="profile-box">
                                <div className="profile-avtar">
                                    <Avatar alt="Remy Sharp" src={usrImg} className="bigAvatar" />
                                </div>
                                <div className="profil-info">
                                    <span> {`${user.firstname} ${user.lastname}`}</span>
                                    <br />
                                    <span> {`${userRoles[user.role.role_id]}`}</span>
                                    <br />
                                    <span>   {user.email}</span>
                                </div>
                            </div>
                        </div>
                    )}
                    <h5>Change Password</h5>
                    <Grid container spacing={16} className="center-align">
                        <Grid item xs={12} sm={4}>
                            <MyInput
                                name="currentPassword"
                                onChange={this.handleChange}
                                placeholder="Current Password"
                                type="password"
                                value={currentPassword}
                                validation={validations.currentPasswordV}
                            />
                            <MyInput
                                name="password"
                                onChange={this.handleChange}
                                placeholder="New Password"
                                type="password"
                                value={password}
                                validation={validations.passwordV}
                            />
                            <MyInput
                                name="verifyPassword"
                                value={verifyPassword}
                                onChange={this.handleChange}
                                placeholder="Re-enter New Password"
                                type="password"
                                validation={validations.verifyPasswordV}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <MyButton
                                className="sg-btn"
                                name="Change Password"
                                disabled={checkValid(validations)}
                                loading={loading}
                                onClick={this.changePasswordUser}
                            />
                        </Grid>
                        <Grid item xs={12} className="bottom-link">
                            <h5>Support</h5>
                            <span onClick={this.toggleTerms}>Concreet Terms of Service</span><br />
                            <span onClick={this.togglePrivacy}>Concreet Privacy Policy</span>
                            <p>Contact us at support@concreet.com</p>
                        </Grid>
                    </Grid>
                </div>
                <TermsDialog open={terms} toggle={this.toggleTerms} />
                <PrivacyDialog open={privacy} toggle={this.togglePrivacy} />

            </div>
        )
    }
}

export default EditProfile;
