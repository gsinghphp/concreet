import React from "react";
import MyButton from "../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { toast } from "react-toastify";
import { validate, checkValid } from "../../utils/validations";
import { FullLogo } from "../../components/Logo";
import MyInput from "../../components/MyInput";
import MyCheckbox from "../../components/MyCheckbox";
import TermsDialog from '../../components/Dialog/terms';
import { getEmailFromToken } from '../../store/auth/action';
import { getOS } from '../../utils/methods';

class LandlordRegister extends React.Component {
  state = {
    user: {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      verifyPassword: "",
      role_id: 3,
      id: null
    },
    validations: {
      passwordV: null,
      verifyPasswordV: null,
      firstnameV: null,
      lastnameV: null
    },
    checked: false,
    loading: false,
    terms: false,
  };

  componentDidMount() {
    const { match: { params } } = this.props;
    if (getOS()) {
      window.location.href = `www.concreetapp.com://landlord-register/${JSON.stringify(params)}`
    }
    getEmailFromToken(params).then(res => {
      if (res.message === 'Already registered') {
        this.props.history.push('/login')
      } else {
        const { user } = this.state;
        user.email = res.data.email;
        user.id = res.data.id;
        this.setState({ user })
      }
    })
  }

  handleChange = e => {
    let { user, validations } = this.state;
    const { name, value } = e.target;

    user[[name]] = value;
    validations = validate(validations, name, value, user.password);

    this.setState(() => ({
      user,
      validations
    }));
  };

  registerAgent = async () => {
    const { user } = this.state;
    this.setState(() => ({ loading: true }));
    this.props.register(user);
  };

  checkValidation = () => {
    const { validations, checked } = this.state;
    const check = checkValid(validations)

    if (check || !checked) {
      return true;
    }
    return false;
  };

  static getDerivedStateFromProps(props, state) {
    let updates = {};
    if (props.registerdata) {
      toast.success('Email verified successfully')
      props.clear();
      updates["loading"] = false;
      props.history.push("/login");
    }

    if (props.user) {
      props.history.push("/view-application");
    }

    if (props.error) {
      updates["loading"] = false;
    }
    return updates;
  }

  toggleTerms = () => {
    this.setState((state) => ({
      terms: !state.terms
    }))
  }

  render() {
    const {
      checked,
      loading,
      user: { email, password, verifyPassword, firstname, lastname },
      validations: { emailV, passwordV, verifyPasswordV, firstnameV, lastnameV },
      terms
    } = this.state;
    if (email) {
      return (
        <div className="wrapper-login">
          <div className="container center-align">
            <FullLogo />
            <Grid container spacing={16} className="center-align">
              <Grid item xs={12} sm={4}>
                <div className="pass-form">
                  <MyInput
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    placeholder="Email"
                    validation={emailV}
                    disabled
                  />
                  <MyInput
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                    placeholder="Password"
                    validation={passwordV}
                    type="password"
                  />
                  <MyInput
                    name="verifyPassword"
                    value={verifyPassword}
                    onChange={this.handleChange}
                    placeholder="Re-enter Password"
                    validation={verifyPasswordV}
                    type="password"
                  />
                </div>
                <MyInput
                  name="firstname"
                  value={firstname}
                  onChange={this.handleChange}
                  placeholder="First Name"
                  validation={firstnameV}
                />
                <MyInput
                  name="lastname"
                  value={lastname}
                  onChange={this.handleChange}
                  placeholder="Last Name"
                  validation={lastnameV}
                />
              </Grid>
              <Grid item xs={12}>
                <MyCheckbox
                  outsideLabel
                  value={checked}
                  onChange={e =>
                    this.setState(state => ({
                      checked: !state.checked
                    }))
                  }
                  label={<span>I agree to the <span className="text-underline" onClick={this.toggleTerms}>Concreet Terms of Service</span></span>}
                />
              </Grid>
              <Grid item xs={12}>
                <MyButton
                  name="Register"
                  onClick={this.registerAgent}
                  disabled={this.checkValidation()}
                  loading={loading}
                />
              </Grid>
            </Grid>
          </div>
          <TermsDialog open={terms} toggle={this.toggleTerms} />
        </div>
      );
    }else{
      return(
        <></>
      )
    }

  }
}

export default LandlordRegister;
