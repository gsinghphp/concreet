import React from "react";
import axios from "axios";
import { toast } from "react-toastify";
import { API_URL } from "../../utils/constants";
import { validate, checkValid } from "../../utils/validations";
import MyButton from "../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { FullLogo } from "../../components/Logo";
import MyInput from "../../components/MyInput";

class ChangePassword extends React.Component {
  state = {
    formValues: {
      password: "",
      verifyPassword: ""
    },
    validations: {
      passwordV: null,
      verifyPasswordV: null
    },
    errors: null,
    loading: false
  };

  handleChange = e => {
    let { formValues, validations } = this.state;
    const { name, value } = e.target;
    formValues[[name]] = value;
    validations = validate(validations, name, value, formValues.password);
    this.setState(() => ({
      formValues,
      validations
    }));
  };

  changePassword = () => {
    const {
      match: {
        params: { hash }
      }
    } = this.props;

    const {
      formValues: { password }
    } = this.state;
    this.setState(() => ({ loading: true }));
    axios
      .post(`${API_URL}auth/password/reset`, { password, token: hash })
      .then(res => {
        toast.success("Change Password Successfully");
        this.setState(() => ({ loading: false }));
        this.props.history.push("/login");
      })
      .catch(err => {
        this.setState(() => ({ loading: false }));
      });
  };

  render() {
    const {
      formValues: { password, verifyPassword },
      validations,
      loading
    } = this.state;
    return (
      <div className="wrapper-login">
        <div className="container center-align">
          <FullLogo />
          <h1>Forgot Password?</h1>
          <Grid container spacing={16} className="center-align">
            <Grid item xs={12} sm={4}>
              <MyInput
                name="password"
                value={password}
                onChange={this.handleChange}
                placeholder="Password"
                validation={validations.passwordV}
                type="password"
              />
              <MyInput
                name="verifyPassword"
                value={verifyPassword}
                onChange={this.handleChange}
                placeholder="Re-enter Password"
                validation={validations.verifyPasswordV}
                type="password"
              />
            </Grid>
            <Grid item xs={12}>
              <MyButton
                className="sg-btn"
                name="Reset Password"
                onClick={this.changePassword}
                disabled={checkValid(validations)}
                loading={loading}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default ChangePassword;
