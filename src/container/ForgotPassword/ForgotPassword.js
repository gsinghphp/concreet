import React from "react";
import axios from "axios";
import { toast } from "react-toastify";
import { API_URL } from "../../utils/constants";
import { validate, checkValid } from "../../utils/validations";
import MyButton from "../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { FullLogo } from "../../components/Logo";
import MyInput from "../../components/MyInput";

class ForgotPassword extends React.Component {
  state = {
    formValues: {
      email: ""
    },
    validations: {
      emailV: null
    },
    errors: null,
    loading: false
  };

  handleChange = e => {
    let { formValues, validations } = this.state;
    const { name, value } = e.target;
    formValues[[name]] = value;
    validations = validate(validations, name, value);
    this.setState(() => ({
      formValues,
      validations
    }));
  };

  userLogin = () => {
    const {
      formValues: { email }
    } = this.state;
    this.setState(() => ({ loading: true }));
    axios
      .post(`${API_URL}auth/password/forgot`, { email })
      .then(res => {
        toast.success("You have been emailed a link to reset your password");
        this.setState(() => ({ loading: false }));
        this.props.history.push("/login");
      })
      .catch(err => {
        this.setState(() => ({ loading: false }));
      });
  };

  render() {
    const {
      formValues: { email },
      validations,
      loading
    } = this.state;
    return (
      <div className="wrapper-login">
        <div className="container center-align">
          <FullLogo />
          <h1>Forgot Password?</h1>
          <Grid container spacing={16} className="center-align">
            <Grid item xs={12} sm={6} md={4}>
              <MyInput
                name="email"
                value={email}
                onChange={this.handleChange}
                placeholder="Email"
                validation={validations.emailV}
              />
            </Grid>
            <Grid item xs={12}>
              <MyButton
                className="sg-btn"
                name="Reset Password"
                disabled={checkValid(validations)}
                onClick={this.userLogin}
                loading={loading}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default ForgotPassword;
