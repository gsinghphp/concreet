import React from "react";
import Grid from "@material-ui/core/Grid";
import { Title, MyButton } from '../../../components';
import LeasePackage from '../../../components/LeasePackage';
import { confirmApplication } from '../../../store/application/action';
import { modifyBackAccount, getActiveAccount, checkUserSignature } from '../../../utils/methods';
class Lease extends React.Component {

    state = {
        formValues: {
            payment: null,
        },
        loadedListing: false,
        listing: {},
        applicationValues: {},
        dialog: false,
    }

    componentDidMount() {
        this.props.getRenterApplication();
        this.props.getStripeAccounts();
    }

    static getDerivedStateFromProps(props, state) {
        const updates = {}
        if (props.listing.listing && !state.loadedListing) {
            updates.listing = props.listing.listing
            updates.applicationValues = props.listing
            updates.loadedListing = true
        }

        return updates
    }

    handleChange = e => {
        let { formValues } = this.state;
        let { name, value } = e.target;
        formValues[[name]] = value;
        if (name === 'payment' && value === 'add') {
            this.setState(() => ({ dialog: true }))
        }
        if (name === 'payment' && value !== 'add') {
            this.props.changeStatus(value)
        }
        this.setState(() => ({
            formValues,
        }));
    };

    handleSubmit = () => {
        const { listing } = this.props;
        confirmApplication(listing.id).then(res => {
            console.log(res);
            this.props.history.push('/confirmation');
        }).catch(err => console.log(err))
    }

    setSignature = (docId, value, sigid) => {
        const { listing, deleteDocumentSignatures, addDocumentSignatures } = this.props
        if (value === "true") {
            deleteDocumentSignatures(sigid.id, listing.id)
        } else {
            addDocumentSignatures(listing.id, docId)
        }
        // console.log(docIndex, value, 'change')
        // const { listing } = this.state;
        // //const {value} = event.target
        // // console.log(listing, value)
        // listing.documents[docIndex].signature.push({ id: 0, key: docIndex })

        // this.setState(() => ({
        //     listing
        // }))

    }

    render() {
        const { user, accounts, signatureStore: { signatures } } = this.props;
        const {
            formValues,
            listing,
            applicationValues,
            dialog
        } = this.state;
        
        return (
            <div className="wrapper-lease">
                <div className="lease-top temo">
                    <Grid container spacing={24} justify="center" className="center-align ">
                        <Grid item xs={12} sm={12} className="#">
                            {user && (
                                <Title user={user} />
                            )}
                        </Grid>
                    </Grid>
                    {listing.agent_id &&
                        <LeasePackage
                            handleChange={this.handleChange}
                            paymentValue={getActiveAccount(accounts) || formValues.payment}
                            listing={listing}
                            type='renter'
                            disabled
                            onSelectSignature={this.setSignature}
                            applicationValues={applicationValues}
                            backAccounts={modifyBackAccount(accounts)}
                            dialog={dialog}
                            signatures={signatures}
                            activeRenterSignature
                            onCloseDialog={() => this.setState(() => ({ dialog: false }))}
                        />}
                    <MyButton
                        name="Submit Application"
                        onClick={this.handleSubmit}
                        disabled={!checkUserSignature(signatures) || !accounts.length}
                    />
                    <Grid container spacing={24} justify="center" className="center-align ">
                        <Grid item xs={12} sm={12} className="#">
                            <button className="btn btn-small btn-transparent-sm" onClick={() => this.props.history.goBack()}>back </button>
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }
}

export default Lease;
