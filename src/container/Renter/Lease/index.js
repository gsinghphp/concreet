import { connect } from 'react-redux';
import { getRenterApplication, addSignature } from '../../../store/application/action';
import { getStripeAccounts, changeStatus } from '../../../store/stripeaccounts/action';
import { addDocumentSignatures, deleteDocumentSignatures } from '../../../store/signature/action';

import Lease from './Lease'

const mapStateToProps = ({ authStore, applicationStore, stripeAccountStore, signatureStore}) => ({
    user: authStore.user ? authStore.user.message.user : authStore.user,
    listing: applicationStore.renterApplication.length ? applicationStore.renterApplication[0] : {},
    accounts: stripeAccountStore.accounts,
    signatureStore
});

const mapDispatchToProps = {
    getRenterApplication: () => getRenterApplication(),
    addSignature: data => addSignature(data),
    getStripeAccounts: () => getStripeAccounts(),
    changeStatus: id => changeStatus(id),
    addDocumentSignatures: (applicationid, docId) => addDocumentSignatures({applicationid, docId}),
    deleteDocumentSignatures: (id, appId) => deleteDocumentSignatures({id, appId})
}

export default connect(mapStateToProps, mapDispatchToProps)(Lease);
