import React from "react";
import Grid from "@material-ui/core/Grid";
import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { MyButton, Title, ConfirmDialog } from '../../../components';
import { addressSplit, incomeFormat, ifAddressFilled } from '../../../utils/methods';
import { uploadDocument } from '../../../store/application/action';
import RenterResume from '../../../components/RenterResume';

class RenterMain extends React.Component {
  dialog = React.createRef();
  state = {
    addresses: [],
    accounts: [],
    incomes: [],
    loading: false,
    documents: null,
    loadedAddress: false,
    loadedBanks: false,
    loadedIncomes: false,
    confimPopup: false,
    answers: []
  }
  componentDidMount() {
    const { getAddress, getAccounts, getUserDocs, getIncome, getRenterApplication, getQuestions } = this.props;
    getAddress();
    getAccounts();
    getUserDocs();
    getIncome();
    getRenterApplication();
    getQuestions();
  }

  static getDerivedStateFromProps(props, state) {
    const { addressStore: { addresses, phase }, documents, bankAccountStore, incomeStore, applicationStore } = props;
    const { incomes } = incomeStore;
    const { accounts } = bankAccountStore;
    const { length } = addresses;
    const updatedStates = {};
    const addAddressesCount = 3 - length;
    const bankAccountsCount = 3 - accounts.length;
    const incomeCount = 3 - incomes.length

    if (applicationStore.renterApplication.length) {
      if (applicationStore.renterApplication[0].status === 6)
        props.history.push('/confirmation')
    }
    if (phase === 'SUCCESS' && !state.loadedAddress) {
      for (let i = 0; i < addAddressesCount; i++) {
        addresses.push({
          address: "",
          address_street_name: "",
          address_state: "",
          city: "",
          address_country: "",
          address_street_number: "",
          address_apartment_number: "",
          address_valid_from: "",
          address_valid_to: "",
          change: false
        })
      }
      updatedStates['addresses'] = addresses;
      updatedStates['loadedAddress'] = true
    }

    if (bankAccountStore.phase === 'SUCCESS' && !state.loadedBanks) {
      for (let i = 0; i < bankAccountsCount; i++) {
        accounts.push({
          bank_account_name: "",
          bank_account_type: 1,
          bank_account_last_month_starting_balance: "",
          bank_account_current_month_starting_balance: "",
          bank_account_statement_1: null,
          bank_account_statement_2: null,
          change: false
        })
      }
      updatedStates['accounts'] = accounts;
      updatedStates['loadedBanks'] = true
    }

    if (incomeStore.phase === 'SUCCESS' && !state.loadedIncomes) {
      for (let i = 0; i < incomeCount; i++) {
        incomes.push({
          income_title: "",
          income_type_id: 1,
          income_annual: "",
          income_monthly: "",
          income_start_date: "",
          income_end_date: "",
          income_current: true,
          change: false,
          documents: []
        })
      }
      updatedStates['incomes'] = incomes;
      updatedStates['loadedIncomes'] = true
    }


    if (documents.phase === 'LOADING') {
      updatedStates['loading'] = true
    } else {
      updatedStates['loading'] = false
    }

    return updatedStates;
  }

  changeAddress = (event, index) => {
    const { addresses } = this.state;
    console.log(event)
    let { value, name } = event.target;
    if (name === 'address_apartment_number') {
      value = value.replace(/[^0-9]/g, "")
    }
    addresses[index]['change'] = true;
    addresses[index][[name]] = value;
    this.setState(() => ({
      addresses
    }))
  }

  changeAccount = (event, index) => {
    const { accounts } = this.state;
    let { value, name } = event.target;
    if (name === 'bank_account_current_month_starting_balance' || name === 'bank_account_last_month_starting_balance') {
      value = incomeFormat(value)
    }
    accounts[index]['change'] = true;
    accounts[index][[name]] = value || "";
    this.setState(() => ({
      accounts
    }))
  }

  changeIncome = (event, index) => {
    const { incomes } = this.state;
    let { value, name } = event.target;
    if (name === 'income_monthly') {
      value = incomeFormat(value)
    }
    incomes[index]['change'] = true;
    incomes[index][[name]] = value || "";
    this.setState(() => ({
      incomes
    }))
  }

  changeAnswer = (answer, index) => {
    const { questionStore, updateQuestion } = this.props;
    if (questionStore.questions.length) {
      if (questionStore.questions[index].answer !== answer) {
        questionStore.questions[index].answer = answer;
        updateQuestion(questionStore.questions[index])
      }
    } else {
      const { answers } = this.state;
      answers[index] = answer
      this.setState(() => ({
        answers
      }))
    }

  }

  setIncomeDocument = (accept, index) => {
    if (accept.length) {
      if (accept[0].size < 1024 * 1024 * 5) {
        const { incomes } = this.state;
        incomes[index]['change'] = true;
        incomes[index].documents.push(accept[0]);
        this.setState(() => ({
          incomes
        }))
      } else {
        toast.error('File size should be up to 5 mb')
      }
    } else {
      toast.error('Only PDF, JPEG, or PNG file are accepted')
    }

  }

  selectAddress = (address, index) => {
    const { addresses } = this.state;
    if (typeof address !== 'string') {
      const { address_components } = address;
      const addressData = addressSplit(address_components)
      if (addressData.administrative_area_level_1) { //Country
        addresses[index].city = addressData.administrative_area_level_1
      }

      if (addressData.country) { //Country
        addresses[index].address_country = addressData.country
      }
      if (addressData.street_number) { //Street Number
        addresses[index].address_street_number = addressData.street_number
      }

      if (addressData.locality) { //State
        addresses[index].address_state = addressData.locality
      }

      if (addressData.route) { //Street Number
        addresses[index].address_street_name = addressData.route
        addresses[index].address = addressData.route
      }

      if (addressData.postal_code) { //Street Number
        addresses[index].address_zipcode = addressData.postal_code
      }
    } else {
      addresses[index].address_street_name = address;
      addresses[index].city = ""
      addresses[index].address_country = ""
      addresses[index].address_street_number = ""
      addresses[index].address_state = ""
      addresses[index].address = ""
      addresses[index].address_zipcode = ""
    }
    addresses[index]['change'] = true;
    this.setState(() => ({
      addresses
    }))

  }

  saveRenterForm = async () => {
    const { addresses, accounts, incomes, answers } = this.state;
    const { saveAddress, saveAccount, saveIncome, history, questionStore, insertQuestions } = this.props;
    for (let i = 0; i < addresses.length; i++) {
      if (addresses[i]['change']) {
        await saveAddress({
          id: typeof addresses[i].id === 'undefined' ? null : addresses[i].id,
          address: addresses[i].address,
          street_name: addresses[i].address_street_name,
          street_number: addresses[i].address_street_number,
          apartment_number: addresses[i].address_apartment_number,
          city: addresses[i].city,
          state: addresses[i].address_state,
          state_abbr: addresses[i].address_state,
          zipcode: addresses[i].address_zipcode,
          country: addresses[i].address_country,
          country_abbr: addresses[i].address_country,
          valid_from: addresses[i].address_valid_from,
          valid_to: addresses[i].address_valid_to,
        })
      }
    }

    for (let i = 0; i < accounts.length; i++) {
      if (accounts[i]['change']) {
        await saveAccount({
          id: typeof accounts[i].id === 'undefined' ? null : accounts[i].id,
          name: accounts[i].bank_account_name,
          type: accounts[i].bank_account_type,
          last_month_starting_balance: accounts[i].bank_account_last_month_starting_balance,
          current_month_starting_balance: accounts[i].bank_account_current_month_starting_balance,
          statement_1: typeof accounts[i].bank_account_statement_1 === 'object' && accounts[i].bank_account_statement_1 ? accounts[i].bank_account_statement_1.id : accounts[i].bank_account_statement_1,
          statement_2: typeof accounts[i].bank_account_statement_2 === 'object' && accounts[i].bank_account_statement_2 ? accounts[i].bank_account_statement_2.id : accounts[i].bank_account_statement_2
        })
      }
    }

    for (let i = 0; i < incomes.length; i++) {
      if (incomes[i]['change']) {
        const documents = incomes[i].documents.filter(document => typeof document.name !== 'undefined')
        await saveIncome({
          id: typeof incomes[i].id === 'undefined' ? null : incomes[i].id,
          title: incomes[i].income_title,
          type_id: incomes[i].income_type_id,
          monthly: incomes[i].income_monthly,
          start_date: incomes[i].income_start_date,
        }, documents)
      }
    }

    if (!questionStore.questions.length) {
      // console.log(answers)
      insertQuestions(answers)
    }
    history.push('/lease');
  }

  uploadDoc = (accept, type) => {
    if (accept.length) {
      if (accept[0].size < 1024 * 1024 * 5) {
        return this.props.uploadDoc({ doc: accept[0], type })
      } else {
        return toast.error('File size should be up to 5 mb')
      }
    } else {
      toast.error('Only PDF, JPEG, or PNG file are accepted')
    }
  }

  deleteUserDoc = (documentId) => {
    if (documentId) {
      this.refs.dialog.open().then(() => {
        this.props.deleteUserDocument(documentId)
      }).catch(err => { })
    }
  }

  deleteBankDoc = async (name, index) => {
    this.refs.dialog.open().then(() => {
      const { accounts } = this.state;
      accounts[index]['change'] = true;
      accounts[index][[name]] = null;
      this.setState(() => ({
        accounts
      }))
    }).catch(err => { })

  }

  deleteIncomeDoc = (document, index) => {
    this.refs.dialog.open().then(() => {
      const { incomes } = this.state;
      const { deleteIncomeDoc } = this.props;
      if (typeof document.name === 'undefined') {  //Document from api
        deleteIncomeDoc(incomes[index].id, document.id);
      }
      const documentIndex = incomes[index].documents.findIndex(doc => doc.id === document.id);
      if (documentIndex > -1) {
        incomes[index].documents.splice(documentIndex, 1)
        this.setState(() => ({
          incomes
        }))
      }
    }).catch(err => { })
  }

  uploadBankDoc = (accept, index, name) => {
    if (accept.length) {
      if (accept[0].size < 1024 * 1024 * 5) {
        const { accounts } = this.state;
        return uploadDocument(accept[0], 5).then(response => {
          accounts[index]['change'] = true;
          accounts[index][[name]] = response.data;
          this.setState(() => ({
            accounts
          }))
        })
      } else {
        return toast.error('File size should be up to 5 mb')
      }
    } else {
      toast.error('Only PDF, JPEG, or PNG file are accepted')
    }

  }

  render() {
    const { addresses, loading, accounts, incomes, answers } = this.state;
    const { applicationStore, userDocuments, questionStore: { questions } } = this.props;

    if (applicationStore.renterApplication.length) {
      return (
        <div className="wrapper-renter">
          {loading && <div className="loading"><CircularProgress /></div>}
          <div className="renter-top">
            <Grid container spacing={24} justify="center" className="center-align ">
              <Grid item xs={12} md={6} sm={8} className="#">
                <Title />
                {/* <div className="renter-top-detail">
                <p>{user.email}</p>
                <p>Applied at 12:34 AM opaymentsShown mm/dd/yyyy</p>
              </div> */}
              </Grid>
            </Grid>
          </div>
          <RenterResume
            uploadDoc={this.uploadDoc}
            userDocuments={userDocuments}
            deleteUserDoc={this.deleteUserDoc}
            addresses={addresses}
            changeAddress={this.changeAddress}
            selectAddress={this.selectAddress}
            accounts={accounts}
            changeAccount={this.changeAccount}
            uploadBankDoc={this.uploadBankDoc}
            deleteBankDoc={this.deleteBankDoc}
            incomes={incomes}
            changeIncome={this.changeIncome}
            setIncomeDocument={this.setIncomeDocument}
            deleteIncomeDoc={this.deleteIncomeDoc}
            answers={answers}
            background_questions={questions}
            onChangeAnswer={(answer, index) => this.changeAnswer(answer, index)}
          />
          <Grid item xs={12}>
            <MyButton
              name="Save and Continue"
              onClick={this.saveRenterForm}
              disabled={typeof userDocuments[1] === 'undefined' || ifAddressFilled(addresses) || (answers.length < 5 && questions.length < 5)}
            />
          </Grid>
          <ConfirmDialog ref="dialog" />
        </div>
      );
    } else {
      return (
        <div className="wrapper-renter">
          No Application Found
        </div>
      )
    }

  }

  componentWillUnmount() {
    const { initIncome, setInitAccount, initAddress } = this.props;
    initIncome();
    setInitAccount();
    initAddress();
  }
}

export default RenterMain;
