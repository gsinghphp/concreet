import { connect } from 'react-redux';
import { uploadUserDoc, getUserDocs, deleteUserDocument, addUserDoc } from '../../../store/documents/action';
import { getAddresses, saveAddress, initAddress } from '../../../store/address/action';
import { getBankAccounts, saveAccount, setInitAccount } from '../../../store/bankaccount/action';
import { getIncomes, saveIncome, deleteIncomeDoc, initIncome } from '../../../store/income/action';
import { getRenterApplication } from '../../../store/application/action';
import { getQuestions, insertQuestions, updateQuestion } from '../../../store/question/action';
import RenterMain from './Main'

const mapStateToProps =
    ({ applicationStore, addressStore, authStore, documentsStore, bankAccountStore, listingDocumentsStore, incomeStore, questionStore }) => ({
        addressStore,
        user: authStore.user ? authStore.user.message.user : authStore.user,
        documents: documentsStore,
        bankAccountStore,
        userDocuments: listingDocumentsStore.userDocuments,
        incomeStore,
        questionStore,
        applicationStore
    });

const mapDispatchToProps = {
    uploadDoc: data => uploadUserDoc(data),
    getAddress: () => getAddresses(),
    saveAddress: address => saveAddress(address),
    getAccounts: () => getBankAccounts(),
    saveAccount: data => saveAccount(data),
    getUserDocs: () => getUserDocs(),
    deleteUserDocument: document_id => deleteUserDocument(document_id),
    addUserDoc: payload => addUserDoc(payload),
    getIncome: () => getIncomes(),
    saveIncome: (data, documents) => saveIncome(data, documents),
    deleteIncomeDoc: (incomeID, docId) => deleteIncomeDoc(incomeID, docId),
    initIncome: () => initIncome(),
    setInitAccount: () => setInitAccount(),
    initAddress: () => initAddress(),
    getRenterApplication: () => getRenterApplication(),
    getQuestions: () => getQuestions(),
    insertQuestions: answers => insertQuestions(answers),
    updateQuestion: question => updateQuestion(question)
}
export default connect(mapStateToProps, mapDispatchToProps)(RenterMain);
