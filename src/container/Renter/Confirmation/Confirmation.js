import React from "react";
import Grid from "@material-ui/core/Grid";
import moment from 'moment';
import { Title } from '../../../components';
import { MyButton } from '../../../components';
import RenterResume from '../../../components/RenterResume';
import LeasePackage from '../../../components/LeasePackage';
import { modifyBackAccount, getActiveAccount } from '../../../utils/methods';
class Confirmation extends React.Component {
    state = {
        resumeShow: true,
        loadedListing: false,
        listing: {},
        applicationValues: {},
    }

    componentDidMount() {
        this.props.getRenterResume();
        this.props.getStripeAccounts();
    }

    static getDerivedStateFromProps(props, state) {
        const updates = {}
        if (props.listing.listing && !state.loadedListing) {
            updates.listing = props.listing.listing
            updates.applicationValues = props.listing
            updates.loadedListing = true
        }

        return updates
    }


    changeView = view => {
        this.setState(() => ({
            resumeShow: view
        }))
    }
    render() {
        
        const { user, resume, accounts, signatureStore: { signatures } } = this.props;
        const {
            listing,
            applicationValues,
        } = this.state;
        const { resumeShow } = this.state;
        const { address, income, bank_accounts, documents, background_questions } = resume;
        let userDocuments = []
        if (documents) {
            userDocuments = documents.reduce((acc, doc) => {
                acc[doc.document_type_id] = doc;
                return acc
            }, {})
        }
        
        return (
            <div className={resumeShow ? "wrapper-renter" : "wrapper-lease"}>
                <div className="lease-top">
                    <Grid container spacing={24} justify="center" className="center-align ">
                        <Grid item xs={12} sm={12} className="#">
                            {user && (
                                <>
                                    <Title type="Application" />
                                    <div className="renter-top-detail">
                                        <p>Applied at {moment(listing.updated_at).format('HH:mm a')} on {moment(listing.updated_at).format('MM/DD/YYYY')}</p>
                                    </div>
                                </>
                            )}
                        </Grid>

                    </Grid>

                </div>
                <Grid container spacing={24} justify="center" className="center-align ">
                    <Grid item xs={12} sm={12} className="lease-tab-btn">
                        <MyButton
                            name="Renter's Resume"
                            onClick={() => this.changeView(true)}
                            className={`btn-transparent ${resumeShow && 'active'}`}
                        />
                        <MyButton
                            name="Lease Package"
                            onClick={() => this.changeView(false)}
                            className={`btn-transparent ${!resumeShow && 'active'}`}
                        />
                    </Grid>

                </Grid>

                {resumeShow ? (
                    <RenterResume
                        addresses={address}
                        incomes={income}
                        accounts={bank_accounts}
                        userDocuments={userDocuments}
                        background_questions={background_questions}
                        disabled />
                ) : (
                        listing && (
                            <LeasePackage
                                disabled
                                listing={listing}
                                applicationValues={applicationValues}
                                paymentValue={0 || getActiveAccount(accounts)}
                                backAccounts={modifyBackAccount(accounts)}
                                signatures={signatures}
                            />
                        )
                    )}

            </div>
        );
    }
}

export default Confirmation;
