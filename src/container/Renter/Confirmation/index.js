import { connect } from 'react-redux';
import { getRenterResume } from '../../../store/application/action';
import { getStripeAccounts } from '../../../store/stripeaccounts/action';
import Confirmation from './Confirmation'

const mapStateToProps = ({ authStore, applicationStore, stripeAccountStore, signatureStore }) => ({
    user: authStore.user ? authStore.user.message.user : authStore.user,
    resume: applicationStore.resume,
    accounts: stripeAccountStore.accounts,
    listing: applicationStore.renterApplication.length ? applicationStore.renterApplication[0] : {},
    signatureStore
});

const mapDispatchToProps = {
    getRenterResume: () => getRenterResume(),
    getStripeAccounts: () => getStripeAccounts(),
};

export default connect(mapStateToProps, mapDispatchToProps)(Confirmation);
