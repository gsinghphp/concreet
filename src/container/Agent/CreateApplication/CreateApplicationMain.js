import React from "react";
import MyButton from "../../../components/MyButton";
import Grid from "@material-ui/core/Grid";
import { withRouter } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { debounce } from 'throttle-debounce';
import FormControl from '@material-ui/core/FormControl';
import MyInput, { CurrencyInput } from "../../../components/MyInput";
import { MyCheckbox, MyRadio, Upload, MySelect, ConfirmDialog } from "../../../components";
import { validate, checkValid, checkTrue } from "../../../utils/validations";
import { searchString, fullListingName, formatAmount, modifyBackAccount, getActiveAccount } from '../../../utils/methods';
import { searchByEmail, autocompleteEmail } from "../../../store/application/action";

import pddplaceholderImg from '../../../img/pdf-placeholder.png';

class CreateApplicationMain extends React.Component {
    dialog = React.createRef();
    constructor(props) {
        super(props);

        const { listingAdded, formValues: { listingAddress }, applicationStore: { values } } = props;
        const { agent_id } = listingAddress;
        let listingId = agent_id;
        if (values) {
            listingId = values.agent_listing_id;
        } else if (typeof listingId === 'undefined') {
            listingId = listingAdded.id;
        }
        console.log(values)
        this.state = {
            formValues: {
                ...props.formValues,
                broker_fee: 0,
                security_deposit: 0,
                first_months_rent: 0,
                last_months_rent: 0,
                verification: values ? values.verification : 1,
                payment: "",
                landlord_email: values ? values.listing.landlord_email : "",
                landlord: {},
                listingId,
                ...values
            },
            validations: {
                landlord_emailV: values ? true : null
            },
            errors: null,
            loading: false,
            paymentsShow: {
                broker_fee: values ? Boolean(values.broker_fee) : false,
                security_deposit: values ? Boolean(values.security_deposit) : false,
                first_months_rent: values ? Boolean(values.first_months_rent) : false,
                last_months_rent: values ? Boolean(values.last_months_rent) : false,
            },
            added: false,
            dialog: false,
            renteremails: [],
            searchListings: [],
        };
    }

    static getDerivedStateFromProps(props, state) {
        const { createApplicationStore, applicationStore: {values}} = props;
        const updatedStates = {};

        if (createApplicationStore.phase === 'SUCCESS') {

            toast.success(`Application ${values ? "updated" : "created"} successfully`)
            updatedStates['added'] = true;
            props.onSuccess();
            props.clearCreateApplication();
        }

        return updatedStates;
    }

    getRenters = debounce(500, (value) => {
        autocompleteEmail(value).then(res => {
          this.setState(() => ({
            renteremails: res.message
          }))
        });
      });

      checkIfEmailExists () {
        if (this.state.renteremails.length && this.state.formValues.email) {
          for (let i = 0; i < this.state.renteremails.length; i++) {
            if (this.state.renteremails[i]['email'] === this.state.formValues.email) {
              return this.state.renteremails[i]
            }
          }
          return false
        }
      }

      findAlreadyListing = email => {
        const { putApiValues } = this.props;
        const { validations, formValues } = this.state;
        searchByEmail(email)
          .then(res => {
            formValues["listingAddress"] = res.message[0].listing
            putApiValues(res.message[0])
            validations["listingAddressV"] = true;
            formValues["listingAddressInput"] = fullListingName(res.message[0].listing.details)
            this.setState(() => ({ validations, showListingField: false, formValues }))
          })
          .catch(err => {
            // if (err.message === "No Applications Found") {
            validations["listingAddressV"] = null;
            this.setState(() => ({ validations, showListingField: true }))
            this.props.getAgentListing();
            // }
          });
      }

      

      selectRenter = renter => {
        const {
          formValues,
          validations
        } = this.state;
        const { addValues } = this.props;
        formValues['renter'] = renter;
        formValues['email'] = renter.email;
        validations["emailV"] = true;
        this.setState(() => ({ formValues, renteremails: [], validations }))
        addValues(formValues);
        this.findAlreadyListing(renter.email);
      }

      
    
    

    componentDidMount() {
        const { getListingDoc, getStripeAccounts } = this.props;
        const { formValues: { listingId } } = this.state;
        getListingDoc(listingId)
        getStripeAccounts()
    }

     handleChange = e => {
        let { formValues, validations } = this.state;
        const { addValues, changeStatus } = this.props;
        let { name, value } = e.target;
        if (name === 'broker_fee' || name === 'security_deposit' || name === 'first_months_rent' || name === 'last_months_rent') {
            value = String(value).replace(/,/g, '')
            if (!isNaN(value)) {
                formValues[[name]] = value;
            }
        } else {
            formValues[[name]] = value;
        }


        validations = validate(validations, name, value);
        // if (validations.landlord_emailV === true && name === 'landlord_email') {
        //     this.getLandlord(formValues, validations, value);
        // }

        if (name === 'payment' && value === 'add') {
            this.setState(() => ({ dialog: true }))
        }
        if (name === 'payment' && value !== 'add') {
            changeStatus(value)
        }
        if (name === 'listingAddressInput') {
            const searchListings = searchString(value, this.props.listings)
            this.setState(() => ({ searchListings }));
            if (value === "") {
              validations["listingAddressV"] = null;
              this.setState(() => ({ searchListings }));
            }
          }

        if (name === 'email') {
            this.getRenters(value.toLowerCase())
          }

        this.setState(() => ({
            formValues,
            validations
        }));
        addValues(formValues);
    };

    // getLandlord = debounce(500, (formValues, validations, value) => {
    //     searchLandlordByEmail(value).then(res => {
    //         formValues['landlord'] = res.message;
    //         this.setState(() => ({
    //             formValues
    //         }));
    //     }).catch(err => {
    //         validations['landlord_emailV'] = 'Landlord Not Found';
    //         this.setState(() => ({
    //             validations
    //         }));
    //     })
    // })


    checkValidations = () => {
        const { validations } = this.state;
        return checkValid(validations);
    };

    createApplication = () => {
        // const { formValues: { renter: { renters_id } }, formValues } = this.state;
        const { formValues } = this.state;
        const { applicationStore: { values }, inviteLandlord, createApplication } = this.props;
        const application = {
            id: values ? values.id : null,
            agent_listing_id: formValues.listingId,
            broker_fee: Number(formValues.broker_fee),
            security_deposit: Number(formValues.security_deposit),
            first_months_rent: Number(formValues.first_months_rent),
            last_months_rent: Number(formValues.last_months_rent),
            verification: formValues.verification
        }
        if (values) {
            createApplication(values.renter_id, formValues.landlord_email, application)
        } else {
            inviteLandlord(formValues.landlord_email, application)
        }

    };

    enablePayment = (type, value) => {
        const { paymentsShow, formValues } = this.state;
        var isTrueSet = (value === 'true');
        paymentsShow[[type]] = !isTrueSet;
        if (isTrueSet) {
            formValues[[type]] = 0;
        }
        this.setState(() => ({
            paymentsShow,
            formValues
        }))
    }

    uploadFile = (accepted) => {
        const { uploadDoc } = this.props;
        const { formValues: { listingId } } = this.state;
        if (accepted.length) {
            if (accepted[0].size < 1024 * 1024 * 5) {
                uploadDoc({ doc: accepted[0], type: 16 }, listingId)
            } else {
                toast.error('File size should be up to 5 mb')
            }
        } else {
            toast.error('Please Select PDF Document')
        }
    }

    deleteFile = id => {
        const { deleteDocument } = this.props;
        const { formValues: { listingId } } = this.state;
        this.refs.dialog.open().then(() => {
            deleteDocument(id, listingId)
        }).catch(err => { })

    }

    setVerification = status => {
        const { formValues } = this.state;
        formValues.verification = status
        this.setState(() => ({
            formValues
        }))
    }

    // emailUpdate = (type) => {
    //     const {emailDisabled, addressDisabled} = this.state;
    //     // const email = (type === 'email') ? !emailDisabled : emailDisabled;
    //     // const add = (type === 'address') ? !addressDisabled : addressDisabled
    //     this.setState({
    //         emailDisabled: !emailDisabled,
    //         // addressDisabled: add,
    //     })
    // }

    selectListing = (listing) => {
        const { validations, formValues } = this.state;
        validations["listingAddressV"] = true;
        formValues["listingAddressInput"] = fullListingName(listing)
        formValues["listingAddress"] = listing
        this.setState(() => ({
          searchListings: [],
          validations,
          formValues
        }))
        this.props.addValues(formValues)
      }
    

    render() {
        const {
            formValues,
            paymentsShow: { broker_fee, security_deposit, first_months_rent, last_months_rent },
            paymentsShow,
            validations,
            dialog,
            renteremails,
            searchListings,
        } = this.state;
        const { accounts, applicationStore: { values }, documents, createApplicationStore, uploadDocuments: { phase } } = this.props;

        const documentsLength = documents.length;
        const emptyDocuments = 5 - documentsLength;
        for (let i = 0; i < emptyDocuments; i++) {
            documents.push({
                document_id: 0,
            })
        }

        let listingDisabled = false;
        if (values) {
            if (values.listing.landlord_email) {
                listingDisabled = true
            }
        }

        return (
            <div className="wrapper-freelink">
                {(phase === 'LOADING' || createApplicationStore.phase === 'LOADING') && <div className="loading"> <CircularProgress /></div>}
                <h1> {values ? "Update an Application" : "Create an Application"} </h1>
                <Grid container spacing={16} justify="center" className="center-align r-email">
                    <Grid item xs={12} sm={12} className=" r-email adress-email">
                        <MyInput
                            name="email"
                            value={formValues.email}
                            placeholder="Renter's Email"
                            onChange={this.handleChange}
                            disabled ={ values ? true : false}
                            suggestions={
                                renteremails.length ? (
                                  <div className="option-list">
                                    {
                                      renteremails.map(renter => (
                                        <div style={{ textAlign: 'center' }} className="option-item" onClick={() => this.selectRenter(renter)} key={renter.id}>
                                          {renter.email}
                                        </div>
                                      ))
                                    }
                                  </div>
                                ) : <></>
                              }
                        />
                        <MyInput
                            name="listingAddressInput"
                            value={formValues.listingAddressInput}
                            onChange={this.handleChange}
                            placeholder="123 Sesame Street, Apt 1A, New York, NY 12345"
                            disabled ={ values ? true : false}
                            suggestions={
                                (formValues.listingAddressInput !== "" && searchListings.length) ?
                                  <div className="option-list">
                                    {
                                      searchListings.map(listing => (
                                        <div style={{ textAlign: 'center' }} className="option-item" onClick={() => this.selectListing(listing)} key={listing.id}>
                                          {fullListingName(listing)}
                                        </div>
                                      ))
                                    }
                                  </div> : <></>
                              }
                        />

                    </Grid>
                    <Grid item xs={12} sm={12} md={4} className="#">
                    </Grid>
                    <Grid item xs={12} sm={12} md={5} className="payment-box">
                        <h5>Payments</h5>
                        <ul className="payment-item">
                            <li className="payment-list">
                                <MyCheckbox
                                    label="Broker Fee"
                                    onChange={value => this.enablePayment('broker_fee', value)}
                                    value={broker_fee}
                                />
                                <div className="payment-listLeft">
                                    {broker_fee && (
                                        <CurrencyInput
                                            name="broker_fee"
                                            value={formatAmount(formValues.broker_fee)}
                                            placeholder="$ 1,000"
                                            onChange={this.handleChange}
                                        />
                                    )}



                                </div>

                            </li>
                            <li className="payment-list">
                                <MyCheckbox
                                    label="Security Deposit"
                                    onChange={value => this.enablePayment('security_deposit', value)}
                                    value={security_deposit}
                                />
                                {security_deposit && (
                                    <CurrencyInput
                                        name="security_deposit"
                                        value={formatAmount(formValues.security_deposit)}
                                        placeholder="$ 1,000"
                                        onChange={this.handleChange}
                                    />
                                )}
                            </li>
                            <li className="payment-list">

                                <MyCheckbox
                                    label="First Month's Rent"
                                    onChange={value => this.enablePayment('first_months_rent', value)}
                                    value={first_months_rent}
                                />
                                {first_months_rent && (
                                    <CurrencyInput
                                        name="first_months_rent"
                                        value={formatAmount(formValues.first_months_rent)}
                                        placeholder="$ 1,000"
                                        onChange={this.handleChange}
                                    />
                                )}
                            </li>
                            <li className="payment-list">
                                <MyCheckbox
                                    label="Last Month's Rent"
                                    onChange={value => this.enablePayment('last_months_rent', value)}
                                    value={last_months_rent}
                                />
                                {last_months_rent && (
                                    <CurrencyInput
                                        name="last_months_rent"
                                        value={formatAmount(formValues.last_months_rent)}
                                        placeholder="$ 1,000"
                                        onChange={this.handleChange}
                                    />
                                )}
                            </li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3} className="#">
                        <div className="payment-slect-input">
                            {checkTrue(paymentsShow) && <FormControl className="input-formControl">
                                <MySelect
                                    className="demo"
                                    placeholder="Link an account to receive fee"
                                    values={modifyBackAccount(accounts)}
                                    value={getActiveAccount(accounts) || formValues.payment}
                                    add
                                    addText="Add a New Payment Method"
                                    onChange={this.handleChange}
                                    name="payment"
                                    dialog={dialog}
                                    onCloseDialog={() => this.setState(() => ({ dialog: false }))}
                                />
                            </FormControl>}
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} className="verify-box">
                        <h5>Verification Requirements</h5>
                        <ul className="verify-item">
                            <li className="verify-item">
                                <MyRadio onChange={() => this.setVerification(0)} value={formValues.verification === 0} name="verification" label="Concreet Verification" content="Concreet Verification is a one-time background check option. The renter will be charged $20 for this. The Background Check results will be displayed on the Renter's Resume once complete. Results typically take 24-48 hours to populate." />
                            </li>
                            <li><MyRadio onChange={() => this.setVerification(1)} value={formValues.verification === 1} name="verification" label="Self-Provided Verification" content="Either the Landlord or Agent will be providing background check services" /></li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} sm={12} className="#">
                        <h5>Landlord</h5>
                        <MyInput
                            name="landlord_email"
                            placeholder="Type in Landlord's Email"
                            validation={validations.landlord_emailV}
                            onChange={this.handleChange}
                            value={formValues.landlord_email}
                            disabled={listingDisabled}
                        />
                    </Grid>

                    <Grid item xs={10} sm={12} className="#">
                        <h5 className="mb-20">Lease Documents and Retainers</h5>
                        <div className="upload-list full-small">
                            {documents.map((file, index) => (
                                <Upload key={index}
                                    accept="application/pdf"
                                    onDrop={accept => this.uploadFile(accept)}
                                    placeholder={pddplaceholderImg}
                                    file={file.document_id !== 0 ? file : null}
                                    onDelete={() => this.deleteFile(file.document_id)} />
                            ))}

                        </div>
                    </Grid>
                    <Grid item xs={12} className="btn-bottom">
                        <MyButton
                            className="sg-btn"
                            name={values ? "Update Application" : "Create an Application"}
                            onClick={this.createApplication}
                            disabled={checkValid(validations) || createApplicationStore.phase === 'LOADING' || !accounts.length}
                            loading={createApplicationStore.phase === 'LOADING'}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12} className="#">
                        <button className="btn btn-small btn-transparent-sm" onClick={() => this.props.onSuccess()}>back </button>
                    </Grid>
                </Grid>
                <ConfirmDialog ref="dialog" />
            </div>
        );
    }
}

export default withRouter(CreateApplicationMain)
