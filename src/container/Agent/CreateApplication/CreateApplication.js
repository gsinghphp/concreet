import React from "react";
import Grid from "@material-ui/core/Grid";
import { toast } from "react-toastify";
import moment from 'moment';
import { debounce } from 'throttle-debounce';
import { validate, checkValid } from "../../../utils/validations";
import { searchString, fullListingName, getUserEmail } from '../../../utils/methods';
import { MyButton, MyInput } from "../../../components";
import { CreateApplicationMainRedux } from "./index";
import { searchByEmail, autocompleteEmail } from "../../../store/application/action";


class CreateApplication extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    props.clearForm();
  }

  static getDerivedStateFromProps(props, state) {
    const updates = {}
    const { addListing: { phase } } = props;

    if (phase === 'SUCCESS') {
      updates['showMain'] = true;
      props.clearIdxListing();
    }

    return updates
  }

  get initialState() {
    return {
      formValues: {
        email: "",
        listingAddressInput: "",
        listingAddress: "",
      },
      validations: {
        emailV: null,
      },
      errors: null,
      loading: false,
      showListingField: false,
      showMain: false,
      searchListings: [],
      renteremails: []
    };
  }

  handleChange = e => {
    let { formValues, validations } = this.state;
    const { name, value } = e.target;
    formValues[[name]] = value;
    validations = validate(validations, name, value);
    if (name === 'listingAddressInput') {
      const searchListings = searchString(value, this.props.listings)
      this.setState(() => ({ searchListings }));
      if (value === "") {
        validations["listingAddressV"] = null;
        this.setState(() => ({ searchListings }));
      }
    }

    if (name === 'email') {
      this.getRenters(value.toLowerCase())
    }
    this.setState(() => ({
      formValues,
      validations
    }));
    this.props.addValues(formValues)
  };

  checkValidations = () => {
    const { validations } = this.state;
    return checkValid(validations);
  };

  clearSuggestionsAndCreate = () => {
    let renter = this.checkIfEmailExists()
    if (renter) {
      this.selectRenter(renter)
    }
    this.setState({renteremails: []})
    this.createApplication()
  }

  createApplication = () => {
    const {
      formValues: { email, listingAddress },
      formValues,
      validations
    } = this.state;
    const { history, applicationStore: { values } } = this.props;
    if (values !== null) {
      if (values.status === 6 || values.status === 5)
        history.push(`/view-application/${values.agent_listing_id}`)
    }

    if (getUserEmail() === email) {
      return toast.error('Own emailid can not be used')
    }


    if (typeof validations.listingAddressV !== 'undefined') {
      if (typeof listingAddress.agent_id === 'undefined') {
        this.props.addAgentListing(listingAddress.listing_id);
      } else {
        this.setState(() => ({ showMain: true }))
      }
    } else if (values) {
      const { listing: { details } } = values
      formValues['listingAddressInput'] = fullListingName(details)
      this.setState(() => ({ showMain: true, formValues }))
    } else {
      this.findAlreadyListing(email);
    }
  };

  selectListing = (listing) => {
    const { validations, formValues } = this.state;
    validations["listingAddressV"] = true;
    formValues["listingAddressInput"] = fullListingName(listing)
    formValues["listingAddress"] = listing
    this.setState(() => ({
      searchListings: [],
      validations,
      formValues
    }))
    this.props.addValues(formValues)
  }

  selectRenter = renter => {
    const {
      formValues,
      validations
    } = this.state;
    const { addValues } = this.props;
    formValues['renter'] = renter;
    formValues['email'] = renter.email;
    validations["emailV"] = true;
    this.setState(() => ({ formValues, renteremails: [], validations }))
    addValues(formValues);
    this.findAlreadyListing(renter.email);
  }

  findAlreadyListing = email => {
    const { putApiValues } = this.props;
    const { validations, formValues } = this.state;
    searchByEmail(email)
      .then(res => {
        formValues["listingAddress"] = res.message[0].listing
        putApiValues(res.message[0])
        validations["listingAddressV"] = true;
        formValues["listingAddressInput"] = fullListingName(res.message[0].listing.details)
        this.setState(() => ({ validations, showListingField: false, formValues }))
      })
      .catch(err => {
        // if (err.message === "No Applications Found") {
        validations["listingAddressV"] = null;
        this.setState(() => ({ validations, showListingField: true }))
        this.props.getAgentListing();
        // }
      });
  }

  getRenters = debounce(500, (value) => {
    autocompleteEmail(value).then(res => {
      this.setState(() => ({
        renteremails: res.message
      }))
    });
  });

  checkIfEmailExists () {
    if (this.state.renteremails.length && this.state.formValues.email) {
      for (let i = 0; i < this.state.renteremails.length; i++) {
        if (this.state.renteremails[i]['email'] === this.state.formValues.email) {
          return this.state.renteremails[i]
        }
      }
      return false
    }
  }

  initPage = () => {
    this.setState(this.initialState, () => {
      this.props.clearForm();
    });
  }

  render() {
    const {
      formValues: { email, listingAddressInput, renter },
      validations,
      showListingField,
      showMain,
      searchListings,
      formValues,
      renteremails
    } = this.state;
    const { addValues, applicationStore: { values } } = this.props;
    return (
      <div className={`${showMain ? 'wrapper-inner' : 'wrapper-login'} `}>
        <div className="container center-align">
          {!showMain ? <>
            <h1> Find or Create an Application </h1>
            <Grid container spacing={16} className="center-align r-email">
              <Grid item xs={12} sm={6} md={4} className=" r-email">
                <MyInput
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  placeholder="Renter's Email"
                  validation={validations.emailV}
                  disabled={!!renter}
                  suggestions={
                    renteremails.length ? (
                      <div className="option-list">
                        {
                          renteremails.map(renter => (
                            <div style={{ textAlign: 'center' }} className="option-item" onClick={() => this.selectRenter(renter)} key={renter.id}>
                              {renter.email}
                            </div>
                          ))
                        }
                      </div>
                    ) : <></>
                  }
                />
                {(values && !!formValues.renter) &&
                  <div className="color-key-1">
                    {`An Application was opened for ${formValues.renter.firstname !== null ? `${formValues.renter.firstname} ${formValues.renter.lastname}` : formValues.renter.email}
                    on ${moment(values.created_at).format('MM/DD/YYYY')} for ${fullListingName(values.listing.details)}`}
                  </div>}
                {showListingField && (
                  <>
                    <MyInput
                      name="listingAddressInput"
                      value={listingAddressInput}
                      onChange={this.handleChange}
                      placeholder="Listing Address"
                      validation={validations.listingAddressV}
                      suggestions={
                        (listingAddressInput !== "" && searchListings.length) ?
                          <div className="option-list">
                            {
                              searchListings.map(listing => (
                                <div style={{ textAlign: 'center' }} className="option-item" onClick={() => this.selectListing(listing)} key={listing.id}>
                                  {fullListingName(listing)}
                                </div>
                              ))
                            }
                          </div> : <></>
                      }
                    />
                  </>
                )}
              </Grid>
              <Grid item xs={12}>
                <MyButton
                  className="sg-btn"
                  name="Next"
                  onClick={this.clearSuggestionsAndCreate}
                  disabled={checkValid(validations)}
                />
                {!checkValid(validations) &&
                  <Grid container spacing={24} justify="center" className="center-align ">
                    <Grid item xs={12} sm={12} className="#">
                      <button className="btn btn-small btn-transparent-sm" onClick={this.initPage}>back </button>
                    </Grid>
                  </Grid>
                }
              </Grid>
            </Grid>
          </> : <CreateApplicationMainRedux
              formValues={formValues}
              addValues={addValues}
              onSuccess={this.initPage} />}
        </div>
      </div>
    );
  }
}

export default CreateApplication;
