import CreateApplication from "./CreateApplication";
import CreateApplicationMain from "./CreateApplicationMain";
import { connect } from "react-redux";
import { getAgentIDXListing, insertAgentListing, clearIdxListing } from "../../../store/agentListing/action";
import { putAppValues, addApplication, putApiValues, clearApplicationForm, inviteLandlord, clearCreateApplication } from '../../../store/application/action';
import { uploadListingDoc, getListingDoc, deleteDocument } from '../../../store/documents/action';
import { getStripeAccounts, changeStatus } from '../../../store/stripeaccounts/action'

const mapStateToProps = ({ idxListingStore, insertAgentListingStore, applicationStore }) => ({
  listings: idxListingStore.listings,
  addListing: insertAgentListingStore,
  applicationStore
});
const mapDispatchToProps = {
  getAgentListing: value => getAgentIDXListing(value),
  addValues: values => putAppValues(values),
  addAgentListing: listingId => insertAgentListing(listingId),
  uploadDoc: data => uploadListingDoc(data),
  putApiValues: values => putApiValues(values),
  clearForm: () => clearApplicationForm(),
  clearIdxListing: () => clearIdxListing(),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateApplication);


const mapStateToPropsMain = ({idxListingStore, stripeAccountStore, documentsStore, insertAgentListingStore, listingDocumentsStore, applicationStore, createApplicationStore }) => ({
  listingAdded: insertAgentListingStore.listing,
  documents: listingDocumentsStore.documents,
  applicationStore,
  uploadDocuments: documentsStore,
  createApplicationStore,
  accounts: stripeAccountStore.accounts,
  listings: idxListingStore.listings,
});


const mapDispatchToPropsMain = {
  uploadDoc: (data, listingId) => uploadListingDoc(data, listingId),
  getAgentListing: value => getAgentIDXListing(value),
  clearCreateApplication: () => clearCreateApplication(),
  getListingDoc: listingID => getListingDoc(listingID),
  deleteDocument: (documentId, listingID) => deleteDocument({ documentId, listingID }),
  inviteLandlord: (landlord, application) => inviteLandlord({landlord, application }),
  createApplication: (renter_id, landlord, application) => addApplication({ renter_id, landlord, application }),
  getStripeAccounts: () => getStripeAccounts(),
  changeStatus: id => changeStatus(id)
};

const CreateApplicationMainRedux = connect(mapStateToPropsMain, mapDispatchToPropsMain)(CreateApplicationMain)

export {
  CreateApplicationMainRedux
}
