import { connect } from 'react-redux';
import { getLandlordListing, addSignature } from '../../../store/application/action';
import { getStripeAccounts, changeStatus } from '../../../store/stripeaccounts/action'
import { addDocumentSignatures, deleteDocumentSignatures } from '../../../store/signature/action';
import ViewApplication from './ViewApplication';

const mapStateToProps = ({ applicationStore, authStore, stripeAccountStore, signatureStore}) => ({
    resume: applicationStore.resume,
    listing: applicationStore.renterApplication.length ? applicationStore.renterApplication[0] : {},
    user: authStore.user ? authStore.user.message.user : authStore.user,
    accounts: stripeAccountStore.accounts,
    signatureStore
})

const mapDispatchToProps = {
    getLandlordListing: id => getLandlordListing(id),
    addSignature: data => addSignature(data),
    getStripeAccounts: () => getStripeAccounts(),
    changeStatus: id => changeStatus(id),
    addDocumentSignatures: (applicationid, docId) => addDocumentSignatures({applicationid, docId}),
    deleteDocumentSignatures: (id, appId) => deleteDocumentSignatures({id, appId})
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewApplication);
