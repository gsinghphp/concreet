import React from 'react';
import Grid from "@material-ui/core/Grid";
import moment from 'moment';
import { toast } from "react-toastify";
import CircularProgress from '@material-ui/core/CircularProgress';
import RenterResume from '../../../components/RenterResume';
import LeasePackage from '../../../components/LeasePackage';
import { buyApplication } from '../../../store/application/action';
import { Title, MyButton } from '../../../components';
import { modifyBackAccount, getActiveAccount, getSignatures, getRenterSignatures } from '../../../utils/methods';

class ViewApplication extends React.Component {

    state = {
        formValues: {
            payment: null,
        },
        showApplication: true,
        loadedListing: false,
        listing: {},
        dialog: false,
        applicationValues: {},
        loading: false,
    }

    componentDidMount() {
        const { getLandlordListing, getStripeAccounts, match: { params: { id } } } = this.props;
        getLandlordListing(id);
        getStripeAccounts();
    }

    static getDerivedStateFromProps(props, state) {
        const updates = {}
        if (props.listing.listing && !state.loadedListing) {
            updates.listing = props.listing.listing
            updates.applicationValues = props.listing
            updates.loadedListing = true
        }

        return updates
    }

    gotoLease = () => {
        this.setState((state) => ({
            showApplication: !state.showApplication
        }))
    }

    setSignature = (docId, value, sigid) => {
        const { listing, deleteDocumentSignatures, addDocumentSignatures } = this.props
        if (value === "true") {
            deleteDocumentSignatures(sigid.id, listing.id)
        } else {
            addDocumentSignatures(listing.id, docId)
        }
        // console.log(docIndex, value, 'change')
        // const { listing } = this.state;
        // //const {value} = event.target
        // // console.log(listing, value)
        // listing.documents[docIndex].signature.push({ id: 0, key: docIndex })

        // this.setState(() => ({
        //     listing
        // }))

    }

    handleChange = e => {
        let { formValues } = this.state;
        let { name, value } = e.target;
        formValues[[name]] = value;
        if (name === 'payment' && value === 'add') {
            this.setState(() => ({ dialog: true }))
        }
        if (name === 'payment' && value !== 'add') {
            this.props.changeStatus(value)
        }

        this.setState(() => ({
            formValues,
        }));
    };

    handleSubmit = () => {

        const { listing: { documents }, showApplication } = this.state;
        if (showApplication) {
            this.gotoLease()
        } else {
            const { listing, addSignature } = this.props;
            const listing_documents = documents.reduce((acc, doc) => {
                doc.signature.map(sig => {
                    if (sig.id === 0) {
                        return acc.push(doc.id)
                    }

                    return false;

                })
                return acc;
            }, [])

            if (listing_documents.length) {
                addSignature({ listing_documents, applicationId: listing.id })
            }
            this.setState(() => ({ loading: true }))
            buyApplication(listing.listing.id).then(res => {
                toast.success(res.message)
                this.props.history.push('/my-listings');
                this.setState(() => ({ loading: false }))
            }).catch(err => this.setState(() => ({ loading: false })))
        }
    }

    render() {
        const { showApplication, listing, applicationValues, dialog, formValues, loading } = this.state;
        const { user, resume, accounts, signatureStore: { signatures } } = this.props;
        const { address, income, bank_accounts, documents, background_questions, user: renterInfo } = resume;
        let userDocuments = []
        if (documents) {
            userDocuments = documents.reduce((acc, doc) => {
                acc[doc.document_type_id] = doc;
                return acc
            }, {})
        }

        console.log(applicationValues)
        if (Object.keys(listing).length) {
            return (
                <div className="wrapper-lease wrapper-renter">
                    {loading && <div className="loading"><CircularProgress /></div>}
                    <div className="lease-top">
                        <Grid container spacing={24} justify="center" className="center-align ">
                            <Grid item xs={12} sm={12} className="#">
                                {user && (
                                    <>
                                        <Title type="Application" />
                                        {!!resume.user &&
                                            <div className="renter-top-detail">
                                                <p>{resume.user.email}</p>
                                                <p>Applied at {moment(listing.updated_at).format('HH:mm a')} on {moment(listing.updated_at).format('MM/DD/YYYY')}</p>
                                            </div>}

                                    </>
                                )}
                            </Grid>
                        </Grid>
                        {showApplication ? (
                            <RenterResume
                                addresses={address}
                                incomes={income}
                                accounts={bank_accounts}
                                userDocuments={userDocuments}
                                background_questions={background_questions}
                                disabled />
                        ) : (
                                Object.keys(listing).length && (
                                    <LeasePackage
                                        showSignsInfo
                                        renter={renterInfo}
                                        landlordInfo={user}
                                        handleChange={this.handleChange}
                                        paymentValue={getActiveAccount(accounts) || formValues.payment}
                                        listing={listing}
                                        landlord
                                        onSelectSignature={this.setSignature}
                                        applicationValues={applicationValues}
                                        backAccounts={modifyBackAccount(accounts)}
                                        dialog={dialog}
                                        type="renter"
                                        activeLandlordSignature
                                        signatures={signatures}
                                        onCloseDialog={() => this.setState(() => ({ dialog: false }))} />
                                )
                            )}
                        <MyButton
                            name={showApplication ? "Next" : (applicationValues.status === 6 ? 'Application Completed' : 'Complete Application')}
                            onClick={this.handleSubmit}
                            disabled={(!showApplication && !accounts.length) ||
                                (!showApplication && applicationValues.status === 6) ||
                                (!showApplication && !getSignatures(signatures, "Landlord")) ||
                                (!showApplication && !getRenterSignatures(signatures)) }
                        />
                        {!showApplication &&
                            <Grid container spacing={24} justify="center" className="center-align ">
                                <Grid item xs={12} sm={12} className="#">
                                    <button className="btn btn-small btn-transparent-sm" onClick={this.gotoLease}>back </button>
                                </Grid>
                            </Grid>
                        }

                    </div>
                </div>
            )
        } else {
            return (
                <div className="wrapper-lease">
                    No Listing Found
            </div>
            )
        }

    }
}

export default ViewApplication;
