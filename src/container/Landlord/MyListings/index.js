import { connect } from 'react-redux';
import MyListings from './MyListings';
import { clearApplicationForm } from '../../../store/application/action';

const mapDispatchToProps = {
    clearApplicationForm: () => clearApplicationForm(),
}

export default connect(null, mapDispatchToProps)(MyListings);
