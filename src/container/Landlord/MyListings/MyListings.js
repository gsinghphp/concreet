import React from 'react';
import { Link } from 'react-router-dom'
import Grid from "@material-ui/core/Grid";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { getLandlordListings } from '../../../store/agentListing/action';
import {applicationStatus} from '../../../utils/constants';


import { fullListingName } from '../../../utils/methods';

class MyListings extends React.Component {

    state = {
        listings: []
    }
    componentDidMount() {
        getLandlordListings().then(({ message }) => {
            console.log(message)
            this.setState(() => ({ listings: message }))
        }).catch(() => { })
        this.props.clearApplicationForm();
    }
    render() {
        const { listings } = this.state;
        return (
            <div className="wrapper-lease container ">
                <Grid container spacing={24} justify="center" className="center-align ">
                    <Table className="listing-table-full">
                        <TableHead>
                            <TableRow className="listing-table">
                                <TableCell>Listing ID</TableCell>
                                <TableCell align="right">Address</TableCell>
                                <TableCell align="right">Renter</TableCell>
                                <TableCell align="right">Agent</TableCell>
                                <TableCell align="right">Status</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {listings.map(listing => (
                                <TableRow key={listing.id}>
                                    <TableCell component="th" scope="row">
                                        <Link to={`/view-application/${listing.id}`}>{listing.details.listing_id}</Link>
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {fullListingName(listing.details)}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {listing.renter_application_email}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {listing.agent_details.email}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {applicationStatus[listing.renter_applications_status]}
                                    </TableCell>
                                </TableRow>
                            ))}

                        </TableBody>
                    </Table>
                </Grid>
            </div>
        )

    }
}

export default MyListings;
