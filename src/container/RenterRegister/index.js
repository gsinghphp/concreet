import RenterRegister from "./RenterRegister";
import { connect } from "react-redux";
import { registerRenter, setInitialAuth } from "../../store/register/action";

const mapStateToProps = ({ agentRegisterStore }) => agentRegisterStore;
const mapDispatchToProps = {
    register: user => registerRenter(user),
    clear: () => setInitialAuth(),
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RenterRegister);
