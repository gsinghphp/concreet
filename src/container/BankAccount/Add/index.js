import { connect } from 'react-redux';
import { addStripeAccount, initStripe } from '../../../store/stripeaccounts/action';
import AddBankAccount from './AddBankAccount'

const mapDispatchToProps = {
    addStripeAccount: data => addStripeAccount(data),
    initStripe: () => initStripe()
}

const mapStateToProps = ({ stripeAccountStore }) => ({
    stripeAccountStore
})
export default connect(mapStateToProps, mapDispatchToProps)(AddBankAccount);
