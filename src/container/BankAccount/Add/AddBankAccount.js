import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import { MyButton } from '../../../components';
import { validate } from '../../../utils/validations';
import Plaid from '../../../components/Plaid';
class AddBankAccount extends React.Component {

  static defaultProps = {
    onClose: () => { }
  }

  state = {
    showAccountForm: false,
    showAccountForm2: false,
    verify: false,
    showVerify: true,
    formValues: {
      entity_type: "",
      address: "",
      address2: "",
      street_name: "",
      street_number: "",
      apartment_number: "",
      state: "",
      city: "",
      state_abbr: "",
      zipcode: "",
      country: "",
      country_abbr: "",
      dob: "",
      company_ein: "",
      last_4_of_ssn: "",
      bank_name: "",
      account_number: "",
      routing_number: "",
      public_token: "",
      account: {},
      inititution: {}
    },
    validations: {
      addressV: null,
      cityV: null,
      stateV: null,
      zipcodeV: null,
      dobV: null,
      last_4_of_ssnV: null,
      company_einV: null,
    },
    validationStripe: {
      bank_nameV: null,
      account_numberV: null,
      routing_numberV: null,
    }
  }

  static getDerivedStateFromProps(props, state) {
    const updates = {}
    if (props.stripeAccountStore.addPhase === 'SUCCESS') {
      props.onClose();
      props.initStripe();
    }

    return updates
  }

  handleChange = (e) => {
    const { name, value } = e.target
    console.log(name, value)
    let { formValues, validations } = this.state;
    formValues[[name]] = value;
    validations = validate(validations, name, value);

    this.setState(() => ({
      formValues,
      validations
    }))

    if (name === 'entity_type') {
      this.setState(() => ({
        showAccountForm: true
      }))
    }
  }

  handleChangeStripe = (e) => {
    const { name, value } = e.target
    let { formValues, validationStripe } = this.state;
    formValues[[name]] = value;
    validationStripe = validate(validationStripe, name, value);

    this.setState(() => ({
      formValues,
      validationStripe
    }))
  }

  showAccount = () => {
    this.setState(() => ({
      showAccountForm: true,
      showAccountForm2: false,
      showVerify: false
    }))
  }

  addAccount = () => {
    // this.setState(() => ({
    //   showAccountForm: false,
    //   showAccountForm2: true,
    //   showVerify: false
    // }))
    this.props.onClose();
  }

  showVerify = () => {
    this.setState(() => ({
      showVerify: true,
      showAccountForm2: false
    }))
  }

  saveData = (data) => {
    const { addStripeAccount } = this.props;
    const { formValues } = this.state;
    formValues.public_token = data.public_token;
    formValues.account = data.account;
    formValues.inititution = data.institution;
    addStripeAccount(this.state.formValues)
  }

  render() {
    const { showVerify } = this.state;
    const { stripeAccountStore: { addPhase } } = this.props

    return (
      <div className="dialog-inner">
        {addPhase === 'LOADING' && <div>
          <CircularProgress color="secondary" />
        </div>}
        {/*
        <h5>{showVerify ? 'Bank Account Verification' : 'Add a Bank Account'}</h5>
        {showAccountForm && (
          <p>This information is required by Stripe to set up a payment method for the first time. We do not store this information and it is encrypted in transfer.</p>
        )}
        {(!showVerify && !showAccountForm2) &&
          <MySelect
            className="demo"
            placeholder="Entity Type"
            values={[{
              id: 1,
              name: 'Individual'
            }, {
              id: 2,
              name: 'Business'
            }]}
            value={formValues.entity_type}
            name="entity_type"
            onChange={this.handleChange}
          />
        }

        {showAccountForm &&
          <>
            <BankAddressForm
              values={formValues}
              onChange={this.handleChange}
              validations={validations}
              type={formValues.entity_type} />
            <MyButton name="Next" onClick={this.addAccount} disabled={checkValid(validations, formValues.entity_type)} />
          </>
        }

        {showAccountForm2 &&
          <>
            <BankAccountForm values={formValues} onChange={this.handleChangeStripe} validations={validationStripe} />
            <MyButton name="Next" onClick={this.showVerify} disabled={checkValid(validationStripe)} />
            <button className="mini-btn" onClick={this.showAccount}>back</button>
          </>
        } */}

        {showVerify &&
          <>
            <p>In order to complete adding a Payment Method, we must verify your bank account. Using the Plaid Secure Login, you can safely log into your bank account, which then automatically verifies your account.</p>
            <Plaid onSuccess={data => this.saveData(data)}>
              <MyButton name="Verify my account" />
            </Plaid>
            <button className="mini-btn" onClick={this.addAccount}>back</button>
          </>
        }

      </div>
    );
  }
}

export default AddBankAccount;
