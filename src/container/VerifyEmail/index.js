import React from "react";
import axios from 'axios';
import { toast } from 'react-toastify';
import { API_URL } from '../../utils/constants';

class VerifyEmail extends React.Component {
  componentWillMount() {
    const {
      match: {
        params: { hash }
      }
    } = this.props;
    axios.get(`${API_URL}auth/verifyEmail/${hash}`).then(res=>{
        toast.success('Email Verified Successfully');
        this.props.history.push('/login')
    }).catch(err=>{
        this.props.history.push('/login')
    })
  }

  render() {
    return <div />;
  }
}

export default VerifyEmail;
