import { GET_INCOME, SAVE_INCOME, DELETE_INCOME_DOCUMENT, SET_INIT_INCOME } from './constants';


const getIncomes = () => ({
    type: GET_INCOME,
})

const initIncome = () => ({
    type: SET_INIT_INCOME
})

const saveIncome = (payload, documents) => ({
    type: SAVE_INCOME,
    payload,
    documents
})

const deleteIncomeDoc = (incomeID, docId) => ({
    type: DELETE_INCOME_DOCUMENT,
    payload: {
        incomeID,
        docId
    }
})

export { getIncomes, saveIncome, deleteIncomeDoc, initIncome };
