import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

import { API_URL } from "../../utils/constants";

const api = {
    getIncomes: () => {
        const request = axios
            .get(`${API_URL}users/income/`)
            .then(response => response);
        return fromPromise(request);
    },
    saveIncome: (data) => {
        if (data.id) {
            const request = axios
                .put(`${API_URL}users/income/`, { income: data })
                .then(response => response);
            return fromPromise(request);
        } else {
            const request = axios
                .post(`${API_URL}users/income/`, { income: data })
                .then(response => response);
            return fromPromise(request);
        }

    },
    addIncomeDoc: payload => {
        const request = axios
            .post(`${API_URL}users/income/documents/`, payload)
            .then(response => response);
        return fromPromise(request);
    },
    deleteIncomeDoc: ({ incomeID, docId }) => {
        const request = axios
            .delete(`${API_URL}users/income/documents/`, {
                data: {
                    "income": {
                        "id": incomeID
                    },
                    "document": {
                        "id": docId
                    }
                }
            })
            .then(response => response);
        return fromPromise(request);
    }
};

export default api;
