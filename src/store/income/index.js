import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";
import {
    GET_INCOME,
    GET_INCOME_SUCCESS,
    GET_INCOME_ERROR,
    SAVE_INCOME,
    SAVE_INCOME_SUCCESS,
    DELETE_INCOME_DOCUMENT,
    SET_INIT_INCOME,
} from "./constants";
import { UPLOAD_DOCUMENT, ADD_INCOME_DOCUMENT, UPLOAD_DOCUMENT_SUCCESS } from '../documents/constants';

const initialState = {
    phase: INIT,
    incomes: [],
    error: null
};

const incomeStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_INCOME:
            return initialState;
        case GET_INCOME_SUCCESS:
            return {
                ...state,
                incomes: action.payload,
                phase: SUCCESS,
                error: null
            };
        case GET_INCOME_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };
        case SAVE_INCOME:
            return {
                ...state,
            };
        case SET_INIT_INCOME:
            return {
                ...state,
                phase: INIT
            }

        default:
            return state;
    }
};

const getIncomesEpic = action$ =>
    action$.pipe(
        ofType(GET_INCOME),
        mergeMap(action =>
            api.getIncomes().pipe(
                map(({ message }) => message),
                map(accounts => accounts.map((account, index) => {
                    account['change'] = false;
                    return account
                })),
                map(payload => ({ type: GET_INCOME_SUCCESS, payload })),
                catchError(error =>
                    of({
                        type: GET_INCOME_ERROR,
                        error
                    })
                )
            )
        )
    );

const saveIncomeEpic = action$ => action$.pipe(
    ofType(SAVE_INCOME),
    mergeMap(({ payload, documents }) => api.saveIncome(payload).pipe(
        mergeMap(({ data }) => {
            const uploads = []
            documents.map((doc) => uploads.push(uploadDocuments(data.id, doc)))
            return of(
                ...uploads,
                saveIncomeSuccess(),
            )
        }),
        catchError(error =>
            of({
                type: GET_INCOME_ERROR,
                error
            })
        )
    )
    )
);

const saveIncomeSuccess = () => ({ type: SAVE_INCOME_SUCCESS });

const uploadDocuments = (incomeId, doc) => ({
    type: UPLOAD_DOCUMENT,
    payload: {
        type: 10,
        doc
    },
    docType: 'income',
    incomeId
})

const addIncomeDocEpic = action$ => action$.pipe(
    ofType(ADD_INCOME_DOCUMENT),
    mergeMap(action =>
        api.addIncomeDoc(action.payload).pipe(
            mergeMap(() => of(
                incomeSuccess(),
                uploadSuccess()
            )),
            catchError(error => {
                return of({
                    type: GET_INCOME_ERROR,
                    error: error.errors
                });
            })
        )
    )
);
const incomeSuccess = () => ({ type: 'SUCCESS_INCOME_DOCUMENT' });
const uploadSuccess = () => ({ type: UPLOAD_DOCUMENT_SUCCESS });

const deleteIncomeDocEpic = action$ => action$.pipe(
    ofType(DELETE_INCOME_DOCUMENT),
    mergeMap(action =>
        api.deleteIncomeDoc(action.payload).pipe(
            map(() => ({ type: 'SUCCESS_INCOME_DOCUMENT' })),
            catchError(error => {
                return of({
                    type: GET_INCOME_ERROR,
                    error: error.errors
                });
            })
        )
    )
);

export {
    getIncomesEpic,
    saveIncomeEpic,
    addIncomeDocEpic,
    deleteIncomeDocEpic,
}

export default incomeStore;
