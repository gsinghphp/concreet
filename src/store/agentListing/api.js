import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
    searchListing: text => {
        const request = axios
            .get(`${API_URL}idx/search/?offset=0&limit=5&text=${text}`)
            .then(response => response);
        return fromPromise(request);
    },
    getIDXAgentListing: () => {
        const request = axios
            .post(`${API_URL}idx/agent?offset=0&limit=10`)
            .then(response => response);
        return fromPromise(request);
    },
    getAgentListing: () => {
        const request = axios
            .get(`${API_URL}agent/listings?offset=0&limit=10`)
            .then(response => response);
        return fromPromise(request);
    },
    getLandlordListings: () => {
        return axios
            .get(`${API_URL}landlord/listings/`)
            .then(response => response);
    },
    addAgentListing: listingId => {
        const request = axios
            .post(`${API_URL}agent/listings/`, {
                "listings": [
                    {
                        "listing_id": listingId
                    }
                ]
            })
            .then(response => response);
        return fromPromise(request);
    }
};

export default api;
