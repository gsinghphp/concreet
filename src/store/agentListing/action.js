import { GET_IDX_AGENT_LISTING, INSERT_IDX_AGENT_LISTING, CLEAR_IDX_LISTING } from './constants';
import api from './api';
const getAgentIDXListing = () => ({
    type: GET_IDX_AGENT_LISTING
});

const insertAgentListing = listingId => ({
    type: INSERT_IDX_AGENT_LISTING,
    listingId,
});

const clearIdxListing = () => ({
    type: CLEAR_IDX_LISTING,
});


const getLandlordListings = () => api.getLandlordListings();

export {
    getAgentIDXListing,
    insertAgentListing,
    getLandlordListings,
    clearIdxListing,
}
