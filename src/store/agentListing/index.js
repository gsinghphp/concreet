import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR, LOADING } from "../../utils/constants";

import {
  GET_IDX_AGENT_LISTING,
  GET_IDX_AGENT_LISTING_SUCCESS,
  GET_IDX_AGENT_LISTING_ERROR,
  INSERT_IDX_AGENT_LISTING,
  INSERT_IDX_AGENT_LISTING_SUCCESS,
  INSERT_IDX_AGENT_LISTING_ERROR,
  GET_AGENT_LISTING,
  CLEAR_IDX_LISTING,
} from "./constants";

const initialState = {
  phase: INIT,
  error: null,
  listings: []
};

/*Reducer */
const idxListingStore = (state = initialState, action) => {
  switch (action.type) {
    case GET_IDX_AGENT_LISTING:
      return initialState;
    case GET_IDX_AGENT_LISTING_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        listings: action.payload,
        error: null
      };
    case GET_IDX_AGENT_LISTING_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };

    default:
      return state;
  }
};

/*Epics */

const agentIDXListingEpic = action$ =>
  action$.pipe(
    ofType(GET_IDX_AGENT_LISTING),
    mergeMap(() =>
      api.getIDXAgentListing().pipe(
        map(({ message }) => {
         // if (message.length) {
          return ({ type: GET_IDX_AGENT_LISTING_SUCCESS, payload: message })
         // }
         // return ({ type: GET_AGENT_LISTING })

        }),
        catchError(error =>
          of({
            type: GET_IDX_AGENT_LISTING_ERROR,
            error
          })
        )
      )
    )
  );

const agentListingEpic = action$ =>
  action$.pipe(
    ofType(GET_AGENT_LISTING),
    mergeMap(() =>
      api.getAgentListing().pipe(
        map(({ message }) => message.map(({ details, id }) => {
          //  details['agent_id'] = id
          return details
        })),
        map(payload => ({ type: GET_IDX_AGENT_LISTING_SUCCESS, payload })
        ),
        catchError(error =>
          of({
            type: GET_IDX_AGENT_LISTING_ERROR,
            error
          })
        )
      )
    )
  );



const initialStateAgent = {
  phase: INIT,
  error: null,
  listing: []
};

const insertAgentListingStore = (state = initialStateAgent, action) => {
  switch (action.type) {
    case INSERT_IDX_AGENT_LISTING:
      return {
        ...state,
        phase: LOADING,
      };
    case INSERT_IDX_AGENT_LISTING_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        listing: action.payload.data,
        error: null
      };
    case INSERT_IDX_AGENT_LISTING_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };
    case CLEAR_IDX_LISTING:
      return initialStateAgent

    default:
      return state;
  }
};

const agentListingInsertEpic = action$ =>
  action$.pipe(
    ofType(INSERT_IDX_AGENT_LISTING),
    mergeMap(({ listingId }) =>
      api.addAgentListing(listingId).pipe(
        map(payload => ({ type: INSERT_IDX_AGENT_LISTING_SUCCESS, payload })),
        catchError(error =>
          of({
            type: INSERT_IDX_AGENT_LISTING_ERROR,
            error
          })
        )
      )
    )
  );


export { agentIDXListingEpic, agentListingEpic, agentListingInsertEpic };

export {
  insertAgentListingStore
}
export default idxListingStore;
