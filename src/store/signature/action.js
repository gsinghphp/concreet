import { GET_DOCUMENT_SIGNATURES, ADD_DOCUMENT_SIGNATURES, DELETE_DOCUMENT_SIGNATURES } from './constants';

const getDocumentSignatures = payload => ({
    type: GET_DOCUMENT_SIGNATURES,
    payload
})

const addDocumentSignatures = payload => ({
    type: ADD_DOCUMENT_SIGNATURES,
    payload
})

const deleteDocumentSignatures = payload => ({
    type: DELETE_DOCUMENT_SIGNATURES,
    payload
})

export {
    getDocumentSignatures,
    addDocumentSignatures,
    deleteDocumentSignatures
}
