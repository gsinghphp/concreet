import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
    getSignatures: (applicationId) => {
        const request = axios
            .get(`${API_URL}application/${applicationId}/document_signature/`)
            .then(response => response);
        return fromPromise(request);
    },
    addSignatures: ({ applicationid, docId }) => {
        const request = axios
            .post(`${API_URL}application/document_signature/`, {
                "application": {
                    "id": applicationid
                },
                "listing_documents": [docId]
            })
            .then(response => response);
        return fromPromise(request);
    },
    deleteSignatures: ({ id }) => {
        const request = axios
            .delete(`${API_URL}application/document_signature`, {
                data: {
                    id
                }
            })
            .then(response => response);
        return fromPromise(request);
    },
};

export default api;
