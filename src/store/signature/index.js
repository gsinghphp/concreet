import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";
import {
    GET_DOCUMENT_SIGNATURES,
    GET_DOCUMENT_SIGNATURES_SUCCESS,
    GET_DOCUMENT_SIGNATURES_ERROR,
    ADD_DOCUMENT_SIGNATURES,
    DELETE_DOCUMENT_SIGNATURES
} from "./constants";

const initialState = {
    phase: INIT,
    signatures: [],
    error: null
};

const signatureStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_DOCUMENT_SIGNATURES:
            return {
                ...state,
                phase: INIT,
            };

        case GET_DOCUMENT_SIGNATURES_SUCCESS:
            return {
                ...state,
                signatures: action.payload.message,
                phase: SUCCESS,
                error: null
            };
        case GET_DOCUMENT_SIGNATURES_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };

        default:
            return state;
    }
};

const getSignaturesEpic = action$ =>
    action$.pipe(
        ofType(GET_DOCUMENT_SIGNATURES),
        mergeMap(action =>
            api.getSignatures(action.payload).pipe(
                map(payload => ({ type: GET_DOCUMENT_SIGNATURES_SUCCESS, payload })),
                catchError(error =>
                    of({
                        type: GET_DOCUMENT_SIGNATURES_ERROR,
                        error
                    })
                )
            )
        )
    );

const addSignaturesEpic = action$ =>
    action$.pipe(
        ofType(ADD_DOCUMENT_SIGNATURES),
        mergeMap(action =>
            api.addSignatures(action.payload).pipe(
                map(payload => ({ type: GET_DOCUMENT_SIGNATURES, payload: action.payload.applicationid })),
                catchError(error =>
                    of({
                        type: GET_DOCUMENT_SIGNATURES_ERROR,
                        error
                    })
                )
            )
        )
    );
const deleteSignaturesEpic = action$ =>
    action$.pipe(
        ofType(DELETE_DOCUMENT_SIGNATURES),
        mergeMap(action =>
            api.deleteSignatures(action.payload).pipe(
                map(payload => ({ type: GET_DOCUMENT_SIGNATURES, payload: action.payload.appId })),
                catchError(error =>
                    of({
                        type: GET_DOCUMENT_SIGNATURES_ERROR,
                        error
                    })
                )
            )
        )
    );

export { getSignaturesEpic, addSignaturesEpic, deleteSignaturesEpic }
export default signatureStore;
