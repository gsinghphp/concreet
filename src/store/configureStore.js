import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { persistStore } from 'redux-persist'
import { logger } from 'redux-logger'
import { rootEpic, rootReducer } from "./root";

let middleware;
const epicMiddleware = createEpicMiddleware();

if (process.env.NODE_ENV !== 'production') {
  middleware = applyMiddleware(epicMiddleware, logger)
} else {
  middleware = applyMiddleware(epicMiddleware)
}


export default () => {
  const store = createStore(
    rootReducer,
    middleware
  );

  const persistor = persistStore(store)

  epicMiddleware.run(rootEpic);

  return { store, persistor }
}
