import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
  loginUser: user => {
    const request = axios
      .post(API_URL + "auth/login", user)
      .then(response => response);
    return fromPromise(request);
  },
  refreshToken: ({ message }) => {
    const request = axios
      .post(API_URL + "auth/token", { user_id: message.user.id, refreshToken: message.refreshToken })
      .then(response => response);
    return fromPromise(request);
  },
  changePassword: user => {
    return axios
      .post(API_URL + "users/password", user)
      .then(response => response);
  },
  getEmailFromToken: data => {
    return axios
      .post(API_URL + "auth/getEmailToken", data)
      .then(response => response);
  }
};

export default api;
