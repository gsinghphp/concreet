import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import axios from "axios";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";

import {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_ERROR,
  SET_INITIAL_AUTH,
  USER_ADD_ADDRESS,
  REFRESH_TOKEN,
  CLEAR_ERROR
} from "./constants";

const initialState = {
  phase: INIT,
  user: null,
  error: null
};

/*Reducer */
const authStore = (state = initialState, action) => {
  if (typeof action.payload === "object") {
    if (action.payload.user) {
      axios.defaults.headers.common["Authorization"] = `JWT ${
        action.payload.user.message.token
        }`;
    }
  }
  switch (action.type) {
    case SET_INITIAL_AUTH:
    case USER_LOGIN:
      return initialState;
    case USER_LOGIN_SUCCESS:
      axios.defaults.headers.common["Authorization"] = `JWT ${
        action.payload.message.token
        }`;
      return {
        ...state,
        phase: SUCCESS,
        user: action.payload,
        error: null
      };
    case USER_LOGIN_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };
    case CLEAR_ERROR: {
      return {
        ...state,
        error: null
      };
    }
    case USER_ADD_ADDRESS:
      return {
        ...state,
        user: {
          ...state.user,
          message: {
            ...state.user.message,
            user: {
              ...state.user.message.user,
              roleProfile: {
                ...state.user.message.user.roleProfile,
                address: action.payload.length ? action.payload[0] : null
              }
            }
          }
        }
      }

    default:
      return state;
  }
};

/*Epics */

const loginUserEpic = action$ =>
  action$.pipe(
    ofType(USER_LOGIN),
    mergeMap(action =>
      api.loginUser(action.payload).pipe(
        map(response => loginUserSuccess(response)),
        catchError(error => {
          console.log(error);
          return of({
            type: USER_LOGIN_ERROR,
            error: error.message
          });
        })
      )
    )
  );

const refreshTokenEpic = (action$, state$) =>
  action$.pipe(
    ofType(REFRESH_TOKEN),
    mergeMap(action =>
      api.refreshToken(state$.value.authStore.user).pipe(
        map(response => loginUserSuccess(response)),
        catchError(error => {
          console.log(error);
          return of({
            type: USER_LOGIN_ERROR,
            error: error
          });
        })
      )
    )
  );

const loginUserSuccess = payload => ({ type: USER_LOGIN_SUCCESS, payload });

export { loginUserEpic, refreshTokenEpic };

export default authStore;
