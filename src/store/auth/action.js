import { USER_LOGIN, SET_INITIAL_AUTH, CLEAR_ERROR } from "./constants";
import api from './api';
const loginUser = payload => ({
  type: USER_LOGIN,
  payload
});

const changePassword = email => {
  return api.changePassword(email);
};

const getEmailFromToken = data => {
  return api.getEmailFromToken(data);
};

const setInitialAuth = () => ({
  type: SET_INITIAL_AUTH
});

const clearError = () => ({
  type: CLEAR_ERROR
});

export { loginUser, setInitialAuth, clearError, changePassword, getEmailFromToken};
