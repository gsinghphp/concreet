import { combineEpics } from "redux-observable";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import authStore, { loginUserEpic, refreshTokenEpic } from "./auth";
import agentRegisterStore, { registerAgentEpic, registerRenterEpic } from "./register";
import applicationStore, {
  createApplicationStore,
  createApplicationEpic,
  inviteLandlordEpic,
  getRenterResumeEpic,
  getRenterApplicationEpic,
  addSignatureEpic,
  getLandlordListingByEmailEpic,
  getRenterResumeByIDEpic,
  inviteRenterEpic,
} from "./application";
import idxListingStore,
{
  insertAgentListingStore,
  agentListingEpic,
  agentListingInsertEpic,
  agentIDXListingEpic,
} from "./agentListing";
import documentsStore,
{
  uploadDocEpic,
  uploadUserDocEpic,
  uploadListingDocEpic,
  getListingDocEpic,
  listingDocumentsStore,
  deleteDocEpic,
  getUserDocEpic,
  deleteUserDocEpic,
} from "./documents";
import addressStore, { getAddressEpic, saveAddressEpic, userAddAddressEpic } from "./address";
import bankAccountStore, { getBankAccountsEpic, saveAccountEpic } from './bankaccount';
import incomeStore, { getIncomesEpic, saveIncomeEpic, addIncomeDocEpic, deleteIncomeDocEpic } from './income';
import questionStore, { getQuestionsEpic, insertQuestionsEpic, updateAnswerEpic, updateAnswerAPIEpic } from './question';
import stripeAccountStore, { addStripeAccountEpic, getStripeAccountEpic, changeStatusStripeEpic } from './stripeaccounts';

import signatureStore, { getSignaturesEpic, addSignaturesEpic, deleteSignaturesEpic } from './signature';

const persistReducerConfig = {
  key: "user",
  storage
};

// const persistReducerConfigApplication = {
//   key: "agent_application",
//   storage
// };

export const rootEpic = combineEpics(
  loginUserEpic,
  registerAgentEpic,
  registerRenterEpic,
  agentListingEpic,
  agentListingInsertEpic,
  getListingDocEpic,
  uploadDocEpic,
  uploadUserDocEpic,
  uploadListingDocEpic,
  getAddressEpic,
  saveAddressEpic,
  refreshTokenEpic,
  userAddAddressEpic,
  agentIDXListingEpic,
  deleteDocEpic,
  deleteUserDocEpic,
  createApplicationEpic,
  getBankAccountsEpic,
  saveAccountEpic,
  getUserDocEpic,
  getIncomesEpic,
  saveIncomeEpic,
  addIncomeDocEpic,
  deleteIncomeDocEpic,
  getRenterResumeEpic,
  getRenterApplicationEpic,
  inviteLandlordEpic,
  addSignatureEpic,
  getLandlordListingByEmailEpic,
  getRenterResumeByIDEpic,
  getQuestionsEpic,
  insertQuestionsEpic,
  updateAnswerEpic,
  addStripeAccountEpic,
  getStripeAccountEpic,
  changeStatusStripeEpic,
  inviteRenterEpic,
  updateAnswerAPIEpic,
  getSignaturesEpic,
  addSignaturesEpic,
  deleteSignaturesEpic
);

export const rootReducer = combineReducers({
  authStore: persistReducer(persistReducerConfig, authStore),
  agentRegisterStore,
  applicationStore,
  createApplicationStore,
  idxListingStore,
  insertAgentListingStore,
  documentsStore,
  listingDocumentsStore,
  addressStore,
  bankAccountStore,
  incomeStore,
  questionStore,
  stripeAccountStore,
  signatureStore
});
