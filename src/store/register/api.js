import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
  agentRegister: user => {
    const request = axios
      .post(`${API_URL}auth/register/agent`, { user })
      .then(response => response);
    return fromPromise(request);
  },
  renterRegister: user => {
    const request = axios
      .post(`${API_URL}auth/register`, { user })
      .then(response => response);
    return fromPromise(request);
  },
};

export default api;
