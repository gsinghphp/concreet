import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";

import {
  AGENT_REGISTER,
  RENTER_REGISTER,
  AGENT_REGISTER_SUCCESS,
  AGENT_REGISTER_ERROR,
  CLEAR_AGENT_REGISTER
} from "./constants";

const initialState = {
  phase: INIT,
  error: null,
  registerdata: null
};

/*Reducer */
const agentRegisterStore = (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_AGENT_REGISTER:
    case AGENT_REGISTER:
      return initialState;
    case AGENT_REGISTER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        registerdata: action.payload,
        error: null
      };
    case AGENT_REGISTER_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };

    default:
      return state;
  }
};

/*Epics */

const registerAgentEpic = action$ =>
  action$.pipe(
    ofType(AGENT_REGISTER),
    mergeMap(action =>
      api.agentRegister(action.payload).pipe(
        map(payload => ({ type: AGENT_REGISTER_SUCCESS, payload })),
        catchError(error =>
          of({
            type: AGENT_REGISTER_ERROR,
            error
          })
        )
      )
    )
  );

const registerRenterEpic = action$ =>
  action$.pipe(
    ofType(RENTER_REGISTER),
    mergeMap(action =>
      api.renterRegister(action.payload).pipe(
        map(payload => ({ type: AGENT_REGISTER_SUCCESS, payload })),
        catchError(error =>
          of({
            type: AGENT_REGISTER_ERROR,
            error
          })
        )
      )
    )
  );

export { registerAgentEpic, registerRenterEpic };

export default agentRegisterStore;
