import { AGENT_REGISTER, RENTER_REGISTER, CLEAR_AGENT_REGISTER } from "./constants";

const registerAgent = payload => ({
  type: AGENT_REGISTER,
  payload
});

const registerRenter = payload => ({
  type: RENTER_REGISTER,
  payload
});

const setInitialAuth = () => ({
  type: CLEAR_AGENT_REGISTER
});

export { registerAgent, registerRenter, setInitialAuth };
