import axios from "axios";
import "rxjs/add/operator/map";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';
import { API_URL } from "../../utils/constants";

const api = {
  searchByEmail: email => {
    return axios
      .post(API_URL + "application/searchByEmail", { email, offset: 0, limit: 4 })
      .then(response => response);
  },
  autocompleteEmail: email => {
    return axios
      .post(API_URL + "renter/autocompleteEmail", { email })
      .then(response => response);
  },
  searchRenterByEmail: email => {
    return axios
      .get(`${API_URL}renter/email/${email}`)
      .then(response => response);
  },
  uploadDocument: (doc, type_id) => {
    const form = new FormData();
    form.append('type_id', type_id);
    form.append('file', doc);
    return axios
      .post(API_URL + "documents", form, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      })
      .then(response => response);
  },
  deleteDocument: document_id => {
    return axios
      .delete(API_URL + "documents", {
        data: { document_id }
      })
      .then(response => response);
  },
  searchLandlordByEmail: email => {
    return axios
      .get(`${API_URL}landlord/email/${email}`)
      .then(response => response);
  },
  addApplication: data => {
    delete data.landlord
    if (data.application.id) {
      const request = axios
        .put(API_URL + "application", data)
        .then(response => response);
      return fromPromise(request);
    } else {
      delete data.application.id
      const request = axios
        .post(API_URL + "application", data)
        .then(response => response);
      return fromPromise(request);
    }

  },
  getRenterResume: renter_role_id => {
    const request = axios
      .get(`${API_URL}renter/${renter_role_id}/resume`)
      .then(response => response);
    return fromPromise(request);
  },
  getRenterApplication: () => {
    const request = axios
      .get(`${API_URL}application?offset=0&limit=3`)
      .then(response => response);
    return fromPromise(request);
  },
  inviteLandlord: ({ application, landlord }) => {
    console.log(application)
    const request = axios
      .post(`${API_URL}agent/listings/landlord/invite`, {
        "landlord": {
          email: landlord 
        },
        "listing": {
          "id": application.agent_listing_id
        }
      })
      .then(response => response);
    return fromPromise(request);
  },
  inviterenter: ({ renter, listing }) => {
    console.log(listing)
    const request = axios
      .post(`${API_URL}agent/listings/renter/invite`, {
        renter: {
          email : renter
        },
        listing
      })
      .then(response => response);
    return fromPromise(request);
  },
  addSignature: ({ applicationId, listing_documents }) => {
    const request = axios
      .post(`${API_URL}application/document_signature`, {
        "application": {
          "id": applicationId
        },
        listing_documents
      })
      .then(response => response);
    return fromPromise(request);
  },
  getLandlordByEmail: id => {
    const request = axios
      .get(`${API_URL}landlord/listings/${id}`)
      .then(response => response);
    return fromPromise(request);
  },
  confirmApplication: id => {
    return axios
      .post(`${API_URL}application/confirm`, {
        "application": {
          id
        }
      })
      .then(response => response);
  },
  buyApplication: id => {
    return axios
      .post(`${API_URL}application/buy`, {
        "application": {
          id
        }
      })
      .then(response => response);
  }
};

export default api;
