import api from "./api";
import {
  PUT_APPLICATION_VALUES,
  CREATE_APPLICATION,
  CLEAR_CREATE_APPLICATION,
  API_APPLICATION_VALUES,
  SET_INITIAL_APPLICATION,
  GET_RENTER_RESUME,
  GET_RENTER_APPLICATION,
  INVITE_LANDLORD,
  INVITE_RENTER,
  ADD_SIGNATRE,
  GET_LANDLORD_LISTING_EMAIL
} from './constants';

const clearApplicationForm = () => ({
  type: SET_INITIAL_APPLICATION
});

const clearCreateApplication = () => ({
  type: CLEAR_CREATE_APPLICATION
})

const putAppValues = payload => ({
  type: PUT_APPLICATION_VALUES,
  payload
})

const putApiValues = payload => ({
  type: API_APPLICATION_VALUES,
  payload
})

const addApplication = payload => ({
  type: CREATE_APPLICATION,
  payload
})

const inviteLandlord = payload => ({
  type: INVITE_LANDLORD,
  payload
})

const inviteRenter = payload => ({
  type: INVITE_RENTER,
  payload
})

const addSignature = payload => ({
  type: ADD_SIGNATRE,
  payload
})

const getRenterResume = () => ({
  type: GET_RENTER_RESUME,
})

const getRenterApplication = () => ({
  type: GET_RENTER_APPLICATION,
})

const getLandlordListing = payload => ({
  type: GET_LANDLORD_LISTING_EMAIL,
  payload
})

const searchByEmail = email => {
  return api.searchByEmail(email);
};

const autocompleteEmail = email => {
  return api.autocompleteEmail(email);
};

const uploadDocument = (doc, type) => {
  return api.uploadDocument(doc, type);
};

const deleteDocument = id => {
  return api.deleteDocument(id);
};

const searchLandlordByEmail = email => {
  return api.searchLandlordByEmail(email);
};

const searchRenterByEmail = email => {
  return api.searchRenterByEmail(email);
};

const confirmApplication = id => {
  return api.confirmApplication(id);
};

const buyApplication = id => {
  return api.buyApplication(id);
};

export {
  putAppValues,
  putApiValues,
  searchByEmail,
  uploadDocument,
  deleteDocument,
  searchLandlordByEmail,
  searchRenterByEmail,
  addApplication,
  clearApplicationForm,
  getRenterResume,
  getRenterApplication,
  inviteLandlord,
  inviteRenter,
  addSignature,
  getLandlordListing,
  clearCreateApplication,
  confirmApplication,
  buyApplication,
  autocompleteEmail,
};
