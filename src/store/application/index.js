import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import { INIT, SUCCESS, ERROR, LOADING } from "../../utils/constants";
import {
  SET_INITIAL_APPLICATION,
  CREATE_APPLICATION,
  API_APPLICATION_VALUES,
  CREATE_APPLICATION_SUCCESS,
  CREATE_APPLICATION_ERROR,
  PUT_APPLICATION_VALUES,
  GET_RENTER_RESUME,
  GET_RENTER_RESUME_BY_ID,
  GET_RENTER_RESUME_SUCCESS,
  GET_RENTER_RESUME_ERROR,
  GET_RENTER_APPLICATION,
  GET_RENTER_APPLICATION_SUCCESS,
  INVITE_LANDLORD,
  ADD_SIGNATRE,
  PHASE_SUCCESS,
  GET_LANDLORD_LISTING_EMAIL,
  CLEAR_CREATE_APPLICATION,
  INVITE_RENTER,
  INVITE_RENTER_SUCCESS,
} from "./constants";

import { getDocumentSignatures } from '../signature/action';

import api from './api';

const initialState = {
  phase: INIT,
  formValues: null,
  error: null,
  values: null,
  application: [],
  resume: {},
  renterApplication: []
};

const applicationStore = (state = initialState, action) => {
  switch (action.type) {
    case SET_INITIAL_APPLICATION:
      return initialState;
    case PUT_APPLICATION_VALUES:
      return {
        ...state,
        formValues: action.payload
      };
    case API_APPLICATION_VALUES:
      return {
        ...state,
        values: action.payload
      };
    case GET_RENTER_RESUME_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        resume: action.payload.message
      };
    case GET_RENTER_APPLICATION_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        renterApplication: action.payload.message
      };
    case GET_RENTER_RESUME_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };
    case ADD_SIGNATRE:
      return {
        ...state,
        phase: INIT
      }
    case PHASE_SUCCESS:
      return {
        ...state,
        phase: SUCCESS
      }
    default:
      return state;
  }
};

const getRenterResumeEpic = (action$, state$) =>
  action$.pipe(
    ofType(GET_RENTER_RESUME),
    mergeMap(action =>
      api.getRenterResume(state$.value.authStore.user.message.user.roleProfile.id).pipe(
        mergeMap(payload => of(
          renterSuccess(payload),
          getRenterApplicationAction()
        )),
        catchError(error =>
          of({
            type: GET_RENTER_RESUME_ERROR,
            error
          })
        )
      )
    ))

const renterSuccess = payload => ({ type: GET_RENTER_RESUME_SUCCESS, payload })
const getRenterApplicationAction = () => ({ type: GET_RENTER_APPLICATION })
const renterApplicationSuccess = payload => ({ type: GET_RENTER_APPLICATION_SUCCESS, payload });

const getRenterApplicationEpic = action$ =>
  action$.pipe(
    ofType(GET_RENTER_APPLICATION),
    mergeMap(action =>
      api.getRenterApplication().pipe(
        mergeMap((payload) => of(
          renterApplicationSuccess(payload),
          getDocumentSignatures(payload.message[0].id)
        )),
        catchError(error =>
          of({
            type: GET_RENTER_RESUME_ERROR,
            error
          })
        )
      )
    ))

const insertApplicationinitialState = {
  phase: INIT,
  application: {},
  error: null
};


const createApplicationStore = (state = insertApplicationinitialState, action) => {
  switch (action.type) {
    case CREATE_APPLICATION:
      return {
        ...state,
        phase: LOADING
      };
    case CREATE_APPLICATION_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        error: null
      };
    case CREATE_APPLICATION_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.error
      };
    case CLEAR_CREATE_APPLICATION:
      return {
        ...state,
        phase: INIT
      }
    case INVITE_LANDLORD:
      return {
        ...state,
        phase: LOADING
      }

    default:
      return state;
  }
};

const createApplicationEpic = action$ =>
  action$.pipe(
    ofType(CREATE_APPLICATION),
    mergeMap(action =>
      api.addApplication(action.payload).pipe(
        map(payload => ({ type: CREATE_APPLICATION_SUCCESS, payload })),
        catchError(error =>
          of({
            type: CREATE_APPLICATION_ERROR,
            error
          })
        )
      )
    )
  );

const inviteLandlordEpic = (action$, state$) =>
  action$.pipe(
    ofType(INVITE_LANDLORD),
    mergeMap(action =>
      api.inviteLandlord(action.payload).pipe(
        mergeMap(() => api.inviterenter({
          renter: state$.value.applicationStore.formValues.email,
          listing: {
            "id": action.payload.application.agent_listing_id,
            "listing_id": state$.value.applicationStore.formValues.listingAddress.listing_id
          }
        }).pipe(map(({ data }) => ({
          type: CREATE_APPLICATION, payload: {
            ...action.payload,
            renter_id: data.renter.renter_id
          }
        })))
        ),
        catchError(error =>
          of({
            type: CREATE_APPLICATION_ERROR,
            error
          })
        )
      )
    )
  );

const inviteRenterEpic = action$ =>
  action$.pipe(
    ofType(INVITE_RENTER),
    mergeMap(action =>
      api.inviterenter(action.payload).pipe(
        map(payload => ({ type: INVITE_RENTER_SUCCESS })),
        catchError(error =>
          of({
            type: CREATE_APPLICATION_ERROR,
            error
          })
        )
      )
    )
  );

const addSignatureEpic = action$ =>
  action$.pipe(
    ofType(ADD_SIGNATRE),
    mergeMap(action =>
      api.addSignature(action.payload).pipe(
        map(payload => ({ type: PHASE_SUCCESS })),
        catchError(error =>
          of({
            type: CREATE_APPLICATION_ERROR,
            error
          })
        )
      )
    )
  );

const getLandlordListingByEmailEpic = action$ =>
  action$.pipe(
    ofType(GET_LANDLORD_LISTING_EMAIL),
    mergeMap(action =>
      api.getLandlordByEmail(action.payload).pipe(
        mergeMap(payload => of(
          getApplicationSuccess(payload),
          getRenterResumeID(payload.message[0].renter_id),
          getDocumentSignatures(payload.message[0].id)
        )),
        catchError(error =>
          of({
            type: CREATE_APPLICATION_ERROR,
            error
          })
        )
      )
    )
  );

const getApplicationSuccess = payload => ({ type: GET_RENTER_APPLICATION_SUCCESS, payload });
const getRenterResumeID = payload => ({ type: GET_RENTER_RESUME_BY_ID, payload })

const getRenterResumeByIDEpic = action$ =>
  action$.pipe(
    ofType(GET_RENTER_RESUME_BY_ID),
    mergeMap(action =>
      api.getRenterResume(action.payload).pipe(
        mergeMap(payload => of(renterSuccess(payload))),
        catchError(error =>
          of({
            type: GET_RENTER_RESUME_ERROR,
            error
          })
        )
      )
    ))



export {
  createApplicationStore,
  createApplicationEpic,
  getRenterResumeEpic,
  getRenterApplicationEpic,
  inviteLandlordEpic,
  addSignatureEpic,
  getLandlordListingByEmailEpic,
  getRenterResumeByIDEpic,
  inviteRenterEpic,
}


export default applicationStore;