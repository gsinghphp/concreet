import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
    getAddress: () => {
        const request = axios
            .get(`${API_URL}users/address/`)
            .then(response => response);
        return fromPromise(request);
    },
    saveAddress: (data, id) => {
        if (data.id) {
            const request = axios
                .put(`${API_URL}users/address/`, { user_id: id, address: data })
                .then(response => response);
            return fromPromise(request);
        } else {
            const request = axios
                .post(`${API_URL}users/address/`, { address: data })
                .then(response => response);
            return fromPromise(request);
        }

    },
};

export default api;
