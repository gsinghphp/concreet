import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";
import {
    GET_ADDRESS,
    GET_ADDRESS_SUCCESS,
    GET_ADDRESS_ERROR,
    SAVE_ADDRESS,
    SAVE_ADDRESS_SUCCESS,
    INIT_ADDRESS,
} from "./constants";

import { USER_ADD_ADDRESS } from '../auth/constants';

const initialState = {
    phase: INIT,
    addresses: [],
    error: null
};

const addressStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_ADDRESS:
            return initialState;
        case GET_ADDRESS_SUCCESS:
            return {
                ...state,
                addresses: action.payload,
                phase: SUCCESS,
                error: null
            };
        case GET_ADDRESS_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };
        case SAVE_ADDRESS_SUCCESS:
            return {
                ...state,
            };
        case INIT_ADDRESS:
            return {
                ...state,
                phase: INIT
            }

        default:
            return state;
    }
};

const getAddressEpic = action$ =>
    action$.pipe(
        ofType(GET_ADDRESS),
        mergeMap(action =>
            api.getAddress().pipe(
                map(({ message }) => message),
                map(addresses => addresses.map((address, index) => {
                    address['change'] = false;
                    return address
                })),
                map(payload => ({ type: GET_ADDRESS_SUCCESS, payload })),
                catchError(error =>
                    of({
                        type: GET_ADDRESS_ERROR,
                        error
                    })
                )
            )
        )
    );

const saveAddressEpic = (action$, state$) =>
    action$.pipe(
        ofType(SAVE_ADDRESS),
        mergeMap(({ payload }) =>
            api.saveAddress(payload, state$.value.authStore.user.message.user.id).pipe(
                map(() => ({ type: SAVE_ADDRESS_SUCCESS })),
                catchError(error =>
                    of({
                        type: GET_ADDRESS_ERROR,
                        error
                    })
                )
            )
        )
    );

const userAddAddressEpic = (action$, state$) =>
    action$.pipe(
        ofType(GET_ADDRESS_SUCCESS),
        map(({ payload }) => ({ type: USER_ADD_ADDRESS, payload }))
    );

export { getAddressEpic, saveAddressEpic, userAddAddressEpic };

export default addressStore;
