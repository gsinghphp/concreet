import { GET_ADDRESS, SAVE_ADDRESS, INIT_ADDRESS } from './constants';


const getAddresses = () => ({
    type: GET_ADDRESS,
})

const saveAddress = payload => ({
    type: SAVE_ADDRESS,
    payload
})

const initAddress = () => ({
    type: INIT_ADDRESS
})

export { getAddresses, saveAddress, initAddress };
