import { GET_QUESTION, INSERT_QUESTIONS, UPDATE_QUESTION } from './constants';


const getQuestions = () => ({
    type: GET_QUESTION,
})

const insertQuestions = payload => ({
    type: INSERT_QUESTIONS,
    payload
})

const updateQuestion = payload => ({
    type: UPDATE_QUESTION,
    payload
})

export { getQuestions, insertQuestions, updateQuestion };
