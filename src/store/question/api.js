import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

import { API_URL } from "../../utils/constants";

const api = {
    getQuestions: () => {
        const request = axios
            .get(`${API_URL}users/background_question/`)
            .then(response => response);
        return fromPromise(request);
    },
    insertQuestions: answers => {
        const request = axios
            .post(`${API_URL}users/background_question/`, {
                answers
            })
            .then(response => response);
        return fromPromise(request);
    },
    updateQuestion: (question) => {
        const request = axios
            .put(`${API_URL}users/background_question/`, {
                "id": question.id,
                "question_id": question.question_id,
                "answer": question.answer
            })
            .then(response => response);
        return fromPromise(request);
    }
};

export default api;
