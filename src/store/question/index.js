import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";

import { INIT, SUCCESS, ERROR } from "../../utils/constants";
import {
    GET_QUESTION,
    GET_QUESTION_SUCCESS,
    GET_QUESTION_ERROR,
    INSERT_QUESTIONS,
    UPDATE_QUESTION,
    UPDATE_QUESTION_API
} from "./constants";

const initialState = {
    phase: INIT,
    questions: [],
    error: null
};

const questionStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_QUESTION:
            return initialState;
        case GET_QUESTION_SUCCESS:
            return {
                ...state,
                questions: action.payload,
                phase: SUCCESS,
                error: null
            };
        case GET_QUESTION_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };

        default:
            return state;
    }
};

const getQuestionsEpic = action$ =>
    action$.pipe(
        ofType(GET_QUESTION),
        mergeMap(action =>
            api.getQuestions().pipe(
                map(payload => ({ type: GET_QUESTION_SUCCESS, payload: payload.message })),
                catchError(error =>
                    of({
                        type: GET_QUESTION_ERROR,
                        error
                    })
                )
            )
        )
    );

const insertQuestionsEpic = action$ =>
    action$.pipe(
        ofType(INSERT_QUESTIONS),
        mergeMap(action =>
            api.insertQuestions(action.payload).pipe(
                map(payload => ({ type: GET_QUESTION, payload })),
                catchError(error =>
                    of({
                        type: GET_QUESTION_ERROR,
                        error
                    })
                )
            )
        )
    );

const updateAnswerEpic = (action$, state$) =>
    action$.pipe(
        ofType(UPDATE_QUESTION),
        mergeMap(action =>
            of(
                updateQuestion(action.payload, state$.value.questionStore.questions),
                updateQuestionApi(action.payload)
            )

        )
    );

const updateQuestion = (questionupdate, questions) => {
    const findQuestionIndex = questions.findIndex(question => question.id === questionupdate.id);
    questions[findQuestionIndex].answer = questionupdate.answer;
    return ({ type: GET_QUESTION_SUCCESS, payload: questions })
}

const updateAnswerAPIEpic = (action$) =>
    action$.pipe(
        ofType(UPDATE_QUESTION_API),
        mergeMap(action => api.updateQuestion(action.payload).pipe(
            map(payload => {
                return ({ type: 'UPDATE_QUESTION_SUCCESS', payload })
            }),
            catchError(error =>
                of({
                    type: GET_QUESTION_ERROR,
                    error
                })
            )
        )
        )
    );

const updateQuestionApi = (payload) => ({
    type: UPDATE_QUESTION_API,
    payload
})

export {
    getQuestionsEpic,
    insertQuestionsEpic,
    updateAnswerEpic,
    updateAnswerAPIEpic
}

export default questionStore;
