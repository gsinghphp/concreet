import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR, LOADING } from "../../utils/constants";
import {
    GET_STRIPE_ACCOUNTS,
    GET_STRIPE_ACCOUNTS_SUCCESS,
    GET_STRIPE_ACCOUNTS_ERROR,
    SAVE_STRIPE_ACCOUNT,
    SAVE_STRIPE_ACCOUNT_SUCCESS,
    CHANGE_STATUS,
    INIT_ACCOUNT
} from "./constants";

const initialState = {
    phase: INIT,
    accounts: [],
    error: null,
    addPhase: INIT
};

const stripeAccountStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_STRIPE_ACCOUNTS:
            return {
                ...state,
                phase: LOADING,
                error: null
            };
        case GET_STRIPE_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accounts: action.payload,
                phase: SUCCESS,
                error: null
            };
        case GET_STRIPE_ACCOUNTS_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error,
                addPhase: ERROR
            };
        case SAVE_STRIPE_ACCOUNT:
            return {
                ...state,
                addPhase: LOADING
            };
        case SAVE_STRIPE_ACCOUNT_SUCCESS:
            return {
                ...state,
                addPhase: SUCCESS
            };
        case INIT_ACCOUNT:
            return {
                ...state,
                addPhase: INIT
            };

        default:
            return state;
    }
};

const getStripeAccountEpic = action$ => action$.pipe(
    ofType(GET_STRIPE_ACCOUNTS),
    mergeMap(action =>
        api.getStripeAccounts().pipe(
            map(payload => ({ type: GET_STRIPE_ACCOUNTS_SUCCESS, payload: payload.message })),
            catchError(error => {
                return of({
                    type: GET_STRIPE_ACCOUNTS_ERROR,
                    error: error.errors
                });
            })
        )
    )
);

const addStripeAccountEpic = action$ => action$.pipe(
    ofType(SAVE_STRIPE_ACCOUNT),
    mergeMap(action =>
        api.addStripeAccount(action.payload).pipe(
            mergeMap(() => of(
                successAdd(),
                getStripeAccounts()
            )),
            catchError(error => {
                return of({
                    type: GET_STRIPE_ACCOUNTS_ERROR,
                    error: error.errors
                });
            })
        )
    )
);

const successAdd = () => ({ type: SAVE_STRIPE_ACCOUNT_SUCCESS })
const getStripeAccounts = () => ({ type: GET_STRIPE_ACCOUNTS })

const changeStatusStripeEpic = (action$, state$) => action$.pipe(
    ofType(CHANGE_STATUS),
    mergeMap(action =>
        api.changeStatus(action.payload).pipe(
            map(() => {
                const { accounts } = state$.value.stripeAccountStore;
                return accounts.map(account => {
                    if (account.id === action.payload) {
                        account.status = true;
                    } else {
                        account.status = false;
                    }
                    return account
                })
            }),
            map((payload) => ({ type: GET_STRIPE_ACCOUNTS_SUCCESS, payload })),
            catchError(error => {
                return of({
                    type: GET_STRIPE_ACCOUNTS_ERROR,
                    error: error.errors
                });
            })
        )
    )
);

export {
    addStripeAccountEpic,
    getStripeAccountEpic,
    changeStatusStripeEpic,
}

export default stripeAccountStore;
