import { SAVE_STRIPE_ACCOUNT, GET_STRIPE_ACCOUNTS, CHANGE_STATUS, INIT_ACCOUNT } from './constants';

const addStripeAccount = payload => ({
    type: SAVE_STRIPE_ACCOUNT,
    payload
})

const getStripeAccounts = payload => ({
    type: GET_STRIPE_ACCOUNTS,
    payload
})

const changeStatus = payload => ({
    type: CHANGE_STATUS,
    payload
})

const initStripe = () => ({
    type: INIT_ACCOUNT
})

export { addStripeAccount, getStripeAccounts, changeStatus, initStripe };
