import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

import { API_URL } from "../../utils/constants";

const api = {
    addStripeAccount: payload => {
        const request = axios
            .post(`${API_URL}payment/stripe/add_account/`, payload)
            .then(response => response);
        return fromPromise(request);
    },
    getStripeAccounts: () => {
        const request = axios
            .get(`${API_URL}payment/stripe/all_account/`)
            .then(response => response);
        return fromPromise(request);
    },
    changeStatus: id => {
        const request = axios
            .post(`${API_URL}payment/stripe/active_account/`, {
                id,
                status: true
            })
            .then(response => response);
        return fromPromise(request);
    }
};

export default api;
