import { GET_BANK_ACCOUNT, SAVE_BANK_ACCOUNT, INIT_BANK_ACCOUNT } from './constants';


const getBankAccounts = () => ({
    type: GET_BANK_ACCOUNT,
})

const saveAccount = payload => ({
    type: SAVE_BANK_ACCOUNT,
    payload
})

const setInitAccount = () => ({
    type: INIT_BANK_ACCOUNT
})

export { getBankAccounts, saveAccount, setInitAccount };
