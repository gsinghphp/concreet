import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
    getBankAccount: () => {
        const request = axios
            .get(`${API_URL}users/bank-account/`)
            .then(response => response);
        return fromPromise(request);
    },
    saveBankAccount: (data) => {
        if (data.id) {
            const request = axios
                .put(`${API_URL}users/bank-account/`, { bank_account: data })
                .then(response => response);
            return fromPromise(request);
        } else {
            const request = axios
                .post(`${API_URL}users/bank-account/`, { bank_account: data })
                .then(response => response);
            return fromPromise(request);
        }
    },
};

export default api;
