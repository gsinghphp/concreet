import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR } from "../../utils/constants";
import {
    GET_BANK_ACCOUNT,
    GET_BANK_ACCOUNT_SUCCESS,
    GET_BANK_ACCOUNT_ERROR,
    SAVE_BANK_ACCOUNT,
    SAVE_BANK_ACCOUNT_SUCCESS,
    INIT_BANK_ACCOUNT,
} from "./constants";

const initialState = {
    phase: INIT,
    accounts: [],
    error: null
};

const bankAccountStore = (state = initialState, action) => {
    switch (action.type) {
        case GET_BANK_ACCOUNT:
            return initialState;
        case GET_BANK_ACCOUNT_SUCCESS:
            return {
                ...state,
                accounts: action.payload,
                phase: SUCCESS,
                error: null
            };
        case GET_BANK_ACCOUNT_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };
        case SAVE_BANK_ACCOUNT:
            return {
                ...state,
            };
        case INIT_BANK_ACCOUNT:
            return {
                ...state,
                phase: INIT
            }

        default:
            return state;
    }
};

const getBankAccountsEpic = action$ =>
    action$.pipe(
        ofType(GET_BANK_ACCOUNT),
        mergeMap(action =>
            api.getBankAccount().pipe(
                map(({ message }) => message),
                map(accounts => accounts.map((account, index) => {
                    account['change'] = false;
                    return account
                })),
                map(payload => ({ type: GET_BANK_ACCOUNT_SUCCESS, payload })),
                catchError(error =>
                    of({
                        type: GET_BANK_ACCOUNT_ERROR,
                        error
                    })
                )
            )
        )
    );

const saveAccountEpic = (action$, state$) =>
    action$.pipe(
        ofType(SAVE_BANK_ACCOUNT),
        mergeMap(({ payload }) =>
            api.saveBankAccount(payload).pipe(
                map(() => ({ type: SAVE_BANK_ACCOUNT_SUCCESS })),
                catchError(error =>
                    of({
                        type: GET_BANK_ACCOUNT_ERROR,
                        error
                    })
                )
            )
        )
    );

export {
    getBankAccountsEpic,
    saveAccountEpic
}

export default bankAccountStore;
