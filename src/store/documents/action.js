import {
    UPLOAD_DOCUMENT,
    GET_AGENT_LISTING_DOCUMENTS,
    DELETE_DOCUMENT,
    GET_USER_DOCUMENTS,
    DELETE_USER_DOCUMENT,
    UPLOAD_USER_DOCUMENT
} from "./constants";

const uploadUserDoc = payload => ({
    type: UPLOAD_DOCUMENT,
    payload,
    docType: 'user'
});

const uploadListingDoc = (payload, listingId) => ({
    type: UPLOAD_DOCUMENT,
    payload,
    docType: 'listing',
    listingId
});

const addUserDoc = payload => ({
    type: UPLOAD_USER_DOCUMENT,
    payload
})

const getListingDoc = payload => ({
    type: GET_AGENT_LISTING_DOCUMENTS,
    payload,
});

const getUserDocs = payload => ({
    type: GET_USER_DOCUMENTS,
    payload,
});

const deleteDocument = payload => ({
    type: DELETE_DOCUMENT,
    payload,
});

const deleteUserDocument = payload => ({
    type: DELETE_USER_DOCUMENT,
    payload,
});

export { addUserDoc, uploadUserDoc, uploadListingDoc, getListingDoc, deleteDocument, getUserDocs, deleteUserDocument };
