import axios from "axios";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/operator/map";
import { API_URL } from "../../utils/constants";
import { fromPromise } from 'rxjs/internal/observable/fromPromise';

const api = {
    uploadDocument: ({ doc, type }) => {
        const form = new FormData();
        form.append('type_id', type);
        form.append('file', doc);
        const request = axios
            .post(API_URL + "documents", form, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
            .then(response => response);
        return fromPromise(request);
    },
    uploadUserDocument: data => {
        const request = axios
            .post(API_URL + "users/documents", data)
            .then(response => response);
        return fromPromise(request);
    },
    uploadListingDocument: data => {
        const request = axios
            .post(API_URL + "listings/document/", data)
            .then(response => response);
        return fromPromise(request);
    },
    getListingDocuments: listingId => {
        const request = axios
            .get(`${API_URL}listings/${listingId}/document/`)
            .then(response => response);
        return fromPromise(request);
    },
    getUserDocuments: () => {
        const request = axios
            .get(`${API_URL}users/documents/`)
            .then(response => response);
        return fromPromise(request);
    },
    deleteDocument: ({ documentId, listingID }) => {
        const request = axios
            .delete(API_URL + "listings/document/", {
                data: {
                    "listing": {
                        "id": listingID,
                        "document": {
                            "id": documentId
                        }
                    }
                }
            })
            .then(response => response);
        return fromPromise(request);
    },
    deleteUserDocument: (document_id) => {
        const request = axios
            .delete(API_URL + "users/documents", {
                data:
                {
                    "document": {
                        "id": document_id
                    }
                }

            })
            .then(response => response);
        return fromPromise(request);
    },
};

export default api;
