import { mergeMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { ofType } from "redux-observable";
import api from "./api";
import { INIT, SUCCESS, ERROR, LOADING } from "../../utils/constants";

import {
    UPLOAD_DOCUMENT,
    UPLOAD_DOCUMENT_LISTING,
    UPLOAD_DOCUMENT_SUCCESS,
    UPLOAD_DOCUMENT_ERROR,
    UPLOAD_USER_DOCUMENT,
    DELETE_DOCUMENT,
    DELETE_USER_DOCUMENT,
    GET_USER_DOCUMENTS,
    GET_USER_DOCUMENTS_SUCCESS,
    GET_AGENT_LISTING_DOCUMENTS,
    GET_AGENT_LISTING_DOCUMENTS_SUCCESS,
    GET_AGENT_LISTING_DOCUMENTS_ERROR,
    ADD_INCOME_DOCUMENT,
} from "./constants";

const initialState = {
    phase: INIT,
    documents: [],
    error: null
};

/*Reducer */
const documentsStore = (state = initialState, action) => {
    switch (action.type) {
        case UPLOAD_DOCUMENT:
            return {
                ...state,
                phase: LOADING,
            }
        case UPLOAD_DOCUMENT_SUCCESS:
            return {
                ...state,
                phase: SUCCESS,
                user: action.payload,
                error: null
            };
        case UPLOAD_DOCUMENT_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };

        default:
            return state;
    }
};

/*Epics */
/** Upload User Documents */
const uploadDocEpic = action$ =>
    action$.pipe(
        ofType(UPLOAD_DOCUMENT),
        mergeMap(action =>
            api.uploadDocument(action.payload).pipe(
                map(response => {
                    const payload = {
                        document: {
                            id: response.data.id,
                            type_id: action.payload.type
                        }
                    }
                    if (action.docType === 'listing') {
                        payload['listing'] = {
                            id: action.listingId,
                            document: payload.document
                        };
                        delete payload.document;
                        return ({ type: UPLOAD_DOCUMENT_LISTING, payload })
                    }
                    if (action.docType === 'income') {
                        payload['income'] = {
                            id: action.incomeId,
                        };
                        return ({ type: ADD_INCOME_DOCUMENT, payload })
                    }
                    return ({ type: UPLOAD_USER_DOCUMENT, payload })
                }),
                catchError(error => {
                    return of({
                        type: UPLOAD_DOCUMENT_ERROR,
                        error: error.errors
                    });
                })
            )
        )
    );

const uploadUserDocEpic = action$ =>
    action$.pipe(
        ofType(UPLOAD_USER_DOCUMENT),
        mergeMap(action =>
            api.uploadUserDocument(action.payload).pipe(
                mergeMap(payload => of(
                    getUserDocuments(),
                    uploadSuccess(payload)
                )),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: UPLOAD_DOCUMENT_ERROR,
                        error: error.errors
                    });
                })
            )
        )
    );
const getUserDocuments = () => ({ type: GET_USER_DOCUMENTS });

/** Upload ListingDocs */
const uploadListingDocEpic = action$ =>
    action$.pipe(
        ofType(UPLOAD_DOCUMENT_LISTING),
        mergeMap(action =>
            api.uploadListingDocument(action.payload).pipe(
                mergeMap(payload => of(
                    getDocuments(action.payload.listing.id),
                    uploadSuccess(payload),
                )),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: UPLOAD_DOCUMENT_ERROR,
                        error: error.errors
                    });
                })
            )
        )
    );
const uploadSuccess = payload => ({ type: UPLOAD_DOCUMENT_SUCCESS, payload });
const getDocuments = payload => ({ type: GET_AGENT_LISTING_DOCUMENTS, payload });
/**************************Agent Listing Document************************************ */

const listingDocumentsinitialState = {
    phase: INIT,
    documents: [],
    userDocuments: {},
    error: null
};

/*Reducer */
const listingDocumentsStore = (state = listingDocumentsinitialState, action) => {
    switch (action.type) {
        case GET_AGENT_LISTING_DOCUMENTS:
            return {
                ...state,
                phase: LOADING,
            }
        case GET_AGENT_LISTING_DOCUMENTS_SUCCESS:
            return {
                ...state,
                phase: SUCCESS,
                documents: action.payload.message,
                error: null
            };
        case GET_AGENT_LISTING_DOCUMENTS_ERROR:
            return {
                ...state,
                phase: ERROR,
                error: action.error
            };
        case GET_USER_DOCUMENTS_SUCCESS:
            return {
                ...state,
                phase: SUCCESS,
                userDocuments: action.payload
            }

        default:
            return state;
    }
};


const getListingDocEpic = action$ =>
    action$.pipe(
        ofType(GET_AGENT_LISTING_DOCUMENTS),
        mergeMap(action =>
            api.getListingDocuments(action.payload).pipe(
                map(payload => ({ type: GET_AGENT_LISTING_DOCUMENTS_SUCCESS, payload })),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: GET_AGENT_LISTING_DOCUMENTS_ERROR,
                        error: error.errors
                    });
                })
            )
        )
    );

const getUserDocEpic = action$ =>
    action$.pipe(
        ofType(GET_USER_DOCUMENTS),
        mergeMap(action =>
            api.getUserDocuments().pipe(
                map(({ message }) => message.reduce((acc, doc) => {
                    acc[doc.document_type_id] = doc;
                    return acc
                }, {})),
                map(payload => ({ type: GET_USER_DOCUMENTS_SUCCESS, payload })),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: GET_AGENT_LISTING_DOCUMENTS_ERROR,
                        error: error.errors
                    });
                })
            )
        )
    );

const deleteDocEpic = action$ =>
    action$.pipe(
        ofType(DELETE_DOCUMENT),
        mergeMap(action =>
            api.deleteDocument(action.payload).pipe(
                map(payload => ({ type: GET_AGENT_LISTING_DOCUMENTS, payload: action.payload.listingID })),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: GET_AGENT_LISTING_DOCUMENTS_ERROR,
                        error
                    });
                })
            )
        )
    );

const deleteUserDocEpic = action$ =>
    action$.pipe(
        ofType(DELETE_USER_DOCUMENT),
        mergeMap(action =>
            api.deleteUserDocument(action.payload).pipe(
                map(payload => ({ type: GET_USER_DOCUMENTS })),
                catchError(error => {
                    console.log(error);
                    return of({
                        type: GET_AGENT_LISTING_DOCUMENTS_ERROR,
                        error
                    });
                })
            )
        )
    );


export {
    uploadDocEpic,
    uploadUserDocEpic,
    uploadListingDocEpic,
    getListingDocEpic,
    listingDocumentsStore,
    deleteDocEpic,
    getUserDocEpic,
    deleteUserDocEpic,
};

export default documentsStore;
