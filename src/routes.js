import React from "react";
import { Route } from "react-router-dom";
import Header from "./components/Header";
import Landing from "./container/Landing";
import Login from "./container/Login";
import AgentRegister from "./container/AgentRegister";
import RenterRegister from "./container/RenterRegister";
import VerifyEmail from "./container/VerifyEmail";
import ForgotPassword from './container/ForgotPassword';
import ChangePassword from "./container/ChangePassword";
import EditProfile from "./container/EditProfile";

import AddBankAccount from "./container/BankAccount/Add";

import RenterMain from './container/Renter/Main';
import Lease from './container/Renter/Lease';
import Confirmation from './container/Renter/Confirmation';

import ViewApplication from './container/Landlord/ViewApplication';
import MyListings from './container/Landlord/MyListings';

import RequireAuth from "./utils/requireAuth";
import LandlordRegister from "./container/LandlordRegister";

const CreateApplication = React.lazy(() => import("./container/Agent/CreateApplication"));

function WaitingComponent(Component) {
  return props => (
    <React.Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </React.Suspense>
  );
}


const Routes = () => (
  <div className="wrapper-full">
    <Header />
    <Route path="/" exact component={Landing} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={AgentRegister} />
    <Route path="/renter-register/:encryptedData" component={RenterRegister} />
    <Route path="/landlord-register/:encryptedData" component={LandlordRegister} />
    <Route path="/forgot-password" component={ForgotPassword} />
    <Route path="/verify-email/:hash" component={VerifyEmail} />
    <Route path="/change-password/:hash" component={ChangePassword} />
    <Route
      path="/create-application"
      component={WaitingComponent(RequireAuth(CreateApplication))}
    />
    <Route
      path="/add-bank-account"
      component={WaitingComponent(RequireAuth(AddBankAccount))}
    />
    <Route
      path="/renter"
      component={WaitingComponent(RequireAuth(RenterMain))}
    />
    <Route
      path="/lease"
      component={WaitingComponent(RequireAuth(Lease))}
    />
    <Route
      path="/edit-profile"
      component={WaitingComponent(RequireAuth(EditProfile))}
    />
    <Route
      path="/confirmation"
      component={WaitingComponent(RequireAuth(Confirmation))}
    />
    <Route
      path="/view-application/:id"
      component={WaitingComponent(RequireAuth(ViewApplication))}
    />
    <Route
      path="/my-listings"
      component={WaitingComponent(RequireAuth(MyListings))} />

  </div>
);


export default Routes;
