import validator from "validator";

const regExPassword =
  "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";

const regExDob =
  /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

const checkEmail = value => {
  if (!validator.isEmail(value)) {
    return `Please enter a valid email`;
  }
  return true;
};

const checkPassword = value => {
  if (!validator.matches(value, regExPassword)) {
    return "Must be at least 8 characters long, alpha numeric and contains 1 special character";
  }
  return true;
};

const checkPasswordMatch = (p1, p2) => {
  if (p1 !== p2) {
    return `Doesn't match password`;
  }
  return true;
};

const checkDateMatch = value => {
  if (!validator.matches(value, regExDob)) {
    return "Please Enter a Date in MM/DD/YYYY format";
  }
  return true;
};

const required = (value, field) => {
  if (!value) {
    return `${field} is Required`;
  }
  return true;
};

const validate = (validations, name, value, match = null) => {
  switch (name) {
    case "email":
      validations["emailV"] = checkEmail(value);
      break;
    case "currentPassword":
      validations["currentPasswordV"] = checkPassword(value);
      break;
    case "password":
      validations["passwordV"] = checkPassword(value);
      break;
    case "verifyPassword":
      validations["verifyPasswordV"] = checkPasswordMatch(value, match);
      validations["passwordV"] = checkPassword(match);
      break;
    case "firstname":
      validations["firstnameV"] = required(value, "First Name");
      break;
    case "lastname":
      validations["lastnameV"] = required(value, "Last Name");
      break;
    case "landlord_email":
      validations["landlord_emailV"] = checkEmail(value);
      break;
    case "company":
      validations["companyV"] = required(value, "Brokerage Name");
      break;
    case "licence":
      validations["licenceV"] = required(value, "Licence Name");
      break;
    case "address":
      validations["addressV"] = required(value, 'Address');
      break;
    case "city":
      validations["cityV"] = required(value, 'City');
      break;
    case "state":
      validations["stateV"] = required(value, 'State');
      break;
    case "zipcode":
      validations["zipcodeV"] = required(value, 'Zip Code');
      break;
    case "dob":
      validations["dobV"] = checkDateMatch(value);
      break;
    case "company_ein":
      validations["company_einV"] = required(value, 'Company EIN');
      break;
    case "last_4_of_ssn":
      validations["last_4_of_ssnV"] = required(value, 'Last 4 SSN');
      break;
    case "bank_name":
      validations["bank_nameV"] = required(value, 'Bank Name');
      break;
    case "account_number":
      validations["account_numberV"] = required(value, 'Account Number');
      break;
    case "routing_number":
      validations["routing_numberV"] = required(value, 'Routing Number');
      break;
    default:
      break;
  }

  return validations;
};

const checkValid = (validations, type = null) => {

  let check = Object.keys(validations).filter(
    v => validations[[v]] === null || validations[[v]] !== true
  );

  let typeIndex = -1;
  if (type === 1) {
    typeIndex = check.findIndex(c => c === 'company_einV')
  } else if (type === 2) {
    typeIndex = check.findIndex(c => c === 'dobV')
  }
  if (typeIndex !== -1) {
    check.splice(typeIndex, 1)
  }
  if (check.length > 0) {
    return true;
  }

  return false;
};

const checkTrue = values => {
  let check = Object.keys(values).filter(
    v => values[[v]] === true
  );

  if (check.length > 0) {
    return true;
  }

  return false;


}

export {
  checkEmail,
  checkPassword,
  checkPasswordMatch,
  required,
  validate,
  checkValid,
  checkTrue,
};
