const searchString = (nameKey, myArray) => {
    return myArray.filter((user) => {
        if (fullListingName(user).toLowerCase().search(nameKey.toLowerCase()) !== -1) {
            return true;
        }
        return false;
    })
}

const formatAmount = number => {
    if (number) {
      return new Intl.NumberFormat().format(String(number).replace(/,/g, ''))
    }
}

const addressSplit = geocoder => geocoder.reduce((acc, component) => {
    acc[component.types[0]] = component.long_name
    return acc;
}, {})

const makeAddress = address => {
    if (typeof address !== 'string') {
        let addressString = "";
        if (address.address_street_number) {
            addressString += address.address_street_number + ' '
        }

        if (address.address_street_name) {
            addressString += address.address_street_name + ' '
        }

        if (address.address_state) {
            addressString += address.address_state + ' '
        }

        if (address.address_country) {
            addressString += address.address_country + ' '
        }

        return addressString;
    }

    return address;

}

const ifAddressFilled = addresses => {
    let checkAddress = true;
    for (var i = 0; i < addresses.length; i++) {
        if (addresses[i].address_street_name !== ""
            && addresses[i].address_apartment_number !== ""
            && addresses[i].address_apartment_number !== ""
            && addresses[i].address_apartment_number !== "") {
            checkAddress = false;
            break;
        }
    }

    return checkAddress;
}

const getExtension = string => string.split('.').pop().toLowerCase();

const getOS = () => {
    var userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

    if (macosPlatforms.indexOf(platform) !== -1) {
        os = false;
    } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = true
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = false;
    } else if (/Android/.test(userAgent)) {
        os = true;
    } else if (!os && /Linux/.test(platform)) {
        os = false;
    }

    return os;
}

const checkAnswer = (answer, type, putanswer) => {
    if (typeof answer !== 'undefined') {
        if ((type === 'yes' && answer.answer) || (type === 'no' && !answer.answer)) {
            return 'active'
        } else {
            return ''
        }
    } else if (typeof putanswer !== 'undefined') {
        if ((type === 'yes' && putanswer) || (type === 'no' && !putanswer)) {
            return 'active'
        }
    }

    return ''
}

const checkLandlordSignature = (signatures, role_id) => {
    const findSignature = signatures.filter(sig => sig.user_role_id === role_id);
    console.log(findSignature)
    if (findSignature.length) {
        return true
    }

    return false;
}

const getRenterSignatures = (signatures) => signatures.some(signature => signature.user_role_type === "Renter");


const getTotalAmount = (values) => formatAmount(values.broker_fee + values.security_deposit + values.first_months_rent + values.last_months_rent + 100)

const modifyBackAccount = (accounts) => accounts.map(account => ({
    id: account.id,
    name: `${account.inititution.name} ${account.account.name} ****${account.account.mask}`
}))

const getActiveAccount = accounts => {
    const index = accounts.findIndex(account => account.status === true)
    if (index > -1) {
        return accounts[index].id;
    }
    return "";
}

const fullListingName = listing => `${listing.building.street_number} ${listing.building.street_name}, Apt ${listing.unit.number}, ${listing.building.city} ${listing.building.state}, ${listing.building.postal_code}`

const incomeFormat = amount => {
    let value = amount.replace(/[^0-9.]/g, "");
    let checkValueFloat = value.split('.')
    if (checkValueFloat.length > 1) {
        if (checkValueFloat[1].length > 1) {
            value = parseFloat(value).toFixed(2)
            value = formatAmount(value.split('.')[0]) + '.' + value.split('.')[1]
        }else{
            value = formatAmount(value.split('.')[0]) + '.' + value.split('.')[1]
        }
    }else{
         value = formatAmount(value)
    }
    
    return value;
}

const checkSignature = (signatures, docId, type) => signatures.some(signature => signature.listing_document_id === docId && signature.user_role_type === type);


const getSignatureId = (signatures, docId, type) => signatures.find(signature => signature.listing_document_id === docId && signature.user_role_type === type);


const getSignatures = (signatures, type) => signatures.some(signature => signature.user_role_type === type);



const checkUserSignature = signatures => {
    const roleID = getUserRoleID();
    return signatures.some(signature => signature.user_role_id === roleID);
}

const getUserRoleID = () => JSON.parse(JSON.parse(localStorage.getItem('persist:user')).user).message.user.role.id;


const getUserEmail = () => JSON.parse(JSON.parse(localStorage.getItem('persist:user')).user).message.user.email;

const getUserRole = () => JSON.parse(JSON.parse(localStorage.getItem('persist:user')).user).message.user.role.role_id;


export {
    searchString,
    formatAmount,
    addressSplit,
    makeAddress,
    getExtension,
    checkAnswer,
    getOS,
    checkLandlordSignature,
    getTotalAmount,
    modifyBackAccount,
    getActiveAccount,
    fullListingName,
    incomeFormat,
    checkSignature,
    checkUserSignature,
    getSignatureId,
    getSignatures,
    getUserEmail,
    ifAddressFilled,
    getRenterSignatures,
    getUserRole
}
