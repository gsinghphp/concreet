import axios from "axios";
import { toast } from "react-toastify";
import { SET_INITIAL_AUTH } from '../store/auth/constants';
export default {
  setupInterceptors: store => {
    // Add a response interceptor
    axios.interceptors.response.use(
      function (response) {
        return response.data;
      },
      function (error) {
        //catches if the session ended!
        if (error.response.status === 401) {
          store.dispatch({ type: SET_INITIAL_AUTH });
        }

        if (error.response.status !== 500
          && error.response.data.message !== "No Applications Found"
          && error.response.data.message !== "User Not Found.") {
          toast.error(error.response.data.message);
        }
        return Promise.reject(error.response.data);
      }
    );
  }
};
