//const API_URL = "http://54.84.174.61:3000/";
const API_URL = process.env.REACT_APP_HOST_URL;

const INIT = "INIT";
const LOADING = "LOADING";
const SUCCESS = "SUCCESS";
const ERROR = "ERROR";

const licenceTypes = [
  {
    id: 0,
    name: "Broker"
  },
  {
    id: 1,
    name: "Associate Broker"
  },
  {
    id: 2,
    name: "Salesperson"
  }
];

const accountTypes = [{
  id: 1,
  name: "Checking"
}, {
  id: 2,
  name: "Saving"
}, {
  id: 3,
  name: "Brokerage"
}];

const incomeTypes = [ {
  id: 1,
  name: "Employment"
}, {
  id: 2,
  name: "Other"
}]

const documents = {
  license: 1, //PDF,JPEG
  us_passport: 2, //Photo JPEG,PDF
  in_passport: 3, //JPEG,PDF
  visa: 4, //PDF,JPEG
  bank: 5, //PDF
  asset: 6, //PDF
  nine: 7,//PDF
  w2: 8,//PDF
  w2_2: 17,//PDF
  pay: 9,//PDF
  income: 10,//PDF
  standard_lease: 11,//PDF
  customized_lease: 12,//PDF
  asbestos: 13,//PDF
  begBugs: 14,//PDF
  leadpaint: 15,//PDF
  other: 16//PDF

}

const userRoles = {
  1: 'Renter',
  2: 'Agent',
  3: 'Landlord',
}

const questions = [
  { id: 1, question: 'Have you ever filed for or currently involved in a bankrupcy, been foreclosed on, or been a defendant in a civil suit?' },
  { id: 2, question: 'Have you ever been evicted from a tenancy or left owing money?' },
  { id: 3, question: 'Have you ever refused to pay rent when it was due?' },
  { id: 4, question: 'Have you ever been convicted of a felony or misdemeanor (other than a traffic or parking violation)?' },
  { id: 5, question: 'Do you represent to the best of your knowledge that the information enter above and contained within this section are true and accurate?' },
]

const applicationStatus = {
  0: 'In Progress',
  1: 'Submitted',
  2: 'Selected',
  3: 'Accepted',
  4: 'Rejected',
  5: 'Confirmed',
  6: 'Completed'
}

export {
  INIT,
  LOADING,
  SUCCESS,
  ERROR,
  API_URL,
  licenceTypes,
  accountTypes,
  incomeTypes,
  documents,
  userRoles,
  questions,
  applicationStatus
};
