import { createMuiTheme } from '@material-ui/core/styles';

const primaryColor = '#2F7AC9'
const secondaryColor = '#B3FDFF'
const darkgreyColor = '#797979'
const whiteColor = '#ffffff'



const theme = createMuiTheme({
    overrides: {
      // Name of the component ⚛️ / style sheet
      MuiButton: {
        // Name of the rule
        root: {
          // Some CSS
          background: darkgreyColor,
          borderRadius: 50,
          border: 0,
          color: 'white',
          height: 50,
          minWidth: 300,
          '&:hover': {
            backgroundColor: secondaryColor,
            color: primaryColor,
          },
          '&:focus': {
            backgroundColor: secondaryColor,
            color: primaryColor,
          },
        },
        
      },
    },
  });
  

  export default theme;
